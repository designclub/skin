<?php

/**
 * VariantTreeWidget
 */
class VariantTreeWidget extends yupe\widgets\YWidget
{
    public $view = 'variant-tree-widget';

    public $product = null;

    public function init()
    {
        $this->registerScript();
        return parent::init();
    }

    public function run()
    {
        if ($this->product===null) {
            echo '<div class="alert alert-danger">Товар не найден</div>';
            return ;
        }

        $this->render($this->view, ['tree' => $this->getTree()]);
    }

    public function getTree()
    {
        $data = [];
        foreach ($this->product->variants as $key => $variant) {
            if ($variant->parent_id===null) {
                $data[$variant->attribute->title][] = $variant->attributes;
            } else {
                $data[$variant->attribute->title][$variant->parent_id][] = $variant->attributes;
            }
        }
        return $data;
    }

    public function registerScript()
    {
        Yii::app()->clientScript->registerScript(
            __FILE__,
            <<<JS
$(document).ready(() => {
    // переключатель активной кнопки
    $('.js-variant-btn').on('click', e => {
        let item = $(e.target);
        let selTarget = item.data('target');
        let target = $(selTarget);
        let id = item.data('id');

        // Выбрать вариант из выпадающего списка
        $('option[value='+id+']').prop('selected', true);
        $('option[value='+id+']').click();
        $('option[value='+id+']').change();

        // Обновить остаток
        let quantity = $('option[value='+id+']').data('quantity');
        if (quantity!==undefined) {
            $('.js-view-quantity').text(quantity);
            $('#maxCount').val(quantity);
        }

        item
            .siblings()
            .removeClass('active');
        item.addClass('active');

        target
            .siblings('.variant-item__list')
            .addClass('hide');

        if (target.length > 0) {
            target.removeClass('hide');
            if (target.find('button.active').length > 0) {
                target
                    .find('button.active')
                    .click();
            } else {
                target
                    .find('button')
                    .first()
                    .click();
            }
        }
        return false;
    });

    // расставляем активные ссылки
    $('.variant-item > .variant-item__list').each((i, e) => {
        if ($(e).index()==1) {
            $(e).removeClass('hide');
            $(e)
                .find('.js-variant-btn')
                .first()
                .click();
        }
    });
});
JS
        );
    }
}
