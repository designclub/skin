<?php

/**
 * Class StoreCategoryRepository
 */
class StoreCategoryRepository extends CApplicationComponent
{

    /**
     * @param $slug
     * @return StoreCategory
     */
    public function getByAlias($slug)
    {
        return StoreCategory::model()->published()->find(
            'slug = :slug',
            [
                ':slug' => $slug,
            ]
        );
    }

    /**
     *
     */
    public function getAllDataProvider()
    {
        return new CArrayDataProvider(
            StoreCategory::model()->published()->getMenuList(1), [
                'id' => 'id',
                'pagination' => false,
            ]
        );
    }
    
    /**
     *
     */
    public function getAllCategoriesRootsDataProvider()
    {
        return new CArrayDataProvider(
            StoreCategory::model()->published()->roots()->findAll(), [
                'pagination' => false,
            ]
        );
    }    
    /**
     *
     */
    public function getAllCategoriesForParentDataProvider($parent)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'parent_id = :parent';
        $criteria->params = [':parent' => $parent];

        return new CArrayDataProvider(
            StoreCategory::model()->published()->findAll($criteria), [
                'pagination' => false,
            ]
        );
    }    
    
    /**
     *
     */
    public function getAllCategoriesListDataProvider($list)
    {
        $criteria = new CDbCriteria;
        $criteria->addIncondition('id', $list);

        return new CArrayDataProvider(
            StoreCategory::model()->published()->findAll($criteria), [
                'pagination' => false,
            ]
        );
    }
 

    /**
     * @param $path
     * @return mixed
     */
    public function getByPath($path)
    {
        return StoreCategory::model()->published()->findByPath($path);
    }
}