<?php

class m191205_150639_add_field_link_youtube_product extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_product}}', 'link_youtube', 'string DEFAULT NULL');
	}

	public function safeDown()
	{
        $this->dropColumn('{{store_product}}', 'link_youtube');
	}
}