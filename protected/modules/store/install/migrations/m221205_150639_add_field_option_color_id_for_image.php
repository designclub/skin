<?php

class m221205_150639_add_field_option_color_id_for_image extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_product_image}}', 'option_color_id', 'int(11) null');
	}

	public function safeDown()
	{
        $this->dropColumn('{{store_product_image}}', 'option_color_id');
	}
}