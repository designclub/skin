<?php

class m201205_150639_add_field_color_attribute_option extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{store_attribute_option}}', 'color', 'string DEFAULT NULL');
	}

	public function safeDown()
	{
        $this->dropColumn('{{store_attribute_option}}', 'color');
	}
}