<?php
Yii::app()->getClientScript()->registerCssFile($this->module->getAssetsUrl() . '/css/order-backend.css');

$this->breadcrumbs = [
    Yii::t('OrderModule.order', 'Мои заказы') => ['/order/clientBackend/view'],
    Yii::t('OrderModule.order', 'Заказ № ' . $model->id) => ['/order/clientBackend/viewOrder', 'id' => $model->id],
    Yii::t('OrderModule.order', 'Детали заказа'),
];

$this->pageTitle = Yii::t('OrderModule.order', 'Детали заказа');
 

$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id' => 'order-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => ['class' => 'well'],
    ]
);
 

?>
 
<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title"><?= Yii::t('OrderModule.order', 'Products'); ?></span>
                    </div>
                    <div class="panel-body">
                        <table id="products" class="table table-hover table-responsive">
                            <tr>
                                <th><?= Yii::t('OrderModule.order', 'Image');?></th>
                                <th colspan="2">Товар</th>
                                <?php //if ($hasVariants): ?><th><?= Yii::t('OrderModule.order', 'Variants');?></th><?php //endif; ?>
                                <th><p class="text-center"><?= Yii::t('OrderModule.order', 'Number');?></p></th>
                                <th><p class="text-center"><?= Yii::t('OrderModule.order', 'Price');?></p></th>
                            </tr>
                            <?php $totalProductCost = 0; ?>
                            <?php foreach ((array)$model->products as $orderProduct): ?>
                                <?php $totalProductCost += $orderProduct->price * $orderProduct->quantity; ?>
                                <?php $this->renderPartial('_product_row', ['model' => $orderProduct]); ?>
                            <?php endforeach; ?>
                        </table>

                        <div class="row" id="orderAddProduct">
                            <div class="col-sm-10">
                                <?php $this->widget(
                                    'bootstrap.widgets.TbSelect2',
                                    [
                                        'name' => 'ProductSelect',
                                        'asDropDownList' => false,
                                        'options' => [
                                            'minimumInputLength' => 2,
                                            'placeholder' => Yii::t('OrderModule.order', 'Select product'),
                                            'width' => '100%',
                                            'allowClear' => true,
                                            'ajax' => [
                                                'url' => Yii::app()->getController()->createUrl(
                                                    '/order/orderBackend/ajaxProductSearch'
                                                ),
                                                'dataType' => 'json',
                                                'data' => 'js:function(term, page) { return {q: term }; }',
                                                'results' => 'js:function(data) { return {results: data}; }',
                                            ],
                                            'formatResult' => 'js:productFormatResult',
                                            'formatSelection' => 'js:productFormatSelection',
                                        ],
                                        'htmlOptions' => [
                                            'id' => 'product-select',
                                        ],
                                    ]
                                ); ?>
                            </div>
                        </div>
                        <div class="text-right">
                            <h4>
                                <?= Yii::t('OrderModule.order', 'Total'); ?>:
                                <span id="total-product-cost"><?= $totalProductCost; ?></span>
                                <?= Yii::t('OrderModule.order', Yii::app()->getModule('store')->currency); ?>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title"><?= Yii::t('OrderModule.order', 'Контактные данные'); ?></span>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->label($model, 'phone'); ?>
                                <?= $model->phone; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->label($model, 'email'); ?>
                                <?= $model->email; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->label($model, 'comment'); ?>
                                <?= $model->comment; ?>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title"><?= Yii::t('OrderModule.order', 'Order details'); ?></span>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->label($model, 'date'); ?>
                                <?= Yii::app()->dateFormatter->format("dd.MM.yyyyг.", $model->date); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->label($model, 'delivery_id'); ?>
                                <?= $model->delivery->name; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <?= $form->label($model, 'address_delivery'); ?>
                                <?= $model->address_delivery; ?>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
     
</div>
<?php $this->endWidget(); ?>