<?php

class m191104_121527_add_field_order_address_delivery extends yupe\components\DbMigration
{
	public function safeUp()
	{
            $this->addColumn('{{store_order}}', 'address_delivery', 'varchar(1000)');
	}

	public function safeDown()
	{
            $this->dropColumn('{{store_order}}', 'address_delivery');
	}
}