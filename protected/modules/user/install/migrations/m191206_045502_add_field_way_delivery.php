<?php

class m191206_045502_add_field_way_delivery extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{user_user}}', 'method_delivery_id', 'int(1) DEFAULT NULL');
        $this->addColumn('{{user_user}}', 'address_delivery', 'varchar(500) DEFAULT NULL');
        
        //fk
        $this->addForeignKey(
            "fk_{{user_user}}_method_delivery_id",
            '{{user_user}}',
            'method_delivery_id',
            '{{store_delivery}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
	}

	public function safeDown()
	{
        $this->dropColumn('{{user_user}}', 'method_delivery');
        $this->dropColumn('{{user_user}}', 'address_delivery');
    }
}