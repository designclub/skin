<?php

class m191102_120102_add_field_is_admin extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{user_user}}', 'is_admin', 'int(1) DEFAULT 0');
	}

	public function safeDown()
	{
        $this->dropColumn('{{user_user}}', 'is_admin');
	}
}