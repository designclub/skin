<?php

/**
 * Виджет вывода новости по идентификатору
 *
 * @category YupeWidget
 * @package  yupe.modules.news.widgets
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @version  0.5.3
 * @link     http://yupe.ru
 *
 **/
Yii::import('application.modules.news.models.*');

/**
 * Class LastNewsWidget
 */
class OneNewsWidget extends yupe\widgets\YWidget
{
    public $id;

    /**
     * @var string
     */
    public $view = 'news';

    /**
     * @throws CException
     */
    public function run(){
		 if (empty($this->id))
			throw new CHttpException(400, Yii::t('NewsModule.news', 'Bad raquest. Please don\'t use similar requests anymore!'));
		
        $news = News::model()->published()->findByPk($this->id);
 
        $this->render($this->view, ['model' => $news]);
    }
}
