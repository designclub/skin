<?php

/**
 * Виджет вывода новостей из выбранной категории
 *
 *
 **/
Yii::import('application.modules.news.models.*');

/**
 * Class CategoryNewsWidget
 */
class CategoryNewsWidget extends yupe\widgets\YWidget
{
    /** @var $categories mixed Список категорий, из которых выбирать новости. NULL - все */
    public $category = 1;

    public $alias = null;
    /**
     * @var string
     */
    public $limit = 2;
    
    public $view = 'news-category';

    /**
     * @throws CException
     */
    public function run(){
        
        $category = empty($alias) ? Category::model()->findByPk($this->category) :  Category::model()->findByAttributes(['slug' => $this->alias]);
        
        $criteria = new CDbCriteria();
        $criteria->compare('category_id', $category->id);
        $criteria->limit = (int)$this->limit;
        $criteria->order = 'date DESC';

        $news = ($this->controller->isMultilang())
            ? News::model()->published()->language(Yii::app()->getLanguage())->findAll($criteria)
            : News::model()->published()->findAll($criteria);

        $this->render($this->view, ['category' => $category, 'models' => $news]);
    }
}
