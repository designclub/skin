<?php
/**
 * Отображение для виджета YAdminPanel:
 *
 * @category YupeView
 * @package  yupe
 * @author   AKulikov <tuxuls@gmail.com>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @version  0.1
 * @link     https://yupe.ru
 *
 **/
$mainAssets = Yii::app()->getAssetManager()->publish(
    Yii::getPathOfAlias('application.modules.yupe.views.assets')
);

$isAdmin = Yii::app()->getUser()->checkAccess('admin');
$canUpdateUser = Yii::app()->getUser()->checkAccess('User.UserBackend.Update');

foreach ($modules as &$item) {
    $item['linkOptions'] = ['title' => $item['label']];
    $item['label'] = CHtml::tag('span', ['class' => 'hidden-sm'], $item['label']);
}

$this->widget(
    'bootstrap.widgets.TbNavbar',
    [
        'fluid'    => true,
        'fixed'    => 'top',
        'brand'    => CHtml::image(
            $mainAssets . '/img/logo.png',
            CHtml::encode(Yii::app()->name),
            [
                'width'  => '50',
                'height' => '50',
                'title'  => CHtml::encode(Yii::app()->name),
            ]
        ),
        'brandUrl' => CHtml::normalizeUrl(["/yupe/backend/index"]),
        'items'    => [
            [
                'class' => 'bootstrap.widgets.TbMenu',
                'type'  => 'navbar',
                'encodeLabel' => false,
                'items' => (Yii::app()->user->getProfile()->is_admin == 1) ? $modules : []
            ],
            [
                'class'       => 'bootstrap.widgets.TbMenu',
                'htmlOptions' => ['class' => 'visible-xs hidden-sm hidden-md visible-lg hidden-xs menu-clients'],
                'type'        => 'navbar',
                'encodeLabel' => false,
                'items'       => [	
					[
						'label' => 'Мои заказы',
						//'url' => Yii::app()->createUrl('/order/clientBackend/view'),
						'url' => Yii::app()->createUrl('/order/order/clientOrders'),
						'visible' => Yii::app()->user->getProfile()->is_admin == 0 ? true : false,
						'itemOptions' => ['class' => 'btn btn-orders'],
					],
					[
						'label' => 'Галереи моих работ',
						'url' => Yii::app()->createUrl('/backend/gallery/gallery'),
						'visible' => Yii::app()->user->getProfile()->is_admin == 0 ? true : false,
						'itemOptions' => ['class' => 'btn btn-orders'],
					],				
				]
			],
            [
                'class'       => 'bootstrap.widgets.TbMenu',
                'htmlOptions' => ['class' => 'navbar-right visible-xs hidden-sm hidden-md visible-lg menu-clients menu-profile-clients'],
                'type'        => 'navbar',
                'encodeLabel' => false,
                'items'       => array_merge(
                    [

                        [
                            'icon' => 'fa fa-fw fa-home',
                            'label' => CHtml::tag(
                                    'span',
                                    ['class' => 'visible-lg'],
                                    Yii::t('YupeModule.yupe', 'На главную')
                                ),
                            'url' => Yii::app()->createAbsoluteUrl('/'),
                            'linkOptions' => ['target' => '_blank']
                        ],
                        [
                            'icon' => 'fa fa-fw fa-user',
                            'label' => '<span class="label label-info">' . CHtml::encode(
                                    Yii::app()->getUser()->getProfileField('email')
                                ) . '</span>',
                            'visible' => Yii::app()->user->getProfile()->is_admin == 1 ? true : false,
                            'items' => [
                                [
                                    'icon' => 'fa fa-fw fa-cog',
                                    'label' => Yii::t('YupeModule.yupe', 'Profile'),
                                    'url' => ($isAdmin || $canUpdateUser) ?
                                        CHtml::normalizeUrl(
                                            (['/user/userBackend/update', 'id' => Yii::app()->getUser()->getId()])
                                        ) 
                                        : 
                                        Yii::app()->createAbsoluteUrl('/user/profile/profile'),
                                ],
                                [
                                    'icon' => 'fa fa-fw fa-power-off',
                                    'label' => Yii::t('YupeModule.yupe', 'Exit'),
                                    'url' => CHtml::normalizeUrl(['/user/account/logout']),
                                ],
                            ],
                        ],
                        [
                            'icon' => 'fa fa-fw fa-sign-out',
                            'label' => CHtml::tag('span', ['class' => 'visible-lg'], Yii::t('YupeModule.yupe', 'Выход')),
                            'url' => Yii::app()->createAbsoluteUrl('/logout'),
                            'linkOptions' => []
                        ],
                    ]
                ),
            ],
        ],
    ]
);?>

<script type="text/javascript">
    $(document).ready(function () {
        var url = window.location.href;
        $('.navbar .nav li').removeClass('active');
        $('.nav a').filter(function () {
            return this.getAttribute("href") != '#' && this.href == url;
        }).parents('li').addClass('active');
    });
</script>
