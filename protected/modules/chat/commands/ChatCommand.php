<?php

Yii::import('application.modules.chat.components.ChatComponent');

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

/**
 * Class ChatCommand
 */
class ChatCommand extends \yupe\components\ConsoleCommand
{
    public function actionIndex()
    {
        /*$app = new \Ratchet\Http\HttpServer(
            new \Ratchet\WebSocket\WsServer(
                new ChatComponent()
            )
        );

        $loop = \React\EventLoop\Factory::create();

        $secure_websockets = new \React\Socket\Server('0.0.0.0:8085', $loop);
        $secure_websockets = new \React\Socket\SecureServer($secure_websockets, $loop, [
            'local_cert' => '/var/www/httpd-cert/designclub/tformulas.ru_le1.crtca',
            'local_pk' => '/var/www/httpd-cert/designclub/tformulas.ru_le1.key',
            'verify_peer' => false
        ]);

        $secure_websockets_server = new \Ratchet\Server\IoServer($app, $secure_websockets, $loop);
        $secure_websockets_server->run();*/

        $chat = new ChatComponent();
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    $chat
                )
            ),
            Yii::app()->getModule('chat')->port
        );

        $server->run();
    }
}
