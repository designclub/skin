<?php
/**
 * Файл настроек для модуля chat
 *
 * @author yupe team <team@yupe.ru>
 * @link https://yupe.ru
 * @copyright 2009-2019 amyLabs && Yupe! team
 * @package yupe.modules.chat.install
 * @since 0.1
 *
 */
return [
    'module'    => [
        'class' => 'application.modules.chat.ChatModule',
    ],
    'import'    => [],
    'component' => [],
    'rules'     => [
        '/chat' => 'chat/chat/index',
    ],
    'commandMap' => [
        'chat' => [
            'class' => 'application.modules.chat.commands.ChatCommand',
        ],
    ]
];
