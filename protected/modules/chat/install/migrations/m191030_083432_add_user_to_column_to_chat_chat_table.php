<?php

class m191030_083432_add_user_to_column_to_chat_chat_table extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{chat_chat}}', 'user_to', 'integer');
        $this->createIndex('ix_{{chat_chat}}_user_to', '{{chat_chat}}', 'user_to');
        $this->addForeignKey('fk_{{chat_chat}}_user_to', '{{chat_chat}}', 'user_to', '{{user_user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropIndex('ix_{{chat_chat}}_user_to', '{{chat_chat}}');
        $this->dropForeignKey('fk_{{chat_chat}}_user_to', '{{chat_chat}}');
        $this->dropTable('{{chat_chat}}');
    }
}
