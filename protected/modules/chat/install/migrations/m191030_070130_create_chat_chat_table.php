<?php

class m191030_070130_create_chat_chat_table extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable('{{chat_chat}}', [
            'id' => 'pk',
            'user_from' => 'integer',
            'message' => 'text',
            'status' => 'integer DEFAULT "0"',
            'create_at' => 'integer',
        ]);

        $this->createIndex('ix_{{chat_chat}}_user_from', '{{chat_chat}}', 'user_from');
        $this->addForeignKey('fk_{{chat_chat}}_user_from', '{{chat_chat}}', 'user_from', '{{user_user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropDableWithForeignKeys('{{chat_chat}}');
    }
}
