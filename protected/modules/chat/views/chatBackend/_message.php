<div class="well message-chat-box message-chat-box-<?= $model->user_from === Yii::app()->user->id ? 'from' : 'to' ?> message-chat-box-js" id="message-<?= $model->id ?>">
    <div class="message-chat-header__info">
        <div class="message-chat-header__name"><label><?= $model->userFrom->getFullName() ?></label>
        </div>
        <div class="message-chat-header__time"><span><?= Yii::app()->dateFormatter->format("dd.MM.yyyyг. HH:mm", $model->create_at) ?></span>
        </div>
    </div>
    <div class="message-chat__text">
        <?= $model->message ?>
    </div>
</div>