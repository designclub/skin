<?php
/**
* Отображение для chatBackend/index
*
* @category YupeView
* @package  yupe
* @author   Yupe Team <team@yupe.ru>
* @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
* @link     https://yupe.ru
**/
$this->breadcrumbs = [
    'Чат',
];

$this->pageTitle = 'Чат';

$this->menu = $this->getModule()->getNavigation();
?>

<div class="page-header">
    <h1><?= $this->pageTitle ?></h1>
</div>

<div class="container-chat">
    <div class="row">
        <div class="col-md-3">
            <div class="container-chat_clients">
                <h4>Пользователи</h4>
                <?php
                    $form = $this->beginWidget(
                        'bootstrap.widgets.TbActiveForm', [
                            'action' => Yii::app()->createUrl( $this->route ),
                            'method' => 'post',
                            'type' => 'inline',
                            'htmlOptions' => [ 'class' => 'form-filter-client', 'id' => 'search-form-client' ],
                        ]
                    );
                ?>
                
                <?=  $form->textFieldGroup($client, 'fio'); ?>

                <?= CHtml::submitButton('Найти', ['class' => 'btn btn-primary btn-search']); ?>
                
                <?php $this->endWidget(); ?>
                
                <ul class="nav nav-pills nav-stacked" id="list-user">
                <?php foreach ($users as $key => $user) : ?>
                    <li>
                        <?= CHtml::link('<span class="badge badge-default">'.$user->quantityMessage.'</span> '.$user->getFullName(), ['/chat/chatBackend/load', 'user_id' => $user->id], [
                            'class' => 'js-load',
                            'data-id' => $user->id,
                        ]) ?>
                    </li>
                <?php endforeach ?>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="container-chat_dialogs">
                <div class="messages-container"></div>
                <hr>
                <div class="form-chat">
                    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
                        'id' => 'chat-form',
                        'type' => 'inline',
                        'htmlOptions' => [
                            'class' => 'hide',
                        ]
                    ]) ?>
                        <?= $form->textAreaGroup($model, 'message') ?>
                        <?= $form->hiddenField($model, 'user_to') ?>
                        <?= $form->hiddenField($model, 'user_from') ?>
                        <?= CHtml::submitButton('Отправить', ['class' => 'btn btn-send']) ?>
                    <?php $this->endWidget() ?>
                </div>
            </div>
        </div>
    </div>
</div>