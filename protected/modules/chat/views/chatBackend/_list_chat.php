<?php foreach ($users as $key => $user) : ?>
    <li>
        <?= CHtml::link('<span class="badge badge-default">'.$user->quantityMessage.'</span> '.$user->getFullName(), ['/chat/chatBackend/load', 'user_id' => $user->id], [
            'class' => 'js-load',
            'data-id' => $user->id,
        ]) ?>
    </li>
<?php endforeach ?>