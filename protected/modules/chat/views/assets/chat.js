$(document).ready(function() {
    var address = host+':'+port;

    var conn = new WebSocket(address);
    conn.onopen = function(e) {
        console.log('Соединение установлено');
        conn.send(JSON.stringify(userData));
    };

    conn.onmessage = function(data) {
        var data = JSON.parse(data.data);
        if ('notRead' in data) {
            var link = $('a.js-load[data-id='+data.userFrom+']');
            link.find('.badge-default').text(data.notRead);
            if (link.parent().hasClass('active')) {
                load(link.attr('href'));
            }
        }
    }

    // Добавление или удаление сообщений
    function load(url) {
        $.ajax({
            url: url,
            dataType: 'html',
            success: function(data) {
                var ids = [];
                $(data).each(function(i, e) {
                    var elem = $(e);
                    var id = elem.attr('id');
                    var item = $('#'+id);
                    if (item.length === 0) {
                        $('.messages-container').prepend(elem);
                    }
                    ids.push(id);
                });
                $('.well').each(function(i, e) {
                    var search = $(e).attr('id');
                    if (ids.indexOf(search)==-1) {
                        $(e).remove();
                    }
                })
            }
        })
    }

    // загрузить в окно переписки новый чат с пользователем
    $('.js-load').on('click', function() {
        var elem = $(this);

        var url = elem.attr('href');
        var id = elem.data('id');
        $('#Chat_user_to').val(id);
        $('#chat-form').removeClass('hide');

        $('.js-load').each(function(i,e){
            $(e).parent().removeClass('active');
        });

        elem.parent().addClass('active');

        load(url);
        return false;
    });

    // обработчик формы
    $('#chat-form').on('submit', function() {
        var form           = $(this);
        var url            = form.attr('action');
        var data           = form.serialize();
        var type           = form.attr('method');
        var formId         = form.attr('id');
        var formIdSelector = '#'+formId;
        $.ajax({
            url,
            data,
            type,
            success: function(data) {
                $(formIdSelector).html($(data).find(formIdSelector).html());
                var link = $('#list-user li.active > a');
                if (link.length > 0) {
                    load(link.attr('href'));
                }

                conn.send(JSON.stringify({
                    "type": "update",
                    "userTo": $('#Chat_user_to').val(),
                    "userFrom": $('#Chat_user_from').val(),
                }));
            }
        });

        return false;
    })
});