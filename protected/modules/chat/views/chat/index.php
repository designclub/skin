<?php
/**
* Отображение для chat/index
*
* @category YupeView
* @package  yupe
* @author   Yupe Team <team@yupe.ru>
* @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
* @link     https://yupe.ru
**/
$this->pageTitle = Yii::t('ChatModule.chat', 'chat');
$this->description = Yii::t('ChatModule.chat', 'chat');
$this->keywords = Yii::t('ChatModule.chat', 'chat');

$this->breadcrumbs = [Yii::t('ChatModule.chat', 'chat')];
?>

<h1>
    <small>
        <?php echo Yii::t('ChatModule.chat', 'chat'); ?>
    </small>
</h1>