<?php
/**
* ChatBackendController контроллер для chat в панели управления
*
* @author yupe team <team@yupe.ru>
* @link https://yupe.ru
* @copyright 2009-2019 amyLabs && Yupe! team
* @package yupe.modules.chat.controllers
* @since 0.1
*
*/

class ChatBackendController extends \yupe\components\controllers\BackController
{
    /**
     * Действие "по умолчанию"
     *
     * @return void
     */
    public function actionIndex()
    {
        $this->_registerScript();

        $criteria = new CDbCriteria;
        $criteria->condition = 'id<>:id';
        $criteria->params[':id'] = Yii::app()->getModule('chat')->adminId;

        $users = User::model()->findAll($criteria);

        $client = new User;
        
        $model = new Chat;
        
        $model->user_from = Yii::app()->getModule('chat')->adminId;
         
        if (Yii::app()->getRequest()->getPost('User') !== null && Yii::app()->getRequest()->getIsAjaxRequest()){
            $client->setAttributes(Yii::app()->getRequest()->getPost('User'));
            $this->renderPartial('_list_chat', ['users' => $client->listClients()], false, true);
            Yii::app()->end();
        }
        
        if (isset($_POST['Chat'])) {
            $model->attributes = $_POST['Chat'];
            if ($model->save()) {
            }
        }

        $this->render('index', [
            'model' => $model,
            'users' => $users,
            'client' => $client,
        ]);
    }

    protected function _registerScript()
    {
        if (!Yii::app()->user->isGuest) {
            $module = Yii::app()->getModule('chat');
            $host = $module->host;
            $port = $module->port;
            $isGuest = (int)Yii::app()->user->isGuest;

            $pathAssets = __DIR__.'/../views/assets';
            $url = Yii::app()->getAssetManager()->publish(
                $pathAssets,
                false,
                -1,
                YII_DEBUG
            );

            Yii::app()->clientScript->registerScriptFile($url.'/chat.js', CClientScript::POS_END);

            $userData = CJSON::encode([
                'type' => 'init',
                'userId' => Yii::app()->user->id,
            ]);

            Yii::app()->clientScript->registerScript(__FILE__.'vars', "
                var userData = {$userData};
                var host = '{$host}';
                var port = {$port};
                var isGuest = {$isGuest};
            ", CClientScript::POS_HEAD);

            Yii::app()->clientScript->registerScript(__FILE__, "
                $(document).ready(function(){
                    console.log('test');
                });
            ");
        }
    }

    public function actionLoad($user_id)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'user_from=:user_id OR user_to=:user_id';
        $criteria->params[':user_id'] = $user_id;
        $criteria->limit = 50;
        $criteria->order = 'id ASC';

        $messages = Chat::model()->findAll($criteria);

        foreach ($messages as $key => $message) {
            $this->renderPartial('_message', ['model' => $message]);
        }
    }
}
