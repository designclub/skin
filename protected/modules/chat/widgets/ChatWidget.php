<?php
Yii::import('application.modules.chat.models.*');
/**
 * ChatWidget
 */
class ChatWidget extends \yupe\widgets\YWidget
{
    public function init()
    {
        $this->_registerScript();
        return parent::init();
    }

    public function run()
    {
        $model = new Chat('search');
        $model->user_from = Yii::app()->user->id;
        $model->user_to = Yii::app()->getModule('chat')->adminId;

        $model->status = [Chat::STATUS_NOT_READ, Chat::STATUS_READ];

        if (isset($_POST['Chat'])) {
            $model->status = Chat::STATUS_NOT_READ;
            $model->attributes = $_POST['Chat'];
            if ($model->save()) {
                Yii::app()->controller->refresh();
            }
        }

        $noRead = Chat::model()->count([
            'condition' => 'status=:status AND user_to=:user_to AND user_from=:user_from',
            'params' => [
                ':status'    => Chat::STATUS_NOT_READ,
                ':user_to'   => (int)$model->user_from,
                ':user_from' => (int)$model->user_to,
            ]
        ]);

        $this->render('chat-widget', [
            'model' => $model,
            'noRead' => $noRead,
        ]);
    }

    protected function _registerScript()
    {
        if (!Yii::app()->user->isGuest) {
            $module = Yii::app()->getModule('chat');
            $host = $module->host;
            $port = $module->port;
            $isGuest = (int)Yii::app()->user->isGuest;

            $pathAssets = __DIR__.DIRECTORY_SEPARATOR.'assets';
            $url = Yii::app()->getAssetManager()->publish(
                $pathAssets,
                false,
                -1,
                YII_DEBUG
            );

            Yii::app()->clientScript->registerScriptFile($url.'/chat.js', CClientScript::POS_END);

            $userData = CJSON::encode([
                'type' => 'init',
                'userId' => Yii::app()->user->id,
            ]);

            Yii::app()->clientScript->registerScript(__FILE__.'vars', "
                var userData = {$userData};
                var host = '{$host}';
                var port = {$port};
                var isGuest = {$isGuest};
            ", CClientScript::POS_HEAD);

            Yii::app()->clientScript->registerScript(__FILE__, "
                $(document).ready(function(){
                    console.log('test');
                });
            ");
        }
    }
}
