$(document).ready(function() {
    var formSelector = '#chat-form';
    var chatSelector = '#chat-list';
    var messageContainerSelector = chatSelector + ' .items';
    var messageSelector = chatSelector + ' .message-chat-box-js';
    var deleteLinkSelector = '.delete-message-js';


    var address = host+':'+port;
    console.log(address);

    var conn = new WebSocket(address);
    conn.onopen = function(e) {
        console.log('Соединение установлено');
        conn.send(JSON.stringify(userData));
    };

    conn.onmessage = function(data) {
        var data = JSON.parse(data.data);
        if ('event' in data) {
            chatObject[data.event](data);
        }
    }

    var chatObject = {};
    chatObject.update = function(data) {
        // Если находимся на странице заказа, подгрузить новое сообщение аяксом
        if ('notRead' in data) {
            $('.js-quantity-message').text(data.notRead);
        }

        $.ajax({
            success: function(data) {
                chatObject.updateMessage($(data).find(messageSelector));
            }
        });
    };

    chatObject.updateMessage = function(data) {
        $.each(data.get().reverse(), function(i,e) {
            var message = $(e);
            var messageId = message.attr('id');
            if ($('#'+messageId).length <= 0) {
                $(messageContainerSelector).prepend(e);
            }
        });
    }

    // Отправка сообщения
    $(document).delegate('#chat-form', 'submit', function() {
        var form = $(this);
        var url = form.attr('action');
        var type = form.attr('method');
        var data = form.serialize();
        var from = form.data('user-from');
        var to = form.data('user-to');

        $.ajax({
            url: url,
            type: type,
            data: data,
            success: function(data) {
                // Валидация формы
                var form = $(data).find(formSelector).html();
                $(formSelector).html(form);

                // Добавление сообщения в чат
                chatObject.updateMessage($(data).find(messageSelector));

                // Оповестить получателя о сообщении через soket
                conn.send(JSON.stringify({
                    "type": "update",
                    "userTo": to,
                    "userFrom": from,
                }));
            }
        })
        return false;
    });

    // Првоерка соединения
    setInterval(function(){
        if (conn.readyState !== 1) {
            conn = new WebSocket(address);
        }
    }, 10000);


    /*$(document).delegate(deleteLinkSelector, 'click', function() {
        if (confirm('Вы действительно хотите удалить это сообщение?')) {
            var item = $(this);
            var href = item.attr('href');
            $.ajax({
                url: href,
                dataType: 'json',
                success: function(data) {
                    if (data.status === 'success') {
                        item
                            .parents('.message-chat-box-js')
                            .fadeOut();
                    }
                }
            });
        }
        return false;
    });*/
});