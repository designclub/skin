<?php

/**
 * This is the model class for table "{{chat_chat}}".
 *
 * The followings are the available columns in table '{{chat_chat}}':
 * @property integer $id
 * @property integer $user_from
 * @property integer $user_to
 * @property string $message
 * @property integer $status
 * @property integer $create_at
 *
 * The followings are the available model relations:
 * @property UserUser $userFrom
 */
class Chat extends \yupe\models\YModel
{
    const STATUS_NOT_READ = 0;
    const STATUS_READ = 1;
    const STATUS_DELETE = 2;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{chat_chat}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['message', 'required'],
            ['user_from, user_to, status, create_at', 'numerical', 'integerOnly'=>true],
            ['message', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, user_from, message, status, create_at, user_to', 'safe', 'on'=>'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'userFrom' => [self::BELONGS_TO, 'User', 'user_from'],
            'userTo' => [self::BELONGS_TO, 'User', 'user_to'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_from' => 'От',
            'user_to' => 'Кому',
            'message' => 'Сообщение',
            'status' => 'Статус',
            'create_at' => 'Время',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->order = 'id DESC';
        $criteria->condition = 'user_to=:user_id or user_from=:user_id';
        $criteria->params[':user_id'] = Yii::app()->user->id;

        $criteria->compare('id', $this->id);
        // $criteria->compare('user_from', $this->user_from);
        // $criteria->compare('message', $this->message, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('create_at', $this->create_at);

        return new CActiveDataProvider($this, [
            'criteria'=>$criteria,
            'pagination' => [
                'pageSize' => 5,
            ]
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Chat the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->create_at = time();
        }

        return parent::beforeSave();
    }
}
