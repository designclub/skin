<?php

// use Yii;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class ChatComponent implements MessageComponentInterface
{
    protected $clients;
    public $users = [];

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);

        echo "Новое соединение, клиент {$conn->resourceId}\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $msg = CJSON::decode($msg);

        $msg['resourceId'] = $from->resourceId;

        if (isset($msg['type'])) {
            $type = $msg['type'];
            $this->$type($msg, $from);
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
        unset($this->users[$conn->resourceId]);

        echo "Соединение {$conn->resourceId} разорвано сокет удален\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "Произошла ошибка: {$e->getMessage()}\n";

        $conn->close();
    }

    public function update($data, $from)
    {
        if (isset($data['userTo'])) {
            foreach ($this->clients as $client) {
                $resourceId = $client->resourceId;
                if (isset($this->users[$resourceId]) and $this->users[$resourceId] == $data['userTo']) {
                    $data['event'] = "update";
                    $notRead = Yii::app()
                        ->db
                        ->createCommand('SELECT count(*) FROM {{chat_chat}} WHERE status=:status AND user_to=:to AND user_from=:from')
                        ->bindValue(':status', Chat::STATUS_NOT_READ)
                        ->bindValue(':to', $data['userTo'])
                        ->bindValue(':from', $data['userFrom'])
                        ->queryScalar();

                    $data['notRead'] = $notRead;

                    $client->send(CJSON::encode($data));
                }
            }
        }
    }

    public function statusRead($data, $from)
    {
        return Yii::app()
            ->db
            ->createCommand()
            ->update('{{chat_chat}}', [
                'status' => Chat::STATUS_NOT_READ
            ], ['user_to=:to and user_from=:from', ':to' => $data['userTo'], ':from' => $data['userFrom']]);
    }

    public function init($data, $from)
    {
        $this->users[$data['resourceId']] = $data['userId'];
        echo "Клиенту {$data['resourceId']} установлен идентификатор {$data['userId']}\n";
    }
}
