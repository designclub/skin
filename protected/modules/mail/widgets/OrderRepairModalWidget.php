<?php
/**
 * OrderRepairModalWidget виджет формы "Заказать ремонт Модалка"
 */
Yii::import('application.modules.mail.models.form.OrderRepairFormModal');

class OrderRepairModalWidget extends yupe\widgets\YWidget
{
    public $view = 'order-repair-widget';

    public function run()
    {
        $model = new OrderRepairFormModal;
        if (isset($_POST['OrderRepairFormModal'])) {

            $model->attributes = $_POST['OrderRepairFormModal'];

            if($model->verify == ''){
                if ($model->validate()){
                    Yii::app()->remonline->addClient(ClientsHelper::clientForm($model));
					Yii::app()->user->setFlash('order-repair-success', Yii::t('MailModule.mail', 'Ваша заявка успешно отправлена.'));
                    Yii::app()->controller->refresh();
                }
            }
        }      

        $this->render($this->view, [
            'model' => $model,
        ]);
    }

}
