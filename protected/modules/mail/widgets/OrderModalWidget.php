<?php
/**
 * OrderFormModal виджет формы "Заказать товар"
 */
Yii::import('application.modules.mail.models.form.OrderForm');

class OrderModalWidget extends yupe\widgets\YWidget
{
    public $view = 'order-product-widget';

    public function run()
    {
        $model = new OrderForm;
        if (isset($_POST['OrderForm'])) {

            $model->attributes = $_POST['OrderForm'];

            if($model->verify == ''){
                if ($model->validate()){
					Yii::app()->user->setFlash('order-product-success', Yii::t('MailModule.mail', 'Ваша заявка успешно отправлена.'));
                    Yii::app()->controller->refresh();
                }
            }
        }      

        $this->render($this->view, [
            'model' => $model,
        ]);
    }

}
