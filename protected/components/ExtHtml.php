<?php 

/**
 * 
 */
class ExtHtml extends CHtml
{
	public static function radioButtonVariantList($name,$select,$data,$htmlOptions=array())
	{
		$template=isset($htmlOptions['template'])?$htmlOptions['template']:'{input} {label}';
		$separator=isset($htmlOptions['separator'])?$htmlOptions['separator']:self::tag('br');
		$container=isset($htmlOptions['container'])?$htmlOptions['container']:'span';
		$itemOptions=isset($htmlOptions['itemOptions'])?$htmlOptions['itemOptions']:[];
		unset($htmlOptions['template'],$htmlOptions['separator'],$htmlOptions['container'], $htmlOptions['itemOptions']);

		$labelOptions=isset($htmlOptions['labelOptions'])?$htmlOptions['labelOptions']:array();
		unset($htmlOptions['labelOptions']);

		if(isset($htmlOptions['empty']))
		{
			if(!is_array($htmlOptions['empty']))
				$htmlOptions['empty']=array(''=>$htmlOptions['empty']);
			$data=CMap::mergeArray($htmlOptions['empty'],$data);
			unset($htmlOptions['empty']);
		}

		$items=array();
		$baseID=isset($htmlOptions['baseID']) ? $htmlOptions['baseID'] : self::getIdByName($name);
		unset($htmlOptions['baseID']);
		$id=0;

		foreach($data as $value=>$labelTitle)
		{
			$attrTitle=$labelTitle['is_color'] == 0 ? '<span>'.$labelTitle['attribute_name'].'</span>' : '';
			$atrrColor=$labelTitle['is_color'] == 1 ? '<span class="is_color" style="background: #'.$labelTitle['attribute_color'].'"></span>' : '';

			$itemOption=isset($itemOptions[$value])?$itemOptions[$value]:[];
			$checked=!strcmp($value,$select);
			$htmlOptions['value']=$value;
			$htmlOptions['id']=$baseID.'_'.$id++;
			$option=self::radioButton($name,$checked,$htmlOptions+$itemOption);
			$beginLabel=self::openTag('label',$labelOptions);
			$label=self::label(
				$attrTitle.$atrrColor,
				$htmlOptions['id'],$labelOptions
			);
			$endLabel=self::closeTag('label');

			$template = '';

			if($labelTitle['is_parent']){
				$template = '<div class="js-variant-price products-sizes products-sizes_size">'.
				'{input}'.
				'{label}'.
				'</div>';
			} else {
				$template = '<div class="js-variant-price products-sizes products-sizes_size">'.
				'{input}'.
				'{label}'.
				'</div>';
			}
			
			$items[]=strtr($template,array(
				'{input}'=>$option,
				'{beginLabel}'=>$beginLabel,
				'{label}'=>$label,
				'{labelTitle}'=>$labelTitle,
				'{endLabel}'=>$endLabel,
			));
		}
		if(empty($container))
			return implode($separator,$items);
		else
			return self::tag($container,array('id'=>$baseID),implode($separator,$items));
	}
}