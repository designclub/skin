<div class="col-sm-4 col-xs-12">
	<div class="item-block">
       <a href="<?= $data->image->getImageUrl(); ?>" data-lightbox="roadtrip">
            <?=
              CHtml::link(CHtml::image(
                $data->image->getImageUrl(350, 300, true),
                $data->image->alt,
                [
                	'title' => $data->image->alt, 
                	'href' => $data->image->getImageUrl(), 
                	'class' => 'gallery-image'
                ]),
             	$data->image->getImageUrl(), ['data-lightbox' => 'road']
            ); ?>
        </a>
    </div>
</div>