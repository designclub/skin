<div class="block-column gallery-albums">
	<?php $this->widget(
		'bootstrap.widgets.TbListView',
		[
			'dataProvider'  => $dataProvider,
			'itemView'      => '_image',
			'template'      => "{items}",
			'itemsCssClass' => 'row',
			'itemsTagName'  => 'div',
		]
	); ?>
</div>