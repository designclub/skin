<?php
// конфиг ресайза изображений
$config = [
    ['class' => 'prima-pelle-item-vertical', 'width' => 270, 'height' => 345],
    ['class' => 'prima-pelle-item-vertical', 'width' => 270, 'height' => 345],
    ['class' => 'prima-pelle-item-horizontal-big', 'width' => 570, 'height' => 345],
    ['class' => 'prima-pelle-item-horizontal', 'width' => 569, 'height' => 225],
    ['class' => 'prima-pelle-item-rect', 'width' => 270, 'height' => 225],
    ['class' => 'prima-pelle-item-rect', 'width' => 270, 'height' => 225],
] ?>

<div class="prima-pelle">
    <div class="prima-pelle-head">
        <div class="prima-pelle-head__title">
            <h2><?= $gallery->name ?></h2>
        </div>
        <div class="prima-pelle-head__buttons">
            <?= CHtml::link('Ваши работы', ['/vashi-raboty'], ['class' => 'btn btn-white']) ?>
            <?= CHtml::link('История успеха', ['/istoriya-uspeha'], ['class' => 'btn btn-white']) ?>
        </div>
    </div>
    <div class="prima-pelle-body">
        <?php $i = 0 ?>
        <?php foreach ($dataProvider->getData() as $key => $image) : ?>
            <?php
            $params = $config[$i] ?? [];
            $src = $image->image->getImageUrl($params['width'] ?? 100, $params['height'] ?? 100);
            ?>

            <div class="prima-pelle-body__item">
                <div class="<?= $params['class'] ?? 'prima-pelle-item-default' ?>" style="background: url(<?= $src ?>)">
                    <div class="prima-pelle-content">
                        <div class="main-title"><?= $image->image->name ?></div>
                        <div class="prima-pelle-desc"><?= $image->image->description ?>
                        </div>
                        <a href="#" class="prima-pelle-more">Подробнее</a>
                    </div>
                </div>
            </div>

            <?php
            $i++;
            if ($i > 6) {
                $i=0;
            }
            ?>
        <?php endforeach ?>
    </div>
</div>