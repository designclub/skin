<div>
    <?php if(!empty($data->image->description)): ?>
	    <?= CHtml::link(CHtml::image($data->image->getImageUrl(), $data->image->alt, ['title' => $data->image->alt, 'href' => $data->image->getImageUrl(), 'class' => 'gallery-image']), $data->image->description); ?>
    <?php else: ?>
        <?= CHtml::image($data->image->getImageUrl(), $data->image->alt, ['title' => $data->image->alt, 'href' => $data->image->getImageUrl(), 'class' => 'gallery-image']); ?>
    <?php endif; ?>
</div>
