<?php
/**
 * Отображение для gallery/_item:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $this GalleryController
 * @var $data Gallery
 **/
?>

<div class="col-sm-4 col-xs-12 gallery-page_item">
	<div class="item">
		<?= CHtml::link(CHtml::image($data->previewImage(400, 300), $data->name, ['class' => 'media-object']), ['/gallery/gallery/view', 'id' => $data->id]); ?>
        <div class="item-content">
			<div class="name">
				<h3>
					<?= CHtml::encode($data->name); ?>
				</h3>
			</div>
			<div class="continue">
				<div class="button">
					<?= CHtml::link(CHtml::encode('Подробнее'), ['/gallery/gallery/view', 'id' => $data->id]); ?>
				</div>
			</div>
		</div>
	</div>
</div>