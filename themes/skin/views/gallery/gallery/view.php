<?php
/**
 * @var $this GalleryController
 * @var $model Gallery
 */

$this->title = Yii::t('GalleryModule.gallery', 'Gallery');
$this->breadcrumbs = [
    Yii::t('GalleryModule.gallery', 'Galleries') => ['/gallery/gallery/index'],
    $model->name
];
?>
<div class="gallery-show">
    <?php $this->widget(
        'gallery.widgets.GalleryWidget',
        ['galleryId' => $model->id, 'gallery' => $model, 'limit' => 30]
    ); ?>

    <?php /*if (Yii::app()->getUser()->isAuthenticated()) : ?>
        <?php if ($model->canAddPhoto) : ?>
            <?php $this->renderPartial('_form', ['model' => $image, 'gallery' => $model]); ?>
        <?php endif ?>
    <?php endif;*/ ?>
</div>

<?php Yii::app()->getClientScript()->registerScript(
    "gallery-albums",
    <<<JS
        lightbox.option({
            'albumLabel': 'Изображение %1 из %2'
        })
JS
); ?>

