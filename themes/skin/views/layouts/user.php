<?php $this->beginContent('//layouts/main'); ?>
    <div class="lk-content">
        <div class="content-site">
            <div class="breadcrumbs hidden-xs">
                <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', [
                     
                ]); ?>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header"><?= $this->title; ?></h2>
                </div>
            </div> 
            <div class="lk-box">
                <div class="lk-box__menu">
                    <?php $this->widget(
                        'bootstrap.widgets.TbMenu',
                        [
                            'id'=>'lk-menu',
                            'items' => [
                                [
                                    'label' =>'Настройки профиля', 
                                    'url' => Yii::app()->createUrl('/user/profile/profile'),
                                    'encodeLabel' => false,
                                ],
                                [
                                    'label' =>'Мои заказы', 
                                    'url' => Yii::app()->createUrl('/order/order/clientOrders'),
                                    'encodeLabel' => false,
                                ],
                                [
                                    'label' =>'Мои галереи изображений', 
                                    'url' => Yii::app()->createUrl('/backend/gallery/gallery'),
                                    'encodeLabel' => false,
                                ],
                                [
                                    'label' =>'Выход', 
                                    'url' => Yii::app()->createUrl('/user/account/logout'),
                                    'encodeLabel' => false,
                                ],
                            ],
                            'htmlOptions' => [
                                'class' => 'lk-menu'
                            ]
                        ]
                    ); ?>
                </div>
                <div class="lk-box__content">
                    <?= $content; ?>
                </div>
            </div>
        </div>
    </div>
<?php $this->endContent(); ?>
