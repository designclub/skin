<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_START);?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="Content-Language" content="ru-RU" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title><?= $this->title;?></title>
    <meta name="description" content="<?= $this->description;?>" />
    <meta name="keywords" content="<?= $this->keywords;?>" />

    <?php if ($this->canonical) : ?>
        <link rel="canonical" href="<?= $this->canonical ?>" />
    <?php endif; ?>

    <?php
        Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/style.css');
        Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/blog.js');
        Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/bootstrap-notify.js');
        Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.li-translit.js');
    ?>
    <script type="text/javascript">
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
    </script>

    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_END);?>
</head>

<body>

<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_START);?>


<!-- container -->
<div class="container-site">

    <!-- header -->
    <?php $this->renderPartial('//layouts/_header'); ?>
    <!-- header end -->

    <!-- flashMessages -->
    <?php $this->widget('yupe\widgets\YFlashMessages'); ?>
    <!-- end flashMessages -->

    <?= $content; ?>

    <!-- footer -->
    <?php $this->renderPartial('//layouts/_footer'); ?>
    <!-- footer end -->


    <script type="text/javascript">
    $(function(){

        $('.m-menu-button').on("click", function(){
            $('body').addClass('menu_m_visible');
            $('.mobile-panel').addClass('active-menu');
            
});

        $('.m-menu-buttons').on("click", function(){
            if($('body').hasClass('menu_m_visible') == true){
                $('body').removeClass('menu_m_visible');
                $('.mobile-panel').removeClass('active-menu');
            }
            else{
                $('body').addClass('menu_m_visible');
                $('.mobile-panel').addClass('active-menu');
            }
        });

        $('body').addClass('home_is_visible');

        function removeClasses() {
            $(".mobile-menu ul li").each(function() {
                var custom_class = $(this).find('a').data('class');
                $('body').removeClass(custom_class);
            });
        }

        $('.mobile-menu a').on('click',function(e){
            removeClasses();
            var custom_class = $(this).data('class');
            $('body').addClass(custom_class);
        });
        
        $(".search-icon").on("click", function(clck){
            clck.preventDefault();
            $(".header-menu").hide();
            $(".search-panel").removeClass("hidden");
            $(".search-icon").addClass("hidden");
            $(".close-icon").removeClass("hidden");
        });
        
        $(".close-icon").on("click", function(clck){
            clck.preventDefault();
            $(".header-menu").show();
            $(".search-panel").addClass("hidden");
            $(".close-icon").addClass("hidden");
            $(".search-icon").removeClass("hidden");
        });
        
        $('#razmer-2 label').on("click", function(clck){
            clck.preventDefault();
            $(this).toggleClass('active');
        });
        
/*        $('.catalog-menu > ul > li').on("click", function(clck){
            clck.preventDefault();
            $(this).toggleClass('active');
        });*/
        
    });   
    
</script>

</div>

<div class='notifications top-right' id="notifications"></div>
<!-- container end -->

<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_END);?>

</body>
</html>