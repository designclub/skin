<div class="footer-header-panel">
    <div class="content-site">
        <div class="footer-header-panel-flex">
            <div class="mail">
                <span class="icon--7"></span>
                Электронная почта для связи: 
                <?php if (Yii::app()->hasModule('contentblock')): ?>
                    <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'email']); ?>
                <?php endif; ?>
            </div>
            
            <div class="phone">
                <a href="tel:+7(3532) 43-10-18"><span class="icon--6"></span></a>                    
                <?php if (Yii::app()->hasModule('contentblock')): ?>
                    <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'phone']); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div> <!-- end footer-header-panel -->

<!-- <div class="footer-middle-panel">
    <div class="content-site">
        <div class="flex-panel">
            <div class="flex-block left-block-footer">
               <?php if (Yii::app()->hasModule('menu')): ?>
               <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'navigation', 'layout' => 'menu']); ?>
               <?php endif; ?>
            </div>
            <div class="flex-block footer-block-footer">
                <?php $this->widget('application.modules.store.widgets.CategoryWidget', ['depth' => 1]); ?>
            </div>
        </div>
    </div>
</div>end footer-middle-panel -->
<div class="footer">
    <div class="content-site">
        <div class="footer-nav">
            <div class="footer-nav-item">
                <p>О нас</p>
                <ul>
                    <li><a href="/istoriya">История</a></li>
                    <li><a href="/news/novosti">Новости</a></li>
                    <li><a href="/preimushchestva">Преимущества</a></li>
                    <li><a href="/news/otzyvy">Отзывы</a></li>
                    <li><a href="/news/nashi-video">Наши видео</a></li>
                    <li><a href="/kontakty">Контакты</a></li>
                </ul>
            </div>

            <div class="footer-nav-item">
                <p>Каталог кожи</p>
                <ul>
                    <li><a href="/store/katalog-kozhi">Все позиции</a></li>
                    <li><a href="/store/katalog-kozhi">Вид дубления</a></li>
                    <li><a href="/store/katalog-kozhi">Назначение</a></li>
                    <li><a href="/store/katalog-kozhi">Постоянный ассортимент</a></li>
                    <li><a href="/store/katalog-kozhi">Ременные заготовки</a></li>
                </ul>
            </div>

            <div class="footer-nav-item">
                <p>Сопутствующие товары</p>
                <ul>
                    <li><a href="/store/soputstvuyushchie-tovary/himiya">Химия</a></li>
                    <li><a href="/store/soputstvuyushchie-tovary/furnitura">Фурнитура</a></li>
                    <li><a href="/store/soputstvuyushchie-tovary/nit">Нитки</a></li>
                </ul>
            </div>

            <div class="footer-nav-item">
                <p>Распродажа</p>
                <ul>
                    <li><a href="#">Все работы</a></li>
                    <li><a href="#">Фильтр</a></li>
                </ul>
            </div>

            <div class="footer-nav-item">
                <p>Галерея работ</p>
            </div>

            <div class="footer-nav-item">
                <p>Покупки в Prime Pelle</p>
                <ul>
                    <li><a href="/payment">Оплата</a></li>
                    <li><a href="/delivery">Доставка</a></li>
                    <li><a href="/return">Легкий возврат</a></li>
                    <li><a href="/support">Служба поддержки</a></li>
                </ul>
            </div>
        </div><!-- footer-nav -->
    </div>
</div><!-- footer -->

<div class="footer-end-panel">
    <div class="content-site">
        <div class="footer-end-panel-text">
            <div class="policy-block">
                © <?= date('Y'); ?> Prima Pelle.
                <?php if (Yii::app()->hasModule('contentblock')): ?>
                    <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'policy']); ?>
                <?php endif; ?>
            </div>
            
            <div class="social">
                <?php if (Yii::app()->hasModule('contentblock')): ?>
                <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'socnetwork']); ?>
                <?php endif; ?>
            </div>
            <div class="dc56">
                <a href="https://designclub56.ru/"><span class="icon-DCMedia--"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span></a>
                <?= CHtml::link('Создание и продвижение сайтов Design Club Media', 'https://designclub56.ru/'); ?>
            </div>
            <!-- <div class="footer-end-panel-copy">
                <span class="icon-DCMedia---"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>
                <?= CHtml::link('Создание и продвижение сайтов Design Club Media', 'https://designclub56.ru/'); ?>
            </div> -->
        </div>
    </div>
</div><!-- end footer-end-panel -->
<?php if(Yii::app()->getUser()->getProfileField('is_admin') != 1):?>
    <?php $this->widget('application.modules.chat.widgets.ChatWidget') ?>
<?php endif; ?>