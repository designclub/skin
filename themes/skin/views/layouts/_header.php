<div class="top-panel">
    <div class="content-site">
        <div class="top-block">
            <div>
                <div class="mobile-panel">
                    <div class="m-menu-button">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                    <div class="mobile-menu ">
                        <div class="m-menu-buttons">
                            <span class="line"></span>
                            <span class="line"></span>
                            <span class="line"></span>
                        </div>
                        <div class="mobile-content">
                            <div class="catalog-menu-mobile visible-xs">
                                <?php $this->widget('application.modules.store.widgets.CategoryWidget', ['depth' => 1]); ?>
                            </div><!-- end catalog-menu -->
                            <?php if (Yii::app()->hasModule('menu')): ?>
                            <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'menu', 'layout' => 'menu']); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>



            <div class="search-panel">
                <?php $this->widget('application.modules.store.widgets.SearchProductWidget'); ?>
            </div>
            <div class="logo">
                <?php if (Yii::app()->hasModule('contentblock')): ?>
                    <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'logo']); ?>
                    <?php endif; ?>
            </div>
            <div class="actions">
                <div class="favorites item">
                   <!-- <span class="icon--2"></span>
                    <a href="#">Избранное</a>-->
                </div>
                <div class="login item">
                    <?php if(Yii::app()->getUser()->getIsGuest()): ?>
                        <?= CHtml::link('<span class="icon--3 key"></span> Войти', ['/login']); ?>
                    <?php else: ?>
                        <?= CHtml::link('<span class="icon--3 key"></span> Мой кабинет', ['/user/profile/profile']); ?>
                    <?php endif; ?>
                </div>
                <div class="backet item" id="shopping-cart-widget">
                    <?php $this->widget('application.modules.cart.widgets.ShoppingCartWidget'); ?>
                </div>
            </div>
        </div>
    </div>
</div><!-- top-panel -->

<div class="menu hidden-xs">
    <div class="content-site">
        <div class="col-sm-12 no-padding">
            <div class="menu-block">
                <div class="row no-margin">
                    <div class="col-sm-6 no-padding">
                        <?php $this->widget('application.modules.store.widgets.CategoryWidget', ['depth' => 1]); ?>
                    </div>
                    <div class="col-sm-6 no-padding">
                        <?php if (Yii::app()->hasModule('menu')): ?>
                        <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'menu', 'layout' => 'menu']); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- menu -->



