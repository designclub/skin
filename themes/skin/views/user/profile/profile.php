<?php
$this->title = Yii::t('UserModule.user', 'Настройки профиля');

$this->breadcrumbs = [
    "Личный кабинет" => [Yii::app()->createUrl('/user/profile/index')],
    Yii::t('UserModule.user', 'Настройки')
];

$this->layout = "//layouts/user";

$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id' => 'profile-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'type' => 'inline',
        'htmlOptions' => [
            'class' => 'form-site form-white form-label',
            'enctype' => 'multipart/form-data',
        ]
    ]
);
?>
    <?= $form->errorSummary($model); ?>
    
    <div class="lk-setting">
        <div class="lk-setting__content">
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->textFieldGroup($model, 'last_name', [
                        'widgetOptions' => [
                            'htmlOptions'=>[
                                'placeholder' => 'Фамилия'
                            ],
                        ],
                    ]) ?>
                    <?= $form->textFieldGroup($model, 'first_name', [
                        'widgetOptions' => [
                            'htmlOptions'=>[
                                'placeholder' => 'Имя'
                            ],
                        ],
                    ]) ?>
                    <?= $form->textFieldGroup($model, 'middle_name', [
                        'widgetOptions' => [
                            'htmlOptions'=>[
                                'placeholder' => 'Отчество'
                            ],
                        ],
                    ]) ?>
                    
                    <div class="form-group">
                        <?= $form->labelEx($model, 'phone', ['class' => 'control-label']) ?>
                        <?php $this->widget('CMaskedTextFieldPhone', [
                            'model' => $model,
                            'attribute' => 'phone',
                            'mask' => '+7-999-999-9999',
                            'htmlOptions'=>[
                                'class' => 'data-mask form-control',
                                'data-mask' => 'phone',
                                'placeholder' => 'Телефон',
                                'autocomplete' => 'off'
                            ]
                        ]) ?>
                    </div>
                
                    <?= $form->textFieldGroup($user, 'email', [
                        'widgetOptions' => [
                            'htmlOptions' => [
                                'disabled' => true,
                                'placeholder' => 'Email'
                            ],
                        ],
                        'append' => CHtml::link(Yii::t('UserModule.user', 'Change email'), ['/user/profile/email']),
                    ]); ?>
                    
                    <?= $form->label($model, 'method_delivery_id'); ?>
                    <?= $form->dropDownListGroup($model, 'method_delivery_id', [
                        'widgetOptions' => [
                            'data' => CHtml::listData(Delivery::model()->published()->findAll(), 'id', 'name'),
                            'htmlOptions' => [
                                'empty' =>  ' -- выберите способ доставки --',
                                'class' => 'popover-help',
                            ]
                        ]
                    ]); ?>
                    
                    <?= $form->label($model, 'address_delivery'); ?>
                    <?= $form->textFieldGroup($model, 'address_delivery', [
                        'widgetOptions' => [
                            'htmlOptions'=>[
                                'placeholder' => 'Пример, офис на Восточной, д. 42, г. Оренбург'
                            ],
                        ],
                    ]) ?>
                    
                </div>
                <div class="col-sm-6">
                    <div class="row lk-setting__img">
                        <div class="col-xs-7 lk-setting__avatar">
                            <?php if($user->avatar) : ?>
                                <?= CHtml::image( '/uploads/' . $this->module->avatarsDir . '/' . $user->avatar, '', ['style' => 'height: auto;']); ?>
                            <?php else: ?>
                                <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/nophoto.png', '', ['class' => 'nophoto']); ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-xs-5 file-upload">
                            <label>
                                <?= $form->fileField($model, 'avatar'); ?>
                                <span><div id="count_file">Прикрепить файл</div></span>
                            </label>
                        </div>
                    </div>
                                        
                    <?= $form->textAreaGroup($model, 'location', [
                        'widgetOptions' => [
                            'htmlOptions' => [
                                'rows' => 6,
                                'placeholder' => 'Адрес'
                            ],
                        ],
                    ]); ?>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                      <?= CHtml::submitButton('Сохранить данные', ['class' => 'btn btn-brown']) ?>
                </div>
            </div>
        </div>
    </div>
<?php $this->endWidget(); ?>

<?php Yii::app()->getClientScript()->registerScript("file-upload", "
    $('.file-upload input[type=file]').change(function(){
        var inputFile = document.getElementById('ProfileForm_avatar').files;
        if(inputFile.length > 0){
            $('#count_file').text('Выбрано файлов ' + inputFile.length);
        }else{
            $('#count_file').text('Прикрепить файл');
        }
    });
"); ?>