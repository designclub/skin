<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="content-site">
    <div class="page-txt page-site">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <h1><?= $model->title; ?></h1>
            </div>
        </div>
    	<div class="row">
    		<div class="col-sm-6 col-xs-12">
        		<?= $model->body; ?>
    		</div>
    		<div class="col-sm-6 col-xs-12">
    			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2471.7582466763697!2d55.135252515977555!3d51.71916530301818!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x417bfa5ea1546c25%3A0xc64e271e6dbfd543!2z0JHQtdC70Y_QtdCy0YHQutCw0Y8g0YPQuy4sIDU1LCDQntGA0LXQvdCx0YPRgNCzLCDQntGA0LXQvdCx0YPRgNCz0YHQutCw0Y8g0L7QsdC7LiwgNDYwMDQ1!5e0!3m2!1sru!2sru!4v1570184413246!5m2!1sru!2sru" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    		</div>
    	</div>
    </div>
</div>