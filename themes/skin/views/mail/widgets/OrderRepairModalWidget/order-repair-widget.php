<div id="orderRepairModal" class="modal modal-my fade" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header box-style">
				<div data-dismiss="modal" class="modal-close">
					<div></div>
				</div>
				<div class="box-style__heading">
					<?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                        'id' => 6
                    ]); ?>
				</div>
			</div>
			<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
                    'id'=>'orderRepair-form-modal',
                    'type' => 'vertical',
                    'htmlOptions' => ['class' => 'form-my form-order-repair', 'data-type' => 'ajax-form'],
                ]); ?>

			<?php if (Yii::app()->user->hasFlash('order-repair-success')) : ?>
				<script>
                    $('#orderRepairModal').modal('hide');
                    $('#messageModal').modal('show');
                    setTimeout(function(){
                        $('#messageModal').modal('hide');
                    }, 4000);
                </script>
			<?php endif ?>

			<div class="modal-body">
				<?= $form->textFieldGroup($model, 'name', [
                        'widgetOptions'=>[
                            'htmlOptions'=>[
                                'class' => '',
                                'autocomplete' => 'off'
                            ]
                        ]
                    ]); ?>

				<?= $form->textFieldGroup($model, 'email', [
						'widgetOptions'=>[
							'htmlOptions'=>[
								'class' => '',
								'autocomplete' => 'off'
							]
						]
					]); ?>

				<div class="form-group">
					<?= $form->labelEx($model, 'phone', ['class' => 'control-label']) ?>
					<?php $this->widget('CMaskedTextFieldPhone', [
                            'model' => $model,
                            'attribute' => 'phone',
                            'mask' => '+7(999)999-99-99',
                            'htmlOptions'=>[
                                'class' => 'data-mask form-control',
                                'data-mask' => 'phone',
                                'placeholder' => Yii::t('MailModule.mail', 'Телефон'),
                                'autocomplete' => 'off'
                            ]
                        ]) ?>
					<?php echo $form->error($model, 'phone'); ?>
				</div>

				<?= $form->hiddenField($model, 'verify'); ?>
				<div class="form-bot form-bot-inline">
					<div class="form-captcha">
						<div class="g-recaptcha" data-sitekey="<?= Yii::app()->params['key']; ?>">
						</div>
						<?= $form->error($model, 'verifyCode');?>
					</div>
					<div class="form-button">
						<button type="submit" class="but but-green" data-send="ajax">Заказать ремонт</button>
					</div>
				</div>
				<div class="form-order-repair__termsOfUse terms_of_use">
					* Оставляя заявку вы соглашаетесь с <a target="_blank" href="<?php $this->widget("application.modules.contentblock.widgets.ContentMyBlockWidget", ["id" => 18]); ?>">Условиями обработки персональных данных</a>
				</div>
			</div>
			<?php $this->endWidget(); ?>
		</div>
	</div>
</div>