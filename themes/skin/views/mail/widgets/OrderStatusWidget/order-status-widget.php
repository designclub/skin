<div id="orderStatusModal" class="modal modal-my fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header box-style">
                <div data-dismiss="modal" class="modal-close"><div></div></div>
                <div class="box-style__heading"><strong class="color-green">Статус </strong><strong>заказа</strong></div>
                <!-- <div class="box-style__desc">Осуществляем полный цикл по ремонту  и обслуживанию цифровой техники</div> -->
                <?php /*$this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                    'id' => 6
                ]);*/ ?>
            </div>
                <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
                    'id'=>'orderStatus-form-modal',
                    'type' => 'vertical',
                    'htmlOptions' => ['class' => 'form-my form-order-repair', 'data-type' => 'ajax-form'],
                ]); ?>

                <?php if (Yii::app()->user->hasFlash('order-status-success')) : ?>
                    <div class="modal-success-message">
                        <br>
                        <div class="modal-success-message__result">Статус Вашего заказа: <span><?= Yii::app()->user->getFlash('order-status-success') ?></span></div>
                        <div class="hint-message">
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 19
                        ]); ?>
                        </div>
                    </div>
                    <script>
                        $('.modal-body').hide();
                        $('.modal-footer').hide();
                        setTimeout(function(){
                            $('#orderStatusModal').modal('hide');
                        }, 5000);
                       setTimeout(function(){
                            $('.modal-success-message').remove();
                            $('.modal-body').show();
                            $('.modal-footer').show();
                        }, 6000);
                    </script>
                <?php endif ?>

                <div class="modal-body">
                    <br>
                    <?= $form->textFieldGroup($model, 'code', [
                        'widgetOptions'=>[
                            'htmlOptions'=>[
                                'class' => '',
                                'autocomplete' => 'off'
                            ]
                        ]
                    ]); ?>

                    <?= $form->hiddenField($model, 'verify'); ?>
                    <div class="form-bot form-bot-inline">
                        <div class="form-captcha">
                            <div class="g-recaptcha" data-sitekey="<?= Yii::app()->params['key']; ?>">
                            </div>
                            <?= $form->error($model, 'verifyCode');?>
                        </div>
                        <div class="form-button">
                            <button type="submit" class="but but-green" data-send="ajax">Проверить</button>
                        </div>
                    </div>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>