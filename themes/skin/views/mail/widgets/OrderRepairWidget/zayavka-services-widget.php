<div class="helpRepair-box">
    <div class="helpRepair-box__content">
        <div class="helpRepair-box__header">
            <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                'id' => 13
            ]); ?>
        </div>
        <div class="helpRepair-box__form">
            <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
                'id'=>'order-repair-form',
                'type' => 'vertical',
                'htmlOptions' => ['class' => 'form form-my form-order-repair', 'data-type' => 'ajax-form'],
            ]); ?>
                <?php if (Yii::app()->user->hasFlash('order-repair-success')): ?>
                    <script>
                        $('#messageModal').modal('show');
                        setTimeout(function(){
                            $('#messageModal').modal('hide');
                        }, 4000);
                    </script>
                <?php endif ?>
                <div class="form-order-repair-row">
                    <div class="form-order-repair-row__item">
                        <?= $form->textFieldGroup($model, 'name', [
                            'widgetOptions'=>[
                                'htmlOptions'=>[
                                    'class' => '',
                                    'autocomplete' => 'off'
                                ]
                            ]
                        ]); ?>
                    </div>
                    <div class="form-order-repair-row__item">
                        <?= $form->textFieldGroup($model, 'email', [
                            'widgetOptions'=>[
                                'htmlOptions'=>[
                                    'class' => '',
                                    'autocomplete' => 'off'
                                ]
                            ]
                        ]); ?>
                    </div>
                    <div class="form-order-repair-row__item">
                        <div class="form-group">
                            <?= $form->labelEx($model, 'phone', ['class' => 'control-label']) ?>
                            <?php $this->widget('CMaskedTextFieldPhone', [
                                'model' => $model,
                                'attribute' => 'phone',
                                'mask' => '+7(999)999-99-99',
                                'htmlOptions'=>[
                                    'class' => 'data-mask form-control',
                                    'data-mask' => 'phone',
                                    'placeholder' => Yii::t('MailModule.mail', 'Ваш телефон'),
                                    'autocomplete' => 'off'
                                ]
                            ]) ?>
                            <?php echo $form->error($model, 'phone'); ?>
                        </div>
                    </div>
                </div>

                <?= $form->hiddenField($model, 'verify'); ?>

                <div class="form-order-repair__bottom">
                    <div class="form-order-repair__captcha">
                        <div class="g-recaptcha" data-sitekey="<?= Yii::app()->params['key']; ?>">
                        </div>
                        <?= $form->error($model, 'verifyCode');?>
                    </div>
                    <div class="form-order-repair__button">
                        <button type="submit" class="but but-green" data-send="ajax">Заказать ремонт</button>
                    </div>
                </div>
                <div class="form-order-repair__termsOfUse terms_of_use">
                    * Оставляя заявку вы соглашаетесь с <a target="_blank" href="<?php $this->widget("application.modules.contentblock.widgets.ContentMyBlockWidget", ["id" => 18]); ?>">Условиями обработки персональных данных</a>
                </div>

            <?php $this->endWidget(); ?>
        </div>
    </div>
    <div class="helpRepair-box__media">
        <div class="helpRepair-box__twenty">
            <div class="helpRepair-box__twenty_img">
                <div class="helpRepair-box__twenty_text">
                    <div>Remont Gadget</div>
                    <div>
                        <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                            'id' => 1
                        ]); ?>
                    </div>
                    <div>00:32</div>
                </div>
            </div>
        </div>
    </div>
</div>