<!-- <div class="form-question-box"> -->
    <!-- <div class="container"> -->
        <!-- <div class="top-block">
                    <h2 class="header-text text-center">Остались вопросы</h2>
                    <p class="text-center" style="position: relative;">Заполните форму и мы ответим на все ваши вопросы</p>
                </div>
                 -->        
        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
            'id'=>'order-repair-form',
            'type' => 'inline',
            'htmlOptions' => ['class' => 'form form-my form-order-repair', 'data-type' => 'ajax-form'],
        ]); ?>
        <?php if (Yii::app()->user->hasFlash('order-repair-success')): ?>
        <script>
            $( '#callbackModal' ).modal( 'hide' );
            $( '#messageModal' ).modal( 'show' );
            setTimeout( function () {
                $( '#messageModal' ).modal( 'hide' );
            }, 4000 );
        </script>
        <?php endif ?>

        <div class="form-flex">
            <div class="ico-inputs">
                <div class="ico-input">
                    <span class="icon--5"></span>
                </div>
                <?= $form->textFieldGroup($model, 'email', [
                        'widgetOptions'=>[
                            'htmlOptions'=>[
                                'class' => '',
                                'autocomplete' => 'off',
                                'placeholder' => 'Введите ваш e-mail'
                            ]
                        ]
                    ]); ?>
            </div>

            <div class="form-order-repair__button">
                <?= CHtml::submitButton(Yii::t('MailModule.mail', 'Подписаться'), [
                    'id' => 'callback-button', 
                    'class' => 'btn btn-brown', 
                    'data-send'=>'ajax'
                ]) ?>
            </div>

        </div>

        <?= $form->hiddenField($model, 'verify'); ?>

        <div class="form-order-repair-row form-order-repair__bottom">
            <div class="form-order-repair__captcha">
                <div class="g-recaptcha" data-sitekey="<?= Yii::app()->params['key']; ?>">
                </div>
                <?= $form->error($model, 'verifyCode');?>
            </div>
        </div>

        <?php $this->endWidget(); ?>
        <?php if (Yii::app()->hasModule('contentblock')): ?>
        <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'policy-form']); ?>
        <?php endif; ?>
    <!-- </div> -->
<!-- </div> -->