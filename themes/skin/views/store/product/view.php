<?php

/* @var $product Product */

$this->title = $product->getMetaTitle();
$this->description = $product->getMetaDescription();
$this->keywords = $product->getMetaKeywords();
$this->canonical = $product->getMetaCanonical();

$mainAssets = Yii::app()->getModule( 'store' )->getAssetsUrl();
Yii::app()->getClientScript()->registerScriptFile( $mainAssets . '/js/jquery.simpleGal.js' );

Yii::app()->getClientScript()->registerCssFile( Yii::app()->getTheme()->getAssetsUrl() . '/css/store-frontend.css' );
Yii::app()->getClientScript()->registerScriptFile( Yii::app()->getTheme()->getAssetsUrl() . '/js/store.js' );

$this->breadcrumbs = array_merge(
	[ Yii::t( "StoreModule.store", 'Catalog' ) => [ '/store/product/index' ] ],
	$product->category ? $product->category->getBreadcrumbs( true ) : [], [ CHtml::encode( $product->name ) ]
);

?>

<script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script src="https://yastatic.net/share2/share.js"></script>

<div class="store-container product-container">
	<div class="content-site">
		<div class="breadcrumbs hidden-xs">
			<div class="row">
				<div class="col-xs-12">
					<?php $this->widget(
						'bootstrap.widgets.TbBreadcrumbs',
						[
							'links' => $this->breadcrumbs,
						]
					);?>
				</div>
			</div>
		</div>
		
		<div class="product-panel">
			<div class="row" xmlns="http://www.w3.org/1999/html" itemscope itemtype="http://schema.org/Product">
				<div class="col-sm-6 col-xs-12">
					<div class="thumbnails">
						<div class="image-preview">
							<img src="<?= StoreImage::product($product); ?>" id="main-image" alt="<?= CHtml::encode($product->getImageAlt()); ?>" title="<?= CHtml::encode($product->getImageTitle()); ?>">
						</div>
						<div class="row">
							<div class="col-xs-4 col-md-4 no-padding">
								<a href="<?= $product->getImageUrl(); ?>" class="thumbnail thumbnail-image">
									<img src="<?= $product->getImageUrl(150, 140); ?>"
									 alt="<?= CHtml::encode($product->getImageAlt()); ?>"
									 title="<?= CHtml::encode($product->getImageTitle()); ?>"
									  />
								</a>
							</div>
							<?php foreach ($product->getImages() as $key => $image): { ?>
								<div class="col-xs-4 col-md-4 no-padding">
									<a href="<?= $image->getImageUrl(); ?>" class="thumbnail thumbnail-image">
										<img src="<?= $image->getImageUrl(150, 140); ?>"
											 alt="<?= CHtml::encode($image->alt) ?>"
											 title="<?= CHtml::encode($image->title) ?>"
											 data-option-id="<?= $image->option_color_id; ?>"
											 data-full-img="<?= $image->getImageUrl(); ?>" />
									</a>
								
								</div>
							<?php } endforeach; ?>
						</div>
					</div>
					<div class="row row-video">
						<div class="row-video__block">
							<?php if(!empty($product->link_youtube)): ?>
							<?= CHtml::link(CHtml::image($product->getImageUrl(150, 140, true), CHtml::encode($product->getImageTitle()), ['alt' => CHtml::encode($product->getImageAlt())]), "$product->link_youtube", [
									'class' => 'thumbnail-video',
									'data-fancybox' => ''
								]); ?>
							<?php endif; ?> 
						</div>
					</div>
				</div><!-- end product-images -->
				
				<div class="col-sm-6 col-xs-12 product-data">
					<div class="tittle-block">
						<h1 class="product-title" itemprop="name"><?= CHtml::encode($product->getTitle()); ?></h1>
						<div class="product-sku">Артикул: <span><?= $product->sku; ?></span></div>
					</div>
					
			 	<form action="<?= Yii::app()->createUrl('cart/cart/add'); ?>" method="post">
					<input type="hidden" name="Product[id]" value="<?= $product->id; ?>"/>
					<?= CHtml::hiddenField(
                        Yii::app()->getRequest()->csrfTokenName,
                        Yii::app()->getRequest()->csrfToken
                    ); ?>
					
					<?php if($product->category_id == 1): ?>
						<div class="products-size">
							<span><?= Yii::t("StoreModule.store", "Цена за дм2"); ?>: </span>
							<strong id="js-products-size" data-price="<?= $product->getBasePrice() ?>"><?= $product->getBasePrice() ?></strong>
							<i class="fa fa-rub" aria-hidden="true"></i>
						</div>
					<?php else: ?>
						<div class="products-size">
							<span><?= Yii::t("StoreModule.store", "Цена за шт."); ?>: </span>
							<strong id="js-products-size" data-price="<?= $product->getBasePrice() ?>"><?= $product->getBasePrice() ?></strong>
							<i class="fa fa-rub" aria-hidden="true"></i>
						</div>
					<?php endif; ?>

					<div class="products-attributes">
						<input type="hidden" id="base-price" value="<?= round($product->getResultPrice(), 2); ?>"/>
						<?php 
							$id = 0;
							foreach ($product->getVariantsGroup() as $title => $variantsGroup): 
							$id++;
						?>
							<div class="products-attributes_attribute <?= $title == 'Цвет' ? 'products-attributes_color js-color-variant' : 'products-attributes_size js-size-variant' ?><?= $title == 'Размер (дм)' || $title == 'Размер (м)' ? 'products-attributes_size js-size-variant' : '' ?>">
								<label class = "label-attribute"><?= $title; ?>:</label>
								<div class = "products-sizes id-<?= $product->id ?>">
									<?php  $listData = CHtml::listData($variantsGroup, 'id', 'optionValue');
										echo ExtHtml::radioButtonVariantList(
											"ProductVariant[]-{$id}-{$product->id}",
											current(array_keys($listData)),
											$product->getOptionsList($variantsGroup),
											[
												'itemOptions' => $product->getVariantsOptions(), 
												'container' => '',
												'class' => 'product-quantity-input',
												'separator' => '',									
												'template' => '
													<div class="js-variant-price products-sizes products-sizes_size">
														{input}
														{label}
													</div>'
											]
									); ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
					<div class="product-block-prices-shopcart">
						<div class="block-flex-auto block-prices">
							<div class="price">
								<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
									<div id="result-price" itemprop="price">
										<?php if ($product->getBasePrice() > $product->getMyResultPrice()): ?>
											<span id="product-price-value">
												<?= $product->getBasePrice() ?>
											</span>
										<?php else: ?>
											<span id="product-price-value">
												<?= $product->getMyResultPrice() ?>
											</span>
										<?php endif; ?>
										<i class="fa fa-rub" aria-hidden="true"></i>
									</div>
								</div>
							</div>
						</div>
						<div class="block-flex-auto block-shopcart">
							<button class="btn btn-backet" id="add-product-to-cart" data-loading-text="<?= Yii::t("StoreModule.store", "В корзину"); ?>">
								<?= Yii::t("StoreModule.store", "Add to cart"); ?>
							</button>
							<a href="#" class="btn-review product-info__review-link repeat_captha" data-toggle="modal" data-target="#reviewZayavkaModal">Оставить отзыв</a>
						</div>
					</div><!-- end product-block-prices-shopcart -->
		
					   <?php /*foreach ($product->getVariantsGroup() as $title => $variantsGroup){
					   		echo CHtml::encode($title); 
							echo CHtml::dropDownList(
							   'ProductVariant[]',
							   null,
							   CHtml::listData($variantsGroup, 'id', 'optionValue'),
							   ['empty' => '', 'class' => 'form-control', 'options' => $product->getVariantsOptions()]
						    );
					   }*/ ?>

					   <?php /**$this->widget('application.modules.store.widgets.VariantTreeWidget', ['product' => $product])**/ ?>
					   <?= CHtml::hiddenField('maxCount', 0); ?>      
						 
						<div class="product-description line-bottom">
							<div class="card-tab">
								<input type="radio" name="inset" value="" id="tab_1" checked="">
								<label for="tab_1"><?= Yii::t("StoreModule.store", "Description"); ?></label>
							
								<input type="radio" name="inset" value="" id="tab_2">
								<label for="tab_2">Оплата и доставка</label>

								<input type="radio" name="inset" value="" id="tab_3">
								<label for="tab_3">Отзывы</label>
							
								<div id="txt_1">
									<p><?= $product->description; ?></p>
								</div>
								<div id="txt_2">
									<p>В нашем магазине возможны различные способы оплаты товара. Вариант оплаты вы выбираете на сайте, либо согласовываете с менеджером. При оформлении заказа обязательно указывайте, какой способ доставки вы выбираете</p>
								</div>
								<div id="txt_3">
									<?php $this->widget('application.modules.review.widgets.ReviewNewWidget', [
				                        'product_id' => $product->id,
				                        'view' => 'review'
				                    ]); ?>
								</div>
							</div>
						   <!--  <label><?= Yii::t("StoreModule.store", "Description"); ?></label>
						   <p><?= $product->description; ?></p> -->
						</div>
					</form>
				</div><!-- end product-data -->
			</div>
		</div><!-- end product-panel -->
		<div class="row">
			<div class="linked-products">
				<?php $this->widget('application.modules.store.widgets.LinkedProductsWidget', ['product' => $product, 'code' => null]); ?>
			</div>
		</div><!-- end linked-products -->	
	</div>
</div><!-- end store-container -->

<?php $this->widget('application.modules.review.widgets.ReviewWidget', [
    'product_id' => $product->id,
    'view' => 'reviewmodalwidget'
]); ?>

<?php Yii::app()->getClientScript()->registerScript(
	"product-images",
	<<<JS
		$(".thumbnails").simpleGal({mainImage: "#main-image"});
		$("#myTab li").first().addClass('active');
		$(".tab-pane").first().addClass('active');
JS
); ?>