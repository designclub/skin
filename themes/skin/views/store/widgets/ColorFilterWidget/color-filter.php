<?php $filter = Yii::app()->getComponent('attributesFilter'); ?>
<div data-collapse="persist" id="filter-radio" class="filter-block">
    <div class="filter-block__title"><?= $attribute->title ?></div>
    <div class="filter-block__body">
        <div class="filter-block__list filter-block__list-color">
            <?php foreach ($attribute->options as $option): ?>
                <div class="filter-block__list-item">
                    <?php 
                        echo CHtml::checkBox(
                            $filter->getDropdownOptionName($option),
                            $filter->getIsDropdownOptionChecked($option, $option->id),
                            [
                                'value' => $option->id,
                                'class' => 'checkbox',
                                'id' => 'filter-attribute-' . $option->id
                            ]);

                            $valueLabel = "
                                <span class='check' style=\"background: #{$option->color}\"></span>
                            ";

                         echo CHtml::label($valueLabel, 'filter-attribute-' . $option->id,
                            [
                                'class' => 'checkbox__label',
                            ]) 
                    ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
