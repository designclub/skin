<?php /* @var $dataProvider CActiveDataProvider */ ?>
<?php if ($dataProvider->getTotalItemCount()): ?>
    <hr>
    <div class="store-container">
        <h3 class="title-store text-center"><?= Yii::t('StoreModule.store', 'Linked products') ?></h3>
        <section class="list-category-products">
            <div class="store-panel">
                <?php $this->widget(
                    'zii.widgets.CListView',
                    [
                        'dataProvider' => $dataProvider,
                        'template' => '{items}',
                        'itemView' => '_item',
                        'cssFile' => false,
                        'pager' => false,
                    ]
                ); ?>
            </div>
        </section>
    </div>
<?php endif; ?>