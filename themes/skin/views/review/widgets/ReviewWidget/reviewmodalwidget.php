<div id="reviewZayavkaModal" class="modal review-modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div data-dismiss="modal" class="modal-close">
                    <i class="glyphicon glyphicon-remove"></i>
                </div>
                <div class="modal-heading">
                    <strong class="modal-heading__title">Оставить отзыв</strong><br>
                    Вы можете оставить свой отзыв. <br>Нам очень важно Ваше мнение.
                </div>
            </div>
            
            <?php Yii::app()->user->returnUrl = Yii::app()->request->requestUri;
                $form = $this->beginWidget(
                    'bootstrap.widgets.TbActiveForm',
                    array(
                        // 'action'      => Yii::app()->createUrl('/review/create/'),
                        'id'          => 'review-form',
                        'type'        => 'vertical',
                        'htmlOptions' => [
                            'class' => 'form-my form-modal', 
                            'data-type' => 'ajax-form',
                            // 'enctype' => 'multipart/form-data'
                        ],        
                    )
                ); ?>

                <?php if (Yii::app()->user->hasFlash('review-success')): ?>
                    <script>
                        $('#reviewZayavkaModal').modal('hide');
                        $('#reviewModal').modal('show');
                        setTimeout(function(){
                            $('#reviewModal').modal('hide');
                        }, 5000);
                    </script>
                <?php endif ?>

                <div class="modal-body">
                    <?= $form->textFieldGroup($model, 'username', [
                        'widgetOptions' => [
                            'htmlOptions' => [
                                'data-original-title' => $model->getAttributeLabel('username'),
                                'data-content'        => $model->getAttributeDescription('username'),
                                'autocomplete' => 'off'
                            ],
                        ],
                    ]); ?>

                    <?= $form->textAreaGroup($model, 'text', [
                        'widgetOptions' => [
                            'htmlOptions' => [
                                'data-original-title' => $model->getAttributeLabel('text'),
                                'data-content'        => $model->getAttributeDescription('text'),
                                'autocomplete' => 'off'
                            ],
                        ],
                    ]); ?>

                    <div class="form-bot">
                        <div class="form-captcha">
                            <?= $form->error($model, 'verifyCode');?>
                        </div>
                        <div class="form-button">
                            <button class="review-modal__link but btn-backet" id="reviewZayavka-button" data-send="ajax">Оставить отзыв</button>
                        </div>
                    </div>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>

<div id="reviewModal" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header box-style">
                <div data-dismiss="modal" class="modal-close">
                    <i class="glyphicon glyphicon-remove"></i>
                </div>
                <div class="box-style__header">
                    <div class="box-style__heading">
                        Уведомление
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="modal-success-message">
                    Ваша отзыв успешно отправлен.
                </div>
            </div>
        </div>
    </div>
</div>



