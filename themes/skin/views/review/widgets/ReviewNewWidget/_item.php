<div class="review-box__item">
	
	<div class="review-box__name">
		<span><?= CHtml::encode( $data->username); ?></span>
        <span class="review-box__date">
            <?= date("d.m.Y", strtotime($data->date_created)); ?>
        </span>
	</div>
	<div class="review-box__raiting review-raiting">
        <div class="product-info__review rating-list">
            <div class="rating-list__icons">
                <?php for ($i=1; $i <= 5; $i++) : ?>
                    <div class="rating-list__item <?= ($i <= $data->rating) ? 'active' : ''; ?>">
                        <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/rating.svg'); ?>
                    </div>
                <?php endfor; ?>
            </div>
            <div class="rating-list__number">
                <?= $data->rating ?>.0/5.0
            </div>
        </div>
	</div>
    <div class="review-box__description">
            <?= $data->text; ?>
        </div>
	<?php $strLength = 430; ?>
<!--     <?php if ($strLength < strlen(strip_tags($data->text))) : ?>
        <div class="review-box__description">
            <div class="review-box__description_short">
            <?= mb_substr(strip_tags($data->text), 0, 430, 'UTF-8') . "..."; ?>
            </div>
        <div class="review-box__description_full hidden">
            <?= $data->text; ?>
        </div>
    </div>
        <div class="review-box__bottom">
            <a class="review-box__link but-link" data-texthidden="Свернуть отзыв" data-textvisible="Читать весь отзыв" href="#">Читать весь отзыв</a>
        </div>
<?php else : ?>
    <div class="review-box__description">
        <?= $data->text; ?>
    </div>
<?php endif; ?> -->
</div>
