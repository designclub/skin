<div class="modal-body">
    <button type="button" class="product-modal__close close" data-dismiss="modal"
            aria-label="Закрыть">
            <i class="icon-close"></i>
    </button>
    <?php $this->widget('application.components.FtListView', [
        'id' => 'review-box',
        'itemView' => '_item',
        'dataProvider' => $dataProvider,
        'itemsCssClass' => 'review-box',
        'template' => '{items}{pager}',
        'htmlOptions' => [
            // "class" => "user-specialist"
        ],
        'pagerCssClass' => 'pagination-box',
        'emptyText' => 'У данного товара нет отзывов',
        'emptyTagName' => 'div',
        'emptyCssClass' => 'empty-form',
        'pager' => [
            'class' => 'application.components.ShowMorePager',
            'buttonText' => 'Показать еще',
            'wrapTag' => 'div',
            'htmlOptions' => [
                'class' => 'but'
            ],
            'wrapOptions' => [
                'class' => 'review-pagination'
            ],
        ]
    ]); ?>
</div>
