<div class="review-box">
	<?php if(!empty($review)): ?>
		<?php foreach ($review as $key => $data) : ?>
			<?php Yii::app()->controller->renderPartial('//review/review/_item', ['data' => $data]) ?>
		<?php endforeach; ?>
	<?php else: ?>
		<p>Отзывы отсутствуют</p>
	<?php endif; ?>
</div>
