<?php
/**
 * Отображение для ./themes/default/views/news/news/news.php:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     https://yupe.ru
 *
 * @var $this NewsController
 * @var $model News
 **/
?>
<?php
$this->title = $model->meta_title ?: $model->title;
$this->description = $model->meta_description;
$this->keywords = $model->meta_keywords;
?>

<?php
$this->breadcrumbs = [
    Yii::t('NewsModule.news', 'News') => ['/news/news/index'],
    $model->title
];
?>

<div class="page-txt page-site news-page">
    <h1 class="text-left"><?= CHtml::encode($model->title); ?></h1>
    <div class="block-news">
        <div class="img-news">
            <?php if ($model->image): ?>
                <?= CHtml::image($model->getImageUrl(550, 400), $model->title, ['class' => 'img-responsive']); ?>
            <?php endif; ?>
        </div>
        <div class="text-news">
            <p> <?= $model->full_text; ?></p>
        </div>
    </div>
</div>
