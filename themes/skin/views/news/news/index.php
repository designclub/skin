<?php
$this->title = Yii::t('NewsModule.news', 'News');
$this->breadcrumbs = [Yii::t('NewsModule.news', 'News')];
?>
<div class="content-site">
    <div class="page-txt page-without-back">
        <div class="panel-news">
            <h1><?= Yii::t('NewsModule.news', 'News') ?></h1>
            <?php $this->widget(
                'bootstrap.widgets.TbListView',
                [
                    'dataProvider' => $dataProvider,
                    'template' => '{items}',
                    'itemView' => '_item',
                ]
            ); ?>
        </div>
    </div>
</div>