<?php
/* @var $data News */
?>
<div class="block-news <?= ($data->category_id == 2) ? 'news' : 'comment' ; ?>">
    <?php if($data->category_id == 2): ?>
        <div class="image-news col-xs-12">
            <?= CHtml::link(CHtml::image($data->getImageUrl(450, 300, true)), Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug])); ?>
        </div>
        <div class="title-news col-xs-12">
            <span><?= Yii::app()->dateFormatter->format("dd.MM.yyyyг.", $data->date); ?></span>
            <h4>
                <?= CHtml::link($data->title, Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug])); ?>
            </h4>
            <button class="btn btn-white btn-news"><?= CHtml::link('Читать далее', Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]));?></button>
        </div>
    <?php elseif($data->category_id == 1): ?>
        <div class="image-comment">
            <?= CHtml::image($data->getImageUrl(100, 100, true)); ?>
        </div>
        <div class="text-comment">
            <label><?= $data->title; ?></label>
            <?= $data->full_text; ?>
        </div>
    <?php endif; ?>
</div>