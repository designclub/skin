<?php
/**
 * @var Category $category
 */

$this->title = $category->name;
$this->breadcrumbs = [
    Yii::t('NewsModule.news', 'News') => ['/news/news/index'],
    Yii::t('NewsModule.news', 'News categories') => ['/news/newsCategory/index'],
    $category->name,
];
?>
<div class="page-txt page-site">
    <h1><?=  $category->name; ?></h1>

    <div class="row news-list">
        <?php $this->widget(
            'bootstrap.widgets.TbListView',
            [
                'dataProvider' => $dataProvider,
                'template' => '{items}',
                'itemsCssClass' => 'panel-list-news',
                'itemView' => '//news/news/_item',
            ]
        ); ?>
    </div>
    
</div>