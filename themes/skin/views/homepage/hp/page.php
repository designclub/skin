<?php
/** @var Page $page */

if ($page->layout) {
    $this->layout = "//layouts/{$page->layout}";
}

$this->title = $page->title;
$this->breadcrumbs = [
    Yii::t('HomepageModule.homepage', 'Pages'),
    $page->title
];
$this->description = !empty($page->meta_description) ? $page->meta_description : Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = !empty($page->meta_keywords) ? $page->meta_keywords : Yii::app()->getModule('yupe')->siteKeyWords
?>

<div class="slider">
    <?php $this->widget("application.modules.gallery.widgets.SlickWidget", ['galleryId' => 1,
        'clietOptions' => [
            'prevArrow' => 'js:$(".prev-slider-arrow")',
            'nextArrow' => 'js:$(".next-slider-arrow")',
            'dotsClass' => 'container dots-slider',
            'dots' => true,
            'arrows' => false,
            'responsive' => [
                [
                    'breakpoint' => 767,
                    'settings'=> [
                        'arrows' => false
                    ]
                ],
            ]
        ]
    ]); ?>
</div>




<div class="panel-categories-block">
    <div class="content-site">
        <div class="categories-list">
            <div class="carousel-store-categories">
                <h2 class="text-center">Популярные категории</h2>
                <?php $this->widget(
                    "application.modules.gallery.widgets.SlickCategoriesStoreWidget",
                    [
                    'clietOptions' => [
                        'autoplay' => false,
                        'prevArrow' => 'js:$(".prev-store-arrow")',
                        'nextArrow' => 'js:$(".next-store-arrow")',
                        'dotsClass' => 'dots-slider dots-categories',
                        'dots' => false,
                        'arrows' => true,
                        'slidesToShow' => 4], 
                    'categoryParent' => [1,6,7,8,9],
                    /*'responsive' => [
                        'breakpoint' => 767,
                        'settings' => [
                            'slidesToShow' => 2
                        ]
                    ],*/
                    ]
                ); ?>
            </div>
            <!-- <div class="row">
                <div class="btn-block text-center">
                    <?= CHtml::link('Подробнее', '/store', ['class' => 'btn btn-white']); ?>
                </div>
            </div> -->
        </div>
    </div>
</div><!-- end panel-categories-block -->



<div class="history">
    <div class="history-block flex-item">
            <?php if (Yii::app()->hasModule('contentblock')) : ?>
                <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'history']); ?>
            <?php endif; ?>
    </div>

    <div class="reviews-block flex-item">
        <div class="reviews-panel">
            <?php $this->widget("application.modules.news.widgets.CategoryNewsWidget", ['alias' => 'otzyvy']); ?>
        </div>
    </div>
</div><!-- end history -->


<div class="advantages">
     <div class="content-site">
        <div class="heading">
            <h2>Преимущества</h2>
        </div>
        <div class="row">
            <?php if (Yii::app()->hasModule('contentblock')) : ?>
                <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'advantages']); ?>
            <?php endif; ?>
        </div>
    </div>
</div><!-- end advantages -->

<!-- Галерея Prima Pelle -->
<div class="gallery">
    <div class="content-site">
        <?php $this->widget('application.modules.gallery.widgets.GalleryWidget', [
            'view' => 'prima-pelle',
            'galleryId' => 2,
            'limit' => 6,
        ]); ?>
    </div>
</div>

<div class="subscription">
    <div class="content-site">
        <div class="subscription-block">
            <div class="subscription-heading">
                <?php if (Yii::app()->hasModule('contentblock')) : ?>
                    <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'subscription-head']); ?>
                <?php endif; ?>
            </div>
            <div class="contact-content-block">
                <?php $this->widget('application.modules.mail.widgets.OrderRepairWidget'); ?>
            </div>
        </div>
    </div>
</div><!-- end subscription -->


