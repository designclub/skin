<?php
    $this->breadcrumbs = [
        Yii::t('OrderModule.order', 'Мои заказы') => ['/order/clientBackend/view'],
        Yii::t('OrderModule.order', 'Просмотр'),
    ];
    $this->title = Yii::t('OrderModule.order', 'Мои заказы');
    $this->layout = "//layouts/user";
?>

<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <?php
                $this->widget(
                    'bootstrap.widgets.TbExtendedGridView',
                    [
                        'id' => 'order-grid',
                        'type' => 'condensed',
                        'dataProvider' => $orders,
                        'template' => '{items}{pager}',
                        'columns' => [
                            [
                                'name' => 'id',
                                'htmlOptions' => ['width' => '90px'],
                                'type' => 'raw',
                                'value' => function ($data) {
                                    return CHtml::link($data->id, ['/order/order/clientOrder', 'id' => $data->id]);
                                },
                            ],
                            [
                                'name' => 'date',
                                'type' => 'html',
                                'filter' => $this->widget('booster.widgets.TbDatePicker', [
                                    'model' => $order,
                                    'attribute' => 'date',
                                    'options' => [
                                        'format' => 'yyyy-mm-dd',
                                    ],
                                    'htmlOptions' => [
                                        'class' => 'form-control',
                                    ],
                                ], true),
                                'value' => function ($data) {
                                    return CHtml::link(Yii::app()->getDateFormatter()->formatDateTime($data->date,
                                        'short',
                                        false), ['/order/order/clientOrder', 'id' => $data->id]);
                                },
                            ],
                            [
                                'name' => 'total_price',
                                'value' => function ($data) {
                                    return Yii::app()->getNumberFormatter()->formatCurrency($data->total_price,
                                        Yii::app()->getModule('store')->currency);
                                },
                            ],
                            [
                                'name' => 'status_id',
                                'value' => '$data->status->name',
                            ],
                            [
                                'name' => 'paid',
                                'value' => '$data->getPaidStatus()',
                            ],
                            [
                                'name' => 'delivery_id',
                                'header' => Yii::t('OrderModule.order', 'Delivery'),
                                'filter' => CHtml::listData(Delivery::model()->findAll(), 'id', 'name'),
                                'value' => function ($data) {
                                    return $data->delivery->name;
                                },
                            ],
                        ],
                    ]
                ); ?>
            </div>
        </div>
    </div>
</div>