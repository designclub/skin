<?php if (!Yii::app()->user->isGuest) : ?>
<button type="button" class="btn btn-brown btn-chat" data-toggle="modal" data-target="#chat-modal"><label>Онлайн-чат</label><span class="badge badge-defautl js-quantity-message"><?= $noRead ?></span></button>

<div class="modal fade" id="chat-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
                'id' => 'chat-form',
                'type' => 'inline',
                'htmlOptions' => [
                    'data-target' => '#chat-list',
                    'data-user-from' => $model->user_from,
                    'data-user-to' => $model->user_to,
                ]
            ]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Онлайн-переписка с администратором сайта</h4>
            </div>
            <div class="modal-body">
            
                <div class="panel-chat">
                    <div class="panel-chat_messages">
                        <?php $this->widget('application.modules.chat.components.FtListView', [
                            'id' => 'chat-list',
                            'dataProvider' => $model->search(),
                            'template' => '{items}',
                            'itemView' => 'chat-message',
                            'emptyText' => 'Пока нет сообщений',
                            'htmlOptions' => [
                                "class" => "message-chat-content",
                            ],
                            'pager' => [
                                'class' => 'application.modules.chat.components.ShowMorePager',
                                'buttonText' => 'Показать больше',
                                // 'wrapTag' => 'div',
                                // 'htmlOptions' => [
                                //     'class' => 'choiceOfSpecialist-link but but-yellow'
                                // ],
                                // 'wrapOptions' => [
                                //     'class' => 'choiceOfSpecialist-button'
                                // ],
                            ]
                        ]); ?>
                </div>
                <div class="panel-chat_form">
                    <?= $form->textAreaGroup($model, 'message') ?>
                    <?= CHtml::submitButton('Отправить', ['class' => 'btn btn-send']) ?>
                    </div>
                </div>
                
            </div>

            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
<?php endif ?>
