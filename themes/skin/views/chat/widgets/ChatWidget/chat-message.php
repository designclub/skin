<div class="message-chat-box message-chat-box-<?= $data->user_from === Yii::app()->user->id ? 'from' : 'to' ?> message-chat-box-js" id="message-chat-box-<?= $data->id ?>">
    <div class="message-chat-box__item">
        <div class="message-chat-box__img">
        </div>
        <div class="message-chat-box__info message-chat">
            <div class="message-chat__body">
                <div class="message-chat__header message-chat-header">
                    <?php if ($data->user_from===Yii::app()->user->id) : ?>
                        <div class="message-chat-header__remove">
                            <!-- <?= CHtml::link('Удалить', ['/user/chat/delete', 'id' => $data->id], [
                                'class' => 'message-chat-header__delete delete-message-js',
                            ]) ?> -->
                        </div>
                    <?php endif ?>
                    <div class="message-chat-header__info">
                        <div class="message-chat-header__name"><label><?= $data->userFrom->getFullName() ?></label></div>
                        <div class="message-chat-header__time"><span><?= Yii::app()->dateFormatter->format("dd.MM.yyyyг. HH:mm", $data->create_at) ?></span></div>
                    </div>
                </div>
                <div class="message-chat__text"><?= $data->message ?></div>
            </div>
        </div>
    </div>
</div>