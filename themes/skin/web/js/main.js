(function( $ ){
    // Плагин лоадер
    var methods = {
        show : function(elem) {
            this
            .addClass('form-loader');
        },
        hide : function(elem) {
            this
            .removeClass('form-loader');
        }
    };

    $.fn.loader = function( method ) {
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        }
    };
    // Плагин лоадер end
})( jQuery );

$(window).load(function() {
    // $("#loading").delay(1000).fadeOut(500);
    setTimeout(function(){
        $(".category-tabs-content .category-tabs-pane:eq(0)").removeClass('category-tabs-loading');
    }, 100);
});

$(document).ready(function(){
    /*
    Ajax на формах
    */
    $('form[data-type="ajax-form"]').on('click', function(event) {
        var elem = event.target;
        var dataSend = elem.getAttribute('data-send');
        if (dataSend=='ajax') {
            var button   = $(elem),
                form     = button.parents('form'),
                type     = form.attr('method'),
                formId   = form.attr('id');

            form.loader('show');

            $.ajax({
                type: type,
            data:  form.serialize(),
            dataType: 'html',
            success: function(html) {
                var newForm = $(html)
                .find('#'+formId);

                form.loader('hide');
                $('#'+formId).html(newForm.html());
                
                if($('#'+formId).find('input[data-mask="phone"]').is('.data-mask')){
                    $('#'+formId).find('input[data-mask="phone"]')
                    .mask("+7(999)999-99-99", {
                        'placeholder':'_',
                        'completed':function() {
                                    //console.log("ok");
                                }
                            });
                }

                $.getScript("https://www.google.com/recaptcha/api.js", function () {});
            },
        });
            return false;
        }
    });
});