-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Мар 24 2020 г., 11:33
-- Версия сервера: 5.7.28
-- Версия PHP: 7.0.33-0+deb9u7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `skin`
--

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_blog_blog`
--

CREATE TABLE `mvc_blog_blog` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `icon` varchar(250) NOT NULL DEFAULT '',
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `member_status` int(11) NOT NULL DEFAULT '1',
  `post_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_blog_post`
--

CREATE TABLE `mvc_blog_post` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `publish_time` int(11) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `quote` text,
  `content` text NOT NULL,
  `link` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `comment_status` int(11) NOT NULL DEFAULT '1',
  `create_user_ip` varchar(20) NOT NULL,
  `access_type` int(11) NOT NULL DEFAULT '1',
  `meta_keywords` varchar(250) NOT NULL DEFAULT '',
  `meta_description` varchar(250) NOT NULL DEFAULT '',
  `image` varchar(300) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_blog_post_to_tag`
--

CREATE TABLE `mvc_blog_post_to_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_blog_tag`
--

CREATE TABLE `mvc_blog_tag` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_blog_user_to_blog`
--

CREATE TABLE `mvc_blog_user_to_blog` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `note` varchar(250) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_category_category`
--

CREATE TABLE `mvc_category_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_category_category`
--

INSERT INTO `mvc_category_category` (`id`, `parent_id`, `slug`, `lang`, `name`, `image`, `short_description`, `description`, `status`) VALUES
(1, NULL, 'otzyvy', 'ru', 'Отзывы', NULL, '', '', 1),
(2, NULL, 'novosti', 'ru', 'Новости', NULL, '', '', 1),
(3, NULL, 'nashi-video', 'ru', 'Наши видео', NULL, '', '', 1),
(4, NULL, 'galerei-izobrazheniy', 'ru', 'Галереи изображений', NULL, '', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_chat_chat`
--

CREATE TABLE `mvc_chat_chat` (
  `id` int(11) NOT NULL,
  `user_from` int(11) DEFAULT NULL,
  `message` text,
  `status` int(11) DEFAULT '0',
  `create_at` int(11) DEFAULT NULL,
  `user_to` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_chat_chat`
--

INSERT INTO `mvc_chat_chat` (`id`, `user_from`, `message`, `status`, `create_at`, `user_to`) VALUES
(3, 6, 'Проверка чата', 0, 1581672385, 4),
(4, 4, 'Администратор - чат работает', 0, 1581672416, 6),
(5, 4, 'Nohup асинхронный запуск', 0, 1581672585, 6),
(6, 6, 'nohup', 0, 1581672612, 4),
(7, 6, 'test', 0, 1581672708, 4),
(8, 4, 'nohup', 0, 1581672729, 6),
(9, 4, 'тук\r\n', 0, 1583207851, 18),
(10, 4, 'cner\r\n\r\n', 0, 1583207886, 18),
(11, 18, 'Пошел ты', 0, 1583209234, 4),
(12, 18, 'ага\r\n', 0, 1583209531, 4),
(13, 4, 'пишет тебе \r\n', 0, 1583209843, 18);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_comment_comment`
--

CREATE TABLE `mvc_comment_comment` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  `root` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_comment_comment`
--

INSERT INTO `mvc_comment_comment` (`id`, `parent_id`, `user_id`, `model`, `model_id`, `url`, `create_time`, `name`, `email`, `text`, `status`, `ip`, `level`, `root`, `lft`, `rgt`) VALUES
(1, NULL, 4, 'Order', 4, '', '2020-03-03 09:18:42', '', '', '', 1, '94.41.170.128', 1, 1, 1, 4),
(2, NULL, 4, 'Order', 4, '', '2020-03-03 09:18:42', 'admin', 'dekanrector@mail.ru', 'Ваш заказ принят\nПлати сюда', 1, '94.41.170.128', 2, 1, 2, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_contentblock_content_block`
--

CREATE TABLE `mvc_contentblock_content_block` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `content` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AVG_ROW_LENGTH=1820 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_contentblock_content_block`
--

INSERT INTO `mvc_contentblock_content_block` (`id`, `name`, `code`, `type`, `content`, `description`, `category_id`, `status`) VALUES
(1, 'logo', 'logo', 3, '<p><a href=\"/\"><img src=\"/uploads/image/2088404b88ca1c11b5535a1ff4c66c12.png\"></a>\r\n</p>', '', NULL, 1),
(2, 'email', 'email', 3, '<p><a href=\"mailto:primapelleoren@gmail.com\" class=\"email\">primapelleoren@gmail.com</a>\r\n</p>', '', NULL, 1),
(3, 'phone', 'phone', 3, '<p><a href=\"tel:+7(3532) 43-10-18\">+7(3532) 43-10-18</a>\r\n</p>', '', NULL, 1),
(4, 'advantages', 'advantages', 3, '<div class=\"col-sm-4\">\r\n	<div class=\"advantages-item\">\r\n		<div class=\"text-block\">\r\n			<label class=\"one-advantages\">Стабильность</label>\r\n			<p>7 лет честной  и ответственной работы\r\n			</p>\r\n		</div>\r\n	</div>\r\n</div>\r\n<div class=\"col-sm-4\">\r\n	<div class=\"advantages-item\">\r\n		<div class=\"text-block\">\r\n			<label class=\"two-advantages\">Оптимальный выбор</label>\r\n			<p>Ваши запросы растут – мы тоже)! За 7 лет мы увеличили наши поставки более чем в 30 раз. Мы стремимся сделать наш ассортимент разнообразным,  чтобы в одном магазине вы могли приобрести качественную итальянскую кожу, фурнитуру и химию\r\n			</p>\r\n		</div>\r\n	</div>\r\n</div>\r\n<div class=\"col-sm-4\">\r\n	<div class=\"advantages-item\">\r\n		<div class=\"text-block\">\r\n			<label class=\"three-advantages\">Качество</label>\r\n			<p>Работаем только с  проверенными  фабриками и поставщиками  кожи\r\n			</p>\r\n		</div>\r\n	</div>\r\n</div>\r\n<div class=\"col-sm-4\">\r\n	<div class=\"advantages-item\">\r\n		<div class=\"text-block\">\r\n			<label class=\"four-advantages\">Доверие</label>\r\n			<p>Нам доверяют более 2000 клиентов по России, Казахстану, Белоруссии\r\n			</p>\r\n		</div>\r\n	</div>\r\n</div>\r\n<div class=\"col-sm-4\">\r\n	<div class=\"advantages-item\">\r\n		<div class=\"text-block\">\r\n			<label class=\"five-advantages\">Честность и информативность</label>\r\n			<p>Мы перед вами честны и открыты, только у нас вы найдете подробное описание позиций, реальные фото и видео\r\n			</p>\r\n		</div>\r\n	</div>\r\n</div>\r\n<div class=\"col-sm-4 \">\r\n	<div class=\"advantages-item\">\r\n		<div class=\"text-block\">\r\n			<label class=\"six-advantages\">Уважение</label>\r\n			<p>Мы уважаем наших клиентов и стремимся к тому, чтобы кожевенное дело в России вышло на престижный и почитаемый уровень\r\n			</p>\r\n		</div>\r\n	</div>\r\n</div>', '', NULL, 1),
(5, 'subscription-head', 'subscription-head', 3, '<div class=\"heading\">\r\n	<h2>Будьте в курсе всех событий от Prima Pelle</h2>\r\n</div><p><label>Мы расскажем о новинках и свежих предложениях нашего <br> магазина, а также поведаем о трендах и тенденциях</label>\r\n</p>', '', NULL, 1),
(6, 'policy-form', 'policy-form', 3, '<div class=\"checkbox\">\r\n	<input type=\"checkbox\" checked=\"\" id=\"check\"><label for=\"check\">Я согласен на обработку <a href=\"/\">персональных данных</a></label>\r\n</div>', '', NULL, 1),
(7, 'socnetwork', 'socnetwork', 3, '<p>Мы в социальных сетях\r\n</p><ul>\r\n	<li><a href=\"https://vk.com/chaizara\" target=\"_blank\"><i class=\"fa fa-vk\" aria-hidden=\"true\"></i></a></li>\r\n	<li><a href=\"https://www.instagram.com/prima_pelle/\" target=\"_blank\"><i class=\"fa fa-instagram\" aria-hidden=\"true\"></i></a></li>\r\n	<li><a href=\"https://www.facebook.com/PrimaPelleorenburg\" target=\"_blank\"><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a></li>\r\n</ul>', '', NULL, 1),
(8, 'policy', 'policy', 3, '<a href=\"/\">Все права защищены</a>', '', NULL, 1),
(9, 'История', 'history', 3, '<div class=\"history-panel\">\r\n	<div class=\"heading\">\r\n		<h2>Наша история</h2>\r\n	</div>\r\n	<div class=\"history-img\">\r\n		<img src=\"/uploads/image/1d1e279d057af79c0e0ad929f733857c.jpg\" style=\"background-color: initial;\">\r\n	</div>\r\n	<div class=\"history-name\">\r\n		<label>Марина Павлова</label>\r\n		руководитель компании\r\n	</div>\r\n	<div class=\"history-description\">\r\n		<p>С другой стороны сложившаяся структура организации позволяет выполнять важные задания по разработке существенных финансовых и административных условий. Разнообразный и богатый опыт начало повседневной\r\n		</p>\r\n	</div>\r\n	<div class=\"history-btn\">\r\n		<button class=\"btn btn-white\"><a href=\"/istoriya\">Читать далее</a></button>\r\n	</div>\r\n</div>', '', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_dictionary_dictionary_data`
--

CREATE TABLE `mvc_dictionary_dictionary_data` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `code` varchar(100) NOT NULL,
  `name` varchar(250) NOT NULL,
  `value` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL DEFAULT '',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_dictionary_dictionary_group`
--

CREATE TABLE `mvc_dictionary_dictionary_group` (
  `id` int(11) NOT NULL,
  `code` varchar(100) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL DEFAULT '',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_feedback_feedback`
--

CREATE TABLE `mvc_feedback_feedback` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `answer_user` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone` varchar(150) DEFAULT NULL,
  `theme` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  `answer_time` datetime DEFAULT NULL,
  `is_faq` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_gallery_gallery`
--

CREATE TABLE `mvc_gallery_gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `owner` int(11) DEFAULT NULL,
  `preview_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_gallery_gallery`
--

INSERT INTO `mvc_gallery_gallery` (`id`, `name`, `description`, `status`, `owner`, `preview_id`, `category_id`, `sort`) VALUES
(1, 'Слайдер ', '<p>Слайдер </p>', 1, NULL, NULL, NULL, 1),
(2, 'Галерея Prima Pelle', '<p>Галерея Prima Pelle</p>', 1, 4, NULL, NULL, 1),
(3, 'Портфель', '<p>Иногда проще = сложнее.</p><p>Казалось бы, ничего необычного, но когда орудуешь всего несколькими прямыми линиями, то испортить всю композицию можно промахнувшись всего на сантиментр, особенно когда зажат в строгие рамки ТЗ.</p><p>Если сложно что-то придумать с формой, то можно поиграть с фактурой!</p><p>И тут снова на помощь пришла матовая фактурная Etrusco black classic в сочетании с глянцевой покрывной кожей и нежной замшей внутри.</p><p>Кожа, как и всегда, вышка!</p><p>Пысы: там если рулончик приедет, то отложите пожалуйста;)</p>', 1, 6, NULL, NULL, 2),
(4, 'Сумка ', '<p>Дошли руки до Etrusco cognac.<br />Внутри персиковая замша.<br />Прошивка комбинированная машинка и ручная.<br />Фактура кожи - просто сказка! Уже представляю, какой \"дорогущей\" она станет через год использования;)<br />Снова спасибо всей команде магазина!<br />Везите ещё эту кожу, на очереди рюкзак и предзаказы ещё)</p>', 1, 6, NULL, NULL, 3),
(5, 'Сумка на длинной ручке', '<p>La Perla Azzurra Dakota tan, благодарю за отличную кожу!</p>', 1, 6, NULL, NULL, 4),
(6, 'Ремни женские', '<p>Спасибо за действительно шикарную кожу ! За профессиональное общение! За бонусы подарки ! За настроение и вдохновение ! На сегодня подборка моих ремней )) P.S. Мама в декрете</p>', 1, 6, NULL, NULL, 5),
(7, 'Сумка черная', '<p>Стас меня стас! Ой, спас!:)<br />Тот случай, когда запорол заказ, а кожа стоковая и найти такую же почти невозможно, но Стас прошёлся по своему огромному складу и отыскал!<br />Заказ спасён:)<br /><br />Безымянная икра с финишем, на деталях Etrusco black, подклад джинса (так захотелось заказчице)</p>', 1, 6, NULL, NULL, 6),
(8, 'Портмоне', '<p>Портмоне из кожи Ithaca Lucido и Nizza, спасибо за большой выбор и качество</p>', 1, 6, NULL, NULL, 7),
(9, 'Портфель коричневый', '<p>Etrusco dark brown.<br />Кожа, в которую влюбился!)</p>', 1, 6, NULL, NULL, 8),
(10, 'Аксессуары', '<p>Работы из кожи купленной в вашем магазине. Спасибо за качество!!!</p>', 1, 6, NULL, NULL, 9),
(11, 'Портмоне из кожи vacchetta firenze', '<p>Кожа ваша, работа моя.<br />Отличная кожа, чувствуется - породистая была коровка)</p>', 1, 6, NULL, NULL, 10),
(12, 'Коллекция кошельков', '<p>Благодарю за отличную кожу!<br />Целая коллекция получилась!</p>', 1, 6, NULL, NULL, 11),
(13, 'Портфель мужской', '<p>В очередной раз благодарю за прекрасную кожу, индивидуальный подход и терпение, ведь когда я выбираю кожу, вопросов сыплю миллион. Кожа растительного дубления двух толщин, шить было одно удовольствие! А запах гипнотизировал меня, теперь гипнотизирует владельца портфел\n</p>', 1, 6, NULL, NULL, 12),
(14, 'Ремешки для часов', '<p>Прекрасная кожа от Prima_pelle<br />Артикул Verta Fieno<br />Стас и Марина, привет дорогие и респект вам</p>', 1, 6, NULL, NULL, 13),
(15, 'Ремень мужской', '<p>Какая все-таки хорошая кожа, инструменты так и пляшут от восторг. Спасибо большое. За бонус отдельное спасибо, прям в точку попали. Как раз такой размерчик нужен был</p>', 1, 6, NULL, NULL, 14),
(16, 'Ремень из кожи Dakota Stout', '<p>La Perla Azzurra, Dakota Stout)<br />Prima Pelle спасибо)</p>', 1, 6, NULL, NULL, 15),
(17, 'Ремешок для часов из темной кожи', '<p>Папайя рулит. А если ее ещё и украсить. Как вам такой эксперимент? И ещё как вы считаете этот ремешок женский или унисекс?</p>', 1, 6, NULL, NULL, 16),
(18, 'Баннер на мобильных', '<p>Баннер на мобильных</p>', 1, 4, NULL, NULL, 17);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_gallery_image_to_gallery`
--

CREATE TABLE `mvc_gallery_image_to_gallery` (
  `id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_gallery_image_to_gallery`
--

INSERT INTO `mvc_gallery_image_to_gallery` (`id`, `image_id`, `gallery_id`, `create_time`, `position`) VALUES
(4, 11, 1, '2019-10-01 17:35:52', 1),
(5, 12, 1, '2019-10-01 17:35:53', 2),
(6, 13, 1, '2019-10-01 17:35:54', 3),
(17, 28, 2, '2019-10-09 14:48:57', 4),
(18, 29, 2, '2019-10-09 14:48:57', 5),
(19, 30, 2, '2019-10-09 14:48:58', 6),
(20, 31, 2, '2019-10-09 14:48:58', 7),
(21, 32, 2, '2019-10-09 14:48:59', 8),
(22, 33, 2, '2019-10-09 14:48:59', 9),
(23, 34, 3, '2019-11-05 10:43:28', 10),
(24, 35, 3, '2019-11-05 10:43:29', 11),
(25, 36, 3, '2019-11-05 10:43:29', 12),
(26, 37, 3, '2019-11-05 10:43:29', 13),
(27, 38, 3, '2019-11-05 10:43:29', 14),
(28, 39, 3, '2019-11-05 10:43:30', 15),
(36, 47, 4, '2019-11-05 14:09:09', 16),
(37, 48, 4, '2019-11-05 14:09:09', 17),
(38, 49, 4, '2019-11-05 14:09:09', 18),
(39, 50, 4, '2019-11-05 14:09:10', 19),
(40, 51, 4, '2019-11-05 14:09:10', 20),
(41, 52, 4, '2019-11-05 14:09:11', 21),
(42, 53, 4, '2019-11-05 14:09:12', 22),
(43, 54, 5, '2019-11-05 14:18:47', 23),
(44, 55, 5, '2019-11-05 14:18:47', 24),
(45, 56, 5, '2019-11-05 14:18:48', 25),
(46, 57, 5, '2019-11-05 14:18:48', 26),
(47, 58, 6, '2019-11-05 14:22:33', 27),
(48, 59, 6, '2019-11-05 14:28:10', 28),
(49, 60, 6, '2019-11-05 14:28:11', 29),
(50, 61, 6, '2019-11-05 14:28:11', 30),
(51, 62, 7, '2019-11-05 14:35:42', 31),
(52, 63, 7, '2019-11-05 14:35:44', 32),
(53, 64, 7, '2019-11-05 14:35:46', 33),
(54, 65, 7, '2019-11-05 14:35:47', 34),
(55, 66, 7, '2019-11-05 14:35:48', 35),
(56, 67, 7, '2019-11-05 14:35:49', 36),
(57, 68, 8, '2019-11-05 14:41:15', 37),
(58, 69, 8, '2019-11-05 14:41:16', 38),
(59, 70, 8, '2019-11-05 14:41:18', 39),
(60, 71, 9, '2019-11-05 14:48:23', 40),
(61, 72, 9, '2019-11-05 14:48:24', 41),
(65, 76, 10, '2019-11-05 15:09:01', 44),
(66, 77, 10, '2019-11-05 15:09:10', 45),
(67, 78, 10, '2019-11-05 15:09:11', 46),
(68, 79, 10, '2019-11-05 15:09:13', 47),
(69, 80, 10, '2019-11-05 15:09:14', 48),
(70, 81, 10, '2019-11-05 15:09:15', 49),
(71, 82, 11, '2019-11-05 15:14:24', 50),
(72, 83, 11, '2019-11-05 15:14:26', 51),
(73, 84, 11, '2019-11-05 15:14:27', 52),
(74, 85, 11, '2019-11-05 15:14:28', 53),
(75, 86, 11, '2019-11-05 15:14:29', 54),
(76, 87, 11, '2019-11-05 15:14:30', 55),
(77, 88, 12, '2019-11-05 15:22:02', 56),
(78, 89, 12, '2019-11-05 15:22:06', 57),
(79, 90, 12, '2019-11-05 15:22:09', 58),
(80, 91, 12, '2019-11-05 15:22:11', 59),
(81, 92, 12, '2019-11-05 15:22:13', 60),
(82, 93, 12, '2019-11-05 15:22:15', 61),
(83, 94, 13, '2019-11-05 15:24:39', 62),
(84, 95, 14, '2019-11-05 15:28:09', 63),
(85, 96, 14, '2019-11-05 15:28:10', 64),
(86, 97, 14, '2019-11-05 15:28:11', 65),
(88, 99, 15, '2019-11-05 15:33:01', 67),
(89, 100, 15, '2019-11-05 15:33:01', 68),
(90, 101, 15, '2019-11-05 15:33:27', 69),
(91, 102, 16, '2019-11-05 15:37:41', 70),
(92, 103, 16, '2019-11-05 15:37:44', 71),
(93, 104, 16, '2019-11-05 15:37:45', 72),
(94, 105, 16, '2019-11-05 15:37:45', 73),
(95, 106, 16, '2019-11-05 15:37:47', 74),
(96, 107, 16, '2019-11-05 15:37:47', 75),
(97, 108, 17, '2019-11-05 15:45:28', 76),
(98, 109, 17, '2019-11-05 15:45:30', 77),
(99, 110, 17, '2019-11-05 15:45:31', 78),
(100, 111, 17, '2019-11-05 15:45:31', 79),
(101, 112, 17, '2019-11-05 15:45:32', 80),
(102, 113, 17, '2019-11-05 15:45:33', 81),
(103, 114, 17, '2019-11-05 15:45:34', 82),
(104, 115, 18, '2019-11-06 11:58:50', 83),
(105, 116, 18, '2019-11-06 11:58:50', 84),
(106, 117, 18, '2019-11-06 11:58:51', 85);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_image_image`
--

CREATE TABLE `mvc_image_image` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `file` varchar(250) NOT NULL,
  `create_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `alt` varchar(250) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AVG_ROW_LENGTH=1260 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_image_image`
--

INSERT INTO `mvc_image_image` (`id`, `category_id`, `parent_id`, `name`, `description`, `file`, `create_time`, `user_id`, `alt`, `type`, `status`, `sort`) VALUES
(1, NULL, NULL, 'logo.png', '', '2088404b88ca1c11b5535a1ff4c66c12.png', '2019-10-01 11:18:36', NULL, 'logo.png', 0, 1, 1),
(2, NULL, NULL, 'advantage-block.jpg', '', 'eca6933c88eb82a859bc2b21918bb087.jpg', '2019-10-01 11:21:11', NULL, 'advantage-block.jpg', 0, 1, 2),
(3, NULL, NULL, 'advantage-block2.jpg', '', 'b4178686f62d0dafe862bcccece0ee31.jpg', '2019-10-01 11:21:36', NULL, 'advantage-block2.jpg', 0, 1, 3),
(4, NULL, NULL, 'advantage-block3.jpg', '', '03f37764c14d1778f5124b776c562c78.jpg', '2019-10-01 11:21:54', NULL, 'advantage-block3.jpg', 0, 1, 4),
(5, NULL, NULL, 'advantage-block4.jpg', '', '671a2cb198a5b78560419f7170292a4a.jpg', '2019-10-01 11:22:11', NULL, 'advantage-block4.jpg', 0, 1, 5),
(6, NULL, NULL, 'advantage-block5.jpg', '', '2562b7aa5ffdb54094796fc4b2cb0e68.jpg', '2019-10-01 11:22:30', NULL, 'advantage-block5.jpg', 0, 1, 6),
(7, NULL, NULL, 'advantage-block6.jpg', '', 'b2e6be028283d02e22bfb700f406ef71.jpg', '2019-10-01 11:22:47', NULL, 'advantage-block6.jpg', 0, 1, 7),
(11, NULL, NULL, 'Материалы из кожи', 'http://skin.dcmr.ru/store/katalog-kozhi', '9f040cab3c4f5727bc1b0ccdc8079512.jpg', '2019-10-01 17:35:52', NULL, 'Материалы из кожи', 0, 1, 8),
(12, NULL, NULL, 'Материалы из кожи', 'http://skin.dcmr.ru/store/katalog-kozhi', 'bcad2f9d2e0486695b6a90cd48be2422.jpg', '2019-10-01 17:35:53', NULL, 'Материалы из кожи', 0, 1, 9),
(13, NULL, NULL, 'Материалы из кожи', 'http://skin.dcmr.ru/store/katalog-kozhi', '6a2475bdcd39988d346a0cd263fb8a19.jpg', '2019-10-01 17:35:54', NULL, 'Материалы из кожи', 0, 1, 10),
(14, NULL, NULL, 'history.jpg', '', '1d1e279d057af79c0e0ad929f733857c.jpg', '2019-10-03 09:24:38', NULL, 'history.jpg', 0, 1, 11),
(15, NULL, NULL, '1.jpg', '', '0d5acc3bfe5bc561e6e014d86173eeba.jpg', '2019-10-03 14:26:00', NULL, '1.jpg', 0, 1, 12),
(16, NULL, NULL, '3.jpg', '', 'ed89281224967b651e1d6b1d3a4b0e6a.jpg', '2019-10-03 14:26:19', NULL, '3.jpg', 0, 1, 13),
(17, NULL, NULL, '6.jpg', '', 'ccd47ac2db223e6ea980d87c7be89bfd.jpg', '2019-10-03 14:26:32', NULL, '6.jpg', 0, 1, 14),
(28, NULL, NULL, 'Пальто из кожи леопарда', 'Идейные соображения высшего порядка, а также постоянное', '063744fbad92f81e2559ffd682e32349.jpg', '2019-10-09 14:48:57', NULL, '1.jpg', 0, 1, 15),
(29, NULL, NULL, 'Классическая осенняя обувь', '', '86a046524a4ceb984241c28849c185fc.jpg', '2019-10-09 14:48:57', NULL, '6.jpg', 0, 1, 16),
(30, NULL, NULL, 'Строгое пальто', '', 'b34b22a16b9e196644285cd6557eb508.jpg', '2019-10-09 14:48:58', NULL, '5.jpg', 0, 1, 17),
(31, NULL, NULL, 'Мужская куртка в двух видах', '', 'a62cc7f0ec9ad81d10b46342da2eed0c.jpg', '2019-10-09 14:48:58', NULL, '4.jpg', 0, 1, 18),
(32, NULL, NULL, 'Штаны из кожи, браз в 2019 году', '', 'bb34a9e09c67ca4c824cc29ba0f9488a.jpg', '2019-10-09 14:48:59', NULL, '3.jpg', 0, 1, 19),
(33, NULL, NULL, 'Модный набор рыбака', '', 'fa9d37fe07aac2cb5ec6b056a98e4854.jpg', '2019-10-09 14:48:59', NULL, '2.jpg', 0, 1, 20),
(34, NULL, NULL, '0eVowVU87fk.jpg', '', '0a5de1ef4db1ba40d6af8d77843cb4ec.jpg', '2019-11-05 10:43:28', 6, '0eVowVU87fk.jpg', 0, 1, 21),
(35, NULL, NULL, 'nBWf7Ezetdc.jpg', '', 'bb28785a15b2dc02b8436aa63dbecb9e.jpg', '2019-11-05 10:43:29', 6, 'nBWf7Ezetdc.jpg', 0, 1, 22),
(36, NULL, NULL, 'QQzMu94h180.jpg', '', 'b78b13d551ee93c66665e896d6a5b60b.jpg', '2019-11-05 10:43:29', 6, 'QQzMu94h180.jpg', 0, 1, 23),
(37, NULL, NULL, 'qEBICht42g4.jpg', '', 'ba5e05202ea4162aa0a8177a0b78ef8a.jpg', '2019-11-05 10:43:29', 6, 'qEBICht42g4.jpg', 0, 1, 24),
(38, NULL, NULL, '8jXVMaHiE1U.jpg', '', '46bd7053628f4b26f046ae0513a1ffc4.jpg', '2019-11-05 10:43:29', 6, '8jXVMaHiE1U.jpg', 0, 1, 25),
(39, NULL, NULL, 'SZ5ndlotwnY.jpg', '', '2af8a1fb3ee9e55f040041f65a0ac5f2.jpg', '2019-11-05 10:43:30', 6, 'SZ5ndlotwnY.jpg', 0, 1, 26),
(47, NULL, NULL, 'XQBigFueVSQ.jpg', '', '4fe27eed80c729b64c4dfd15c0f147aa.jpg', '2019-11-05 14:09:09', 4, 'XQBigFueVSQ.jpg', 0, 1, 27),
(48, NULL, NULL, 'tFxTuG5ulKE.jpg', '', 'cf334d5b63c4808e9b7deaab0b83b9f4.jpg', '2019-11-05 14:09:09', 4, 'tFxTuG5ulKE.jpg', 0, 1, 28),
(49, NULL, NULL, 'kf7LRKJH5CE.jpg', '', 'a8598b58f13814853e3a5939f2169d60.jpg', '2019-11-05 14:09:09', 4, 'kf7LRKJH5CE.jpg', 0, 1, 29),
(50, NULL, NULL, 'Dw2lMuMxKjw.jpg', '', '1ca012944b0e9973d327d39ec6e54d70.jpg', '2019-11-05 14:09:10', 4, 'Dw2lMuMxKjw.jpg', 0, 1, 30),
(51, NULL, NULL, 'BUjSyFHh9_Y.jpg', '', 'f5c4095a6cf08ab6475b90e32bcc0b17.jpg', '2019-11-05 14:09:10', 4, 'BUjSyFHh9_Y.jpg', 0, 1, 31),
(52, NULL, NULL, '6PzuPOgWYks.jpg', '', '3042ed22a125eb738a5df7f81e178491.jpg', '2019-11-05 14:09:11', 4, '6PzuPOgWYks.jpg', 0, 1, 32),
(53, NULL, NULL, '1.jpg', '', '870d425d6f9fe7668959bf463a19d3c8.jpg', '2019-11-05 14:09:12', 4, '1.jpg', 0, 1, 33),
(54, NULL, NULL, '1.jpg', '', '96b04caf8453fdc6c7b8c0f391f8c781.jpg', '2019-11-05 14:18:47', 4, '1.jpg', 0, 1, 34),
(55, NULL, NULL, '4.jpg', '', 'c64db844abfe1e501fe81f5f10e0ee43.jpg', '2019-11-05 14:18:47', 4, '4.jpg', 0, 1, 35),
(56, NULL, NULL, '3.jpg', '', '08f67c5b2dabc79ac79c4c4b05449f7b.jpg', '2019-11-05 14:18:48', 4, '3.jpg', 0, 1, 36),
(57, NULL, NULL, '2.jpg', '', 'c385ed463d9c754f9885b5a9ad3ab69e.jpg', '2019-11-05 14:18:48', 4, '2.jpg', 0, 1, 37),
(58, NULL, NULL, '1.jpg', '', 'd80af067f7601805fe3fb55d02288565.jpg', '2019-11-05 14:22:33', 4, '1.jpg', 0, 1, 38),
(59, NULL, NULL, '4.jpg', '', '5dfc44fb3f52dd505fc6a0a13aa904ae.jpg', '2019-11-05 14:28:10', 4, '4.jpg', 0, 1, 39),
(60, NULL, NULL, '3.jpg', '', 'daf51974c25e97424ca233c13b36df51.jpg', '2019-11-05 14:28:11', 4, '3.jpg', 0, 1, 40),
(61, NULL, NULL, '2.jpg', '', '37f28a2fa4303d8b56d007ccf1973478.jpg', '2019-11-05 14:28:11', 4, '2.jpg', 0, 1, 41),
(62, NULL, NULL, '33.jpg', '', '2a4f13463a30a6714b18f18444f76488.jpg', '2019-11-05 14:35:42', 4, '33.jpg', 0, 1, 42),
(63, NULL, NULL, '22.jpg', '', '9cf5638f7074040b9d3785ee739466d1.jpg', '2019-11-05 14:35:44', 4, '22.jpg', 0, 1, 43),
(64, NULL, NULL, '44.jpg', '', '73179a2f52f2156e7228f7767a437119.jpg', '2019-11-05 14:35:46', 4, '44.jpg', 0, 1, 44),
(65, NULL, NULL, '55.jpg', '', '270884b2ae3d3ed1dc56e2ed394b57df.jpg', '2019-11-05 14:35:47', 4, '55.jpg', 0, 1, 45),
(66, NULL, NULL, '66.jpg', '', 'e163832f063de1b676238809847b4560.jpg', '2019-11-05 14:35:48', 4, '66.jpg', 0, 1, 46),
(67, NULL, NULL, '11.jpg', '', '54b5375b186ebed6c687e6d2fd68d0c4.jpg', '2019-11-05 14:35:49', 4, '11.jpg', 0, 1, 47),
(68, NULL, NULL, '3.jpg', '', '4c54e506167f0592ff2000ed57b310b2.jpg', '2019-11-05 14:41:15', 4, '3.jpg', 0, 1, 48),
(69, NULL, NULL, '2.jpg', '', 'ff711c5422afee4cd83f36af93921a87.jpg', '2019-11-05 14:41:16', 4, '2.jpg', 0, 1, 49),
(70, NULL, NULL, '1.jpg', '', '6ae9d4cae6ffa4062cfb5f8a63a75290.jpg', '2019-11-05 14:41:18', 4, '1.jpg', 0, 1, 50),
(71, NULL, NULL, '1.jpg', '', '16a46faee123827c8284996e343804ce.jpg', '2019-11-05 14:48:23', 4, '1.jpg', 0, 1, 51),
(72, NULL, NULL, '2.jpg', '', 'b1c24dc594d66d5cbe4dfc2ad282ad72.jpg', '2019-11-05 14:48:24', 4, '2.jpg', 0, 1, 52),
(76, NULL, NULL, '1.jpg', '', '2994f7d09c069fd7553c373671d068bc.jpg', '2019-11-05 15:09:01', 4, '1.jpg', 0, 1, 55),
(77, NULL, NULL, '2.jpg', '', '7ac5321912f449ec84219943b774ec16.jpg', '2019-11-05 15:09:10', 4, '2.jpg', 0, 1, 56),
(78, NULL, NULL, '5.jpg', '', '0bbba2916665fe148143d6867a0ed42d.jpg', '2019-11-05 15:09:11', 4, '5.jpg', 0, 1, 57),
(79, NULL, NULL, '3.jpg', '', '95207bad0e47d0f693333c3a63513593.jpg', '2019-11-05 15:09:13', 4, '3.jpg', 0, 1, 58),
(80, NULL, NULL, '4.jpg', '', 'e0ec88b4e87e0a8047dc2fce215cef25.jpg', '2019-11-05 15:09:14', 4, '4.jpg', 0, 1, 59),
(81, NULL, NULL, '6.jpg', '', 'fc07a5cc8daf3ec2d782710cd3906b06.jpg', '2019-11-05 15:09:15', 4, '6.jpg', 0, 1, 60),
(82, NULL, NULL, '3.jpg', '', 'd899fb7946c9bc2791f2354ad22d6e16.jpg', '2019-11-05 15:14:24', 4, '3.jpg', 0, 1, 61),
(83, NULL, NULL, '4.jpg', '', 'b3dd83a4fc543e994d223e8d70c480f8.jpg', '2019-11-05 15:14:26', 4, '4.jpg', 0, 1, 62),
(84, NULL, NULL, '5.jpg', '', '7efd4b810681681ef346fd0b2a86346a.jpg', '2019-11-05 15:14:27', 4, '5.jpg', 0, 1, 63),
(85, NULL, NULL, '6.jpg', '', 'fb6230e36bae4c272f50eb788103b9df.jpg', '2019-11-05 15:14:28', 4, '6.jpg', 0, 1, 64),
(86, NULL, NULL, '2.jpg', '', 'd65617245d9dfc5e498a31016db0c7cb.jpg', '2019-11-05 15:14:29', 4, '2.jpg', 0, 1, 65),
(87, NULL, NULL, '1.jpg', '', '3e452cbf70cb42ac6a9955d2db775aeb.jpg', '2019-11-05 15:14:30', 4, '1.jpg', 0, 1, 66),
(88, NULL, NULL, '5.jpg', '', 'a9e2df527028e2fdc1ebec92693beecc.jpg', '2019-11-05 15:22:02', 4, '5.jpg', 0, 1, 67),
(89, NULL, NULL, '1.jpg', '', 'a1a83f5c244b4357abc069cb3fba1c17.jpg', '2019-11-05 15:22:06', 4, '1.jpg', 0, 1, 68),
(90, NULL, NULL, '3.jpg', '', 'd54eb8450a670027c84a0753dad7340f.jpg', '2019-11-05 15:22:09', 4, '3.jpg', 0, 1, 69),
(91, NULL, NULL, '6.jpg', '', 'bd325f0dac4cbeb08ddc3fe5b3feceb6.jpg', '2019-11-05 15:22:11', 4, '6.jpg', 0, 1, 70),
(92, NULL, NULL, '2.jpg', '', 'a8bc9a787ad487d86660b26d23c93f8b.jpg', '2019-11-05 15:22:13', 4, '2.jpg', 0, 1, 71),
(93, NULL, NULL, '4.jpg', '', '38da45d124d68a96ba36b6e2aa68ce75.jpg', '2019-11-05 15:22:15', 4, '4.jpg', 0, 1, 72),
(94, NULL, NULL, '1.jpg', '', '98e2fa79f29fed518fff8e1c47627b9c.jpg', '2019-11-05 15:24:39', 4, '1.jpg', 0, 1, 73),
(95, NULL, NULL, '1.jpg', '', '1bebc26283445cd8018c03bcb6e0d082.jpg', '2019-11-05 15:28:09', 4, '1.jpg', 0, 1, 74),
(96, NULL, NULL, '2.jpg', '', '3b8361d8dfe17a027bae195ae343f7f2.jpg', '2019-11-05 15:28:10', 4, '2.jpg', 0, 1, 75),
(97, NULL, NULL, '3.jpg', '', 'a0c785d83190c3e1f0da435e9d1d5f45.jpg', '2019-11-05 15:28:11', 4, '3.jpg', 0, 1, 76),
(99, NULL, NULL, '3.jpg', '', '185c5211be12fb1c2c3998205aa796db.jpg', '2019-11-05 15:33:01', 4, '3.jpg', 0, 1, 78),
(100, NULL, NULL, '4.jpg', '', 'd6b52e6122777799a04938471ff89687.jpg', '2019-11-05 15:33:01', 4, '4.jpg', 0, 1, 79),
(101, NULL, NULL, '2.jpg', '', '15585a1d0dd6a516e71d6aa91fc8f106.jpg', '2019-11-05 15:33:27', 4, '2.jpg', 0, 1, 80),
(102, NULL, NULL, 'SbPVJr9LMQU.jpg', '', 'f4e32fa6289f252760e0c797d9f26e75.jpg', '2019-11-05 15:37:41', 4, 'SbPVJr9LMQU.jpg', 0, 1, 81),
(103, NULL, NULL, 'QbrEOJj6ghs.jpg', '', '1927ae6c825d172d48fb8dadfb716204.jpg', '2019-11-05 15:37:44', 4, 'QbrEOJj6ghs.jpg', 0, 1, 82),
(104, NULL, NULL, 'lRggoXqEX-0.jpg', '', 'f8c7963f5a838a994b154dff39d24f50.jpg', '2019-11-05 15:37:45', 4, 'lRggoXqEX-0.jpg', 0, 1, 83),
(105, NULL, NULL, 'HnR2Irj6fsE.jpg', '', 'f81fec59a68fead2702e0c2a467fa7a4.jpg', '2019-11-05 15:37:45', 4, 'HnR2Irj6fsE.jpg', 0, 1, 84),
(106, NULL, NULL, '8djY3KzpL7A.jpg', '', '68e5d96aa9553b408382852b93b2a883.jpg', '2019-11-05 15:37:47', 4, '8djY3KzpL7A.jpg', 0, 1, 85),
(107, NULL, NULL, '4.jpg', '', '10c64802f83efd8d96e9b9e2ce225f1a.jpg', '2019-11-05 15:37:47', 4, '4.jpg', 0, 1, 86),
(108, NULL, NULL, 'BwYEUH1pa1Y.jpg', '', 'b3ad6b30292ac250f1523fee09bdd38f.jpg', '2019-11-05 15:45:28', 4, 'BwYEUH1pa1Y.jpg', 0, 1, 87),
(109, NULL, NULL, 'G-bDD9BLCMw.jpg', '', '5eb973ccabe65232563cdca2664890c9.jpg', '2019-11-05 15:45:30', 4, 'G-bDD9BLCMw.jpg', 0, 1, 88),
(110, NULL, NULL, 'GBDo1_G_MsY.jpg', '', '3fa1b60e4dcdb44df21744438039f17c.jpg', '2019-11-05 15:45:31', 4, 'GBDo1_G_MsY.jpg', 0, 1, 89),
(111, NULL, NULL, 'pJEiBTBrd2M.jpg', '', 'dd108162810ab09c472ecf176393b817.jpg', '2019-11-05 15:45:31', 4, 'pJEiBTBrd2M.jpg', 0, 1, 90),
(112, NULL, NULL, 'tWpJAtJlxSE.jpg', '', '18d91db5eb63b7d6f5871400916af901.jpg', '2019-11-05 15:45:32', 4, 'tWpJAtJlxSE.jpg', 0, 1, 91),
(113, NULL, NULL, 'RvOt49FnyP8.jpg', '', 'd548559e1c813066712a855933897b73.jpg', '2019-11-05 15:45:33', 4, 'RvOt49FnyP8.jpg', 0, 1, 92),
(114, NULL, NULL, 'Vob-MLT-ozQ.jpg', '', 'd876c00cfc95db37925ff897c0f69949.jpg', '2019-11-05 15:45:34', 4, 'Vob-MLT-ozQ.jpg', 0, 1, 93),
(115, NULL, NULL, 'mobile-slider.jpg', '', 'a29162f23a09c69f72831c81bd45b586.jpg', '2019-11-06 11:58:50', 4, 'mobile-slider.jpg', 0, 1, 94),
(116, NULL, NULL, 'mobile-slider.jpg', '', 'ff0baf95f3fb08ce304f2c6be5c48bfb.jpg', '2019-11-06 11:58:50', 4, 'mobile-slider.jpg', 0, 1, 95),
(117, NULL, NULL, 'mobile-slider.jpg', '', 'b700c533196b46f13ca3647a2011880f.jpg', '2019-11-06 11:58:51', 4, 'mobile-slider.jpg', 0, 1, 96);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_mail_mail_event`
--

CREATE TABLE `mvc_mail_mail_event` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_mail_mail_template`
--

CREATE TABLE `mvc_mail_mail_template` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  `from` varchar(150) NOT NULL,
  `to` varchar(150) NOT NULL,
  `theme` text NOT NULL,
  `body` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_menu_menu`
--

CREATE TABLE `mvc_menu_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_menu_menu`
--

INSERT INTO `mvc_menu_menu` (`id`, `name`, `code`, `description`, `status`) VALUES
(2, 'Меню', 'menu', 'Меню', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_menu_menu_item`
--

CREATE TABLE `mvc_menu_menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `regular_link` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL,
  `href` varchar(150) NOT NULL,
  `class` varchar(150) DEFAULT NULL,
  `title_attr` varchar(150) DEFAULT NULL,
  `before_link` varchar(150) DEFAULT NULL,
  `after_link` varchar(150) DEFAULT NULL,
  `target` varchar(150) DEFAULT NULL,
  `rel` varchar(150) DEFAULT NULL,
  `condition_name` varchar(150) DEFAULT '0',
  `condition_denial` int(11) DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `entity_module_name` varchar(40) DEFAULT NULL,
  `entity_name` varchar(40) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1260 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_menu_menu_item`
--

INSERT INTO `mvc_menu_menu_item` (`id`, `parent_id`, `menu_id`, `regular_link`, `title`, `href`, `class`, `title_attr`, `before_link`, `after_link`, `target`, `rel`, `condition_name`, `condition_denial`, `sort`, `status`, `entity_module_name`, `entity_name`, `entity_id`) VALUES
(14, 0, 2, 1, 'Покупки в Prima Pelle', '/pokupki-v-prima-pelle', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 17, 1, NULL, NULL, NULL),
(16, 0, 2, 1, 'О нас', '/o-nas', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 20, 1, NULL, NULL, NULL),
(18, 0, 2, 1, 'Галерея Prima Pelle', '/albums', '', '', '', '', '', '', '', 0, 19, 1, '', '', NULL),
(20, 16, 2, 1, 'История', '/istoriya', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 21, 1, NULL, NULL, NULL),
(21, 16, 2, 1, 'Новости', '/news/novosti', '', '', '', '', '', '', '', 0, 22, 1, '', '', NULL),
(22, 16, 2, 1, 'Преимущества', '/preimushchestva', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 23, 1, NULL, NULL, NULL),
(23, 16, 2, 1, 'Отзывы', '/news/otzyvy', '', '', '', '', '', '', '', 0, 24, 1, '', '', NULL),
(24, 16, 2, 1, 'Наши видео', '/news/nashi-video', '', '', '', '', '', '', '', 0, 25, 1, '', '', NULL),
(25, 16, 2, 1, 'Контакты', '/kontakty', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 26, 1, NULL, NULL, NULL),
(35, 14, 2, 1, 'Доставка', '/delivery', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 36, 1, NULL, NULL, NULL),
(36, 14, 2, 1, 'Легкий возврат', '/return', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 37, 1, NULL, NULL, NULL),
(37, 14, 2, 1, 'Служба поддержки', '/support', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 38, 1, NULL, NULL, NULL),
(38, 14, 2, 1, 'Оплата', '/payment', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 35, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_migrations`
--

CREATE TABLE `mvc_migrations` (
  `id` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=138 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_migrations`
--

INSERT INTO `mvc_migrations` (`id`, `module`, `version`, `apply_time`) VALUES
(1, 'user', 'm000000_000000_user_base', 1569909500),
(2, 'user', 'm131019_212911_user_tokens', 1569909500),
(3, 'user', 'm131025_152911_clean_user_table', 1569909504),
(4, 'user', 'm131026_002234_prepare_hash_user_password', 1569909506),
(5, 'user', 'm131106_111552_user_restore_fields', 1569909506),
(6, 'user', 'm131121_190850_modify_tokes_table', 1569909507),
(7, 'user', 'm140812_100348_add_expire_to_token_table', 1569909507),
(8, 'user', 'm150416_113652_rename_fields', 1569909508),
(9, 'user', 'm151006_000000_user_add_phone', 1569909508),
(10, 'yupe', 'm000000_000000_yupe_base', 1569909509),
(11, 'yupe', 'm130527_154455_yupe_change_unique_index', 1569909510),
(12, 'yupe', 'm150416_125517_rename_fields', 1569909510),
(13, 'yupe', 'm160204_195213_change_settings_type', 1569909510),
(14, 'category', 'm000000_000000_category_base', 1569909512),
(15, 'category', 'm150415_150436_rename_fields', 1569909512),
(16, 'image', 'm000000_000000_image_base', 1569909515),
(17, 'image', 'm150226_121100_image_order', 1569909515),
(18, 'image', 'm150416_080008_rename_fields', 1569909515),
(19, 'comment', 'm000000_000000_comment_base', 1569909518),
(20, 'comment', 'm130704_095200_comment_nestedsets', 1569909519),
(21, 'comment', 'm150415_151804_rename_fields', 1569909519),
(22, 'gallery', 'm000000_000000_gallery_base', 1569909521),
(23, 'gallery', 'm130427_120500_gallery_creation_user', 1569909522),
(24, 'gallery', 'm150416_074146_rename_fields', 1569909522),
(25, 'gallery', 'm160514_131314_add_preview_to_gallery', 1569909523),
(26, 'gallery', 'm160515_123559_add_category_to_gallery', 1569909524),
(27, 'gallery', 'm160515_151348_add_position_to_gallery_image', 1569909524),
(28, 'gallery', 'm181224_072816_add_sort_to_gallery', 1569909524),
(29, 'store', 'm140812_160000_store_attribute_group_base', 1569909525),
(30, 'store', 'm140812_170000_store_attribute_base', 1569909526),
(31, 'store', 'm140812_180000_store_attribute_option_base', 1569909527),
(32, 'store', 'm140813_200000_store_category_base', 1569909528),
(33, 'store', 'm140813_210000_store_type_base', 1569909529),
(34, 'store', 'm140813_220000_store_type_attribute_base', 1569909530),
(35, 'store', 'm140813_230000_store_producer_base', 1569909531),
(36, 'store', 'm140814_000000_store_product_base', 1569909534),
(37, 'store', 'm140814_000010_store_product_category_base', 1569909536),
(38, 'store', 'm140814_000013_store_product_attribute_eav_base', 1569909538),
(39, 'store', 'm140814_000018_store_product_image_base', 1569909538),
(40, 'store', 'm140814_000020_store_product_variant_base', 1569909540),
(41, 'store', 'm141014_210000_store_product_category_column', 1569909543),
(42, 'store', 'm141015_170000_store_product_image_column', 1569909543),
(43, 'store', 'm141218_091834_default_null', 1569909544),
(44, 'store', 'm150210_063409_add_store_menu_item', 1569909544),
(45, 'store', 'm150210_105811_add_price_column', 1569909544),
(46, 'store', 'm150210_131238_order_category', 1569909544),
(47, 'store', 'm150211_105453_add_position_for_product_variant', 1569909544),
(48, 'store', 'm150226_065935_add_product_position', 1569909544),
(49, 'store', 'm150416_112008_rename_fields', 1569909544),
(50, 'store', 'm150417_180000_store_product_link_base', 1569909547),
(51, 'store', 'm150825_184407_change_store_url', 1569909547),
(52, 'store', 'm150907_084604_new_attributes', 1569909550),
(53, 'store', 'm151218_081635_add_external_id_fields', 1569909550),
(54, 'store', 'm151218_082939_add_external_id_ix', 1569909550),
(55, 'store', 'm151218_142113_add_product_index', 1569909550),
(56, 'store', 'm151223_140722_drop_product_type_categories', 1569909551),
(57, 'store', 'm160210_084850_add_h1_and_canonical', 1569909552),
(58, 'store', 'm160210_131541_add_main_image_alt_title', 1569909552),
(59, 'store', 'm160211_180200_add_additional_images_alt_title', 1569909552),
(60, 'store', 'm160215_110749_add_image_groups_table', 1569909553),
(61, 'store', 'm160227_114934_rename_producer_order_column', 1569909553),
(62, 'store', 'm160309_091039_add_attributes_sort_and_search_fields', 1569909553),
(63, 'store', 'm160413_184551_add_type_attr_fk', 1569909554),
(64, 'store', 'm160602_091243_add_position_product_index', 1569909554),
(65, 'store', 'm160602_091909_add_producer_sort_index', 1569909554),
(66, 'store', 'm160713_105449_remove_irrelevant_product_status', 1569909554),
(67, 'store', 'm160805_070905_add_attribute_description', 1569909554),
(68, 'store', 'm161015_121915_change_product_external_id_type', 1569909557),
(69, 'store', 'm161122_090922_add_sort_product_position', 1569909557),
(70, 'store', 'm161122_093736_add_store_layouts', 1569909557),
(71, 'store', 'm181218_121815_store_product_variant_quantity_column', 1569909557),
(72, 'mail', 'm000000_000000_mail_base', 1569909560),
(73, 'payment', 'm140815_170000_store_payment_base', 1569909562),
(74, 'delivery', 'm140815_190000_store_delivery_base', 1569909564),
(75, 'delivery', 'm140815_200000_store_delivery_payment_base', 1569909566),
(76, 'order', 'm140814_200000_store_order_base', 1569909573),
(77, 'order', 'm150324_105949_order_status_table', 1569909575),
(78, 'order', 'm150416_100212_rename_fields', 1569909575),
(79, 'order', 'm150514_065554_change_order_price', 1569909575),
(80, 'order', 'm151209_185124_split_address', 1569909575),
(81, 'order', 'm151211_115447_add_appartment_field', 1569909575),
(82, 'order', 'm160415_055344_add_manager_to_order', 1569909577),
(83, 'order', 'm160618_145025_add_status_color', 1569909577),
(84, 'notify', 'm141031_091039_add_notify_table', 1569909580),
(85, 'page', 'm000000_000000_page_base', 1569909584),
(86, 'page', 'm130115_155600_columns_rename', 1569909585),
(87, 'page', 'm140115_083618_add_layout', 1569909585),
(88, 'page', 'm140620_072543_add_view', 1569909585),
(89, 'page', 'm150312_151049_change_body_type', 1569909586),
(90, 'page', 'm150416_101038_rename_fields', 1569909586),
(91, 'page', 'm180224_105407_meta_title_column', 1569909586),
(92, 'page', 'm180421_143324_update_page_meta_column', 1569909586),
(93, 'blog', 'm000000_000000_blog_base', 1569909602),
(94, 'blog', 'm130503_091124_BlogPostImage', 1569909603),
(95, 'blog', 'm130529_151602_add_post_category', 1569909605),
(96, 'blog', 'm140226_052326_add_community_fields', 1569909605),
(97, 'blog', 'm140714_110238_blog_post_quote_type', 1569909606),
(98, 'blog', 'm150406_094809_blog_post_quote_type', 1569909607),
(99, 'blog', 'm150414_180119_rename_date_fields', 1569909608),
(100, 'blog', 'm160518_175903_alter_blog_foreign_keys', 1569909610),
(101, 'blog', 'm180421_143937_update_blog_meta_column', 1569909611),
(102, 'blog', 'm180421_143938_add_post_meta_title_column', 1569909611),
(103, 'coupon', 'm140816_200000_store_coupon_base', 1569909613),
(104, 'coupon', 'm150414_124659_add_order_coupon_table', 1569909616),
(105, 'coupon', 'm150415_153218_rename_fields', 1569909616),
(106, 'menu', 'm000000_000000_menu_base', 1569909619),
(107, 'menu', 'm121220_001126_menu_test_data', 1569909619),
(108, 'menu', 'm160914_134555_fix_menu_item_default_values', 1569909624),
(109, 'menu', 'm181214_110527_menu_item_add_entity_fields', 1569909624),
(110, 'contentblock', 'm000000_000000_contentblock_base', 1569909630),
(111, 'contentblock', 'm140715_130737_add_category_id', 1569909631),
(112, 'contentblock', 'm150127_130425_add_status_column', 1569909631),
(113, 'feedback', 'm000000_000000_feedback_base', 1569909634),
(114, 'feedback', 'm150415_184108_rename_fields', 1569909634),
(115, 'news', 'm000000_000000_news_base', 1569909638),
(116, 'news', 'm150416_081251_rename_fields', 1569909638),
(117, 'news', 'm180224_105353_meta_title_column', 1569909639),
(118, 'news', 'm180421_142416_update_news_meta_column', 1569909639),
(123, 'dictionary', 'm000000_000000_dictionary_base', 1571037247),
(124, 'dictionary', 'm150415_183050_rename_fields', 1571037247),
(125, 'user', 'm191102_120102_add_field_is_admin', 1572885693),
(126, 'order', 'm191104_121527_add_field_order_address_delivery', 1572885652),
(127, 'user', 'm191102_120102_add_field_is_admin', 1572885693),
(128, 'rbac', 'm140115_131455_auth_item', 1572895710),
(129, 'rbac', 'm140115_131455_auth_item', 1572895710),
(130, 'rbac', 'm140115_132045_auth_item_child', 1572895710),
(131, 'rbac', 'm140115_132319_auth_item_assign', 1572895710),
(132, 'rbac', 'm140702_230000_initial_role_data', 1572895710),
(133, 'chat', 'm191030_070130_create_chat_chat_table', 1572895774),
(134, 'chat', 'm191030_083432_add_user_to_column_to_chat_chat_table', 1572895774),
(135, 'store', 'm191205_150639_add_field_link_youtube_product', 1575559928),
(136, 'user', 'm191206_045502_add_field_way_delivery', 1575608946),
(137, 'store', 'm201205_150639_add_field_color_attribute_option', 1580898834),
(138, 'store', 'm211205_150639_add_field_option_color_id_for_image', 1581067002),
(139, 'store', 'm221205_150639_add_field_option_color_id_for_image', 1581067254),
(140, 'store', 'm231205_150639_add_field_parent_id_variant', 1581332060),
(141, 'review', 'm000000_000000_review_base', 1583476834),
(142, 'review', 'm000000_000001_review_add_column', 1583476835),
(143, 'review', 'm000000_000002_review_add_column_pos', 1583476835),
(144, 'review', 'm000000_000003_review_add_column_name_service', 1583476835),
(145, 'review', 'm000000_000004_review_add_column_rating', 1583476835),
(146, 'review', 'm000000_000005_review_rename_column', 1583476835);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_news_news`
--

CREATE TABLE `mvc_news_news` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `date` date NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `short_text` text,
  `full_text` text NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `link` varchar(300) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `meta_title` varchar(250) NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1820 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_news_news`
--

INSERT INTO `mvc_news_news` (`id`, `category_id`, `lang`, `create_time`, `update_time`, `date`, `title`, `slug`, `short_text`, `full_text`, `image`, `link`, `user_id`, `status`, `is_protected`, `meta_keywords`, `meta_description`, `meta_title`) VALUES
(1, 2, 'ru', '2019-10-11 13:01:39', '2020-03-03 10:16:21', '2019-10-10', 'Новое поступление кожи растительного дубления.', 'novoe-postuplenie-kozhi-rastitelnogo-dubleniya', '', '<p>Вновь на подходе новая партия полы растительного дубления от фабрики Nuova Grenoble (Италия). Как всегда будут классические цвета кожи vacchetta, а также полюбившиеся многим мастерам винтажные артикулы, артикулы с ручным окрасом, артикулы с деликатным блеском. Это не постоянный ассортимент , а значит цены будут отличные - количество ограничено, обязательно следите за новинками на нашем сайте!</p>', '8ca89f2b04386f884b525c85fb034650.jpg', '', NULL, 0, 0, '', '', ''),
(2, 2, 'ru', '2019-10-11 13:24:42', '2020-03-03 10:15:42', '2019-10-15', 'Новое поступление постоянного ассортимента от фабрики La Perla Azzurra.', 'novoe-postuplenie-postoyannogo-assortimenta-ot-fabriki-la-perla-azzurra', '', '<p>Мы\r\nожидаем  новую партию полы растительного\r\nдубления в толщине 1,3-1,5 мм, артикулы  \r\nAlaska,\r\nAmazzonia,\r\nEl\r\nVaquero,\r\n Missouri.\r\nСамые красивые и ходовые цвета, артикулы\r\nс пулл-ап эффектом, с белым воском, с\r\nокрасом –камуфляж!  Данная кожа подойдет\r\nдля создания кошельков, портмоне,\r\nкартхолдеров, обложек, обуви, чехлов и\r\nт.д. Следите за обновлениями на нашем\r\nсайте!</p>', 'f2b274aa9d07c365bf1615451cb89397.jpg', '', NULL, 0, 0, '', '', ''),
(3, 1, 'ru', '2019-10-11 15:44:58', '2020-03-03 10:16:04', '2019-10-11', 'Наталья Саенко', 'natalya-saenko', '<p>Очень довольна покупкой!!! Спасибо большое!!!</p>', '<p>Очень довольна покупкой!!! Спасибо большое!!!</p>', '2d4e7cf4e1440f6c0b13cc448a869f44.jpg', '', NULL, 0, 0, '', '', ''),
(4, 1, 'ru', '2019-10-11 15:45:48', '2020-03-03 10:16:08', '2019-10-11', 'Владимир Ефремов', 'vladimir-efremov', '<p>Спасибо всё отлично!!!</p>', '<p>Спасибо всё отлично!!!</p>', 'b35444fe78c2b9834439cb35f8126976.jpg', '', NULL, 0, 0, '', '', ''),
(5, 1, 'ru', '2019-10-11 15:46:53', '2020-03-03 10:16:12', '2019-10-11', 'Светлана', 'svetlana', '<p>В очередной раз довольна покупкой. Спасибо!</p>', '<p>В очередной раз довольна покупкой. Спасибо!</p>', '542f9289b86daf63d3de0887631c8cd6.jpg', '', NULL, 0, 0, '', '', ''),
(6, 1, 'ru', '2019-10-11 15:47:25', '2020-03-03 10:16:35', '2019-10-07', 'Макс Мороз', 'maks-moroz', '<p>Кожа супер! За бонус отдельное спасибо!</p>', '<p>Кожа супер! За бонус отдельное спасибо!</p>', '8c796676700887789086b6e44a809e50.jpg', '', NULL, 0, 0, '', '', ''),
(7, 1, 'ru', '2019-10-11 15:48:01', '2020-03-03 10:16:40', '2019-10-06', 'Жанна Костыря', 'zhanna-kostyrya', '<p>Отличная кожа. Все собрано и тщательно упаковано. Спасибо за презент). Лучший магазин кожи!</p>', '<p>Отличная кожа. Все собрано и тщательно упаковано. Спасибо за презент). Лучший магазин кожи!</p>', 'a2568d2621966a553d6bfe05d6298ca9.jpg', '', NULL, 0, 0, '', '', ''),
(8, 3, 'ru', '2019-10-11 15:49:16', '2020-03-03 10:17:08', '2019-10-11', 'Prima Pelle,  La Perla Azzurra', 'prima-pelle-la-perla-azzurra', '', '<p>Более 7 лет выбираем лучшую кожу из Италии</p>', NULL, 'https://www.youtube.com/watch?v=c_nPK4k2vWk', NULL, 1, 0, '', '', ''),
(9, 3, 'ru', '2019-10-11 15:49:44', '2020-03-03 10:16:46', '2019-10-11', 'Prima Pelle', 'prima-pelle', '', '<p>Фабрика\r\nLa Perla Azzurra (Италия)</p>', NULL, 'https://www.youtube.com/watch?v=erkMkYbV86U', NULL, 1, 0, '', '', ''),
(10, 1, 'ru', '2020-03-03 10:25:50', '2020-03-03 10:25:50', '2020-03-03', 'Все отзывы за 8 лет работы на ярмарке мастеров', 'vse-otzyvy-za-8-let-raboty-na-yarmarke-masterov', '', '<p>За 8 лет работы мы скопили более 4500 отзывов, как показывает статистика ярмарки мастеров, у нашего магазина 100% отзывов.<br>Более подробно с ними Вы может ознакомиться на нашей страничке на ярмарке мастеров  <a href=\"https://www.livemaster.ru/chaizara/feedbacks\">https://www.livemaster.ru/chaizara/feedbacks</a> </p>', '214794a4b460e5521422dcaa659515a0.jpg', 'https://www.livemaster.ru/chaizara/feedbacks​ ', 4, 1, 0, '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_notify_settings`
--

CREATE TABLE `mvc_notify_settings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `my_post` tinyint(1) NOT NULL DEFAULT '1',
  `my_comment` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_notify_settings`
--

INSERT INTO `mvc_notify_settings` (`id`, `user_id`, `my_post`, `my_comment`) VALUES
(1, 4, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_page_page`
--

CREATE TABLE `mvc_page_page` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `change_user_id` int(11) DEFAULT NULL,
  `title_short` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `body` mediumtext NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `layout` varchar(250) DEFAULT NULL,
  `view` varchar(250) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=3276 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_page_page`
--

INSERT INTO `mvc_page_page` (`id`, `category_id`, `lang`, `parent_id`, `create_time`, `update_time`, `user_id`, `change_user_id`, `title_short`, `title`, `slug`, `body`, `meta_keywords`, `meta_description`, `status`, `is_protected`, `order`, `layout`, `view`, `meta_title`) VALUES
(4, NULL, 'ru', NULL, '2019-10-01 12:16:34', '2019-12-10 15:55:09', NULL, 4, '', 'Покупки в Prima Pelle', 'pokupki-v-prima-pelle', '<h2 class=\"first-h2\">Правила магазина</h2>\r\n<p><label>Уважаемые  покупатели,  пожалуйста, ознакомьтесь с правилами нашего магазина.</label>\r\n</p>\r\n<ul>\r\n	<li>Минимальный заказ в магазине – 1000 рублей.</li>\r\n	<li>Мы работаем только по 100% предоплате.</li>\r\n	<li>Режим работы интернет магазина  понедельник-суббота с 8-17 часов (время московское). Воскресенье-ВЫХОДНОЙ!</li>\r\n	<li>Все заказы обрабатываются в рабочее время согласно очереди, в порядке их поступления,  в зависимости от загруженности менеджеров сроки принятия заказа могут быть увеличены до 1 рабочего дня. </li>\r\n	<li>В нашем магазине вы можете забронировать/зарезервировать определенные позиции сроком до 3 рабочих дней (в определенных случаях сроки резерва могут быть увеличены – обсуждается индивидуально с менеджером).</li>\r\n	<li>Если в разделе размеры - стоит слово \"Резерв\" - это означает, что позиция была зарезервирована другим покупателем  и далее выкуплена.</li>\r\n	<li>Никогда не стесняйтесь уточнять по поводу наличия той или иной позиции, даже там где стоит слово \"Резерв\", всегда с удовольствием ответим  на ваши вопросы!</li>\r\n	<li>Совершая покупку,  покупатель тем самым заявляет, что ознакомлен и согласен с правилами и условиями магазина.</li>\r\n</ul>\r\n<p><label>Что нужно знать  при совершении покупки:</label>\r\n</p>\r\n<ol>\r\n	<li>Расхождение в цвете</li>\r\n	<p>Обращайте внимание на  возможное расхождения цвета кожи с цветом (оттенком) заявленным на фото – это зависит от сложности цветопередачи и от разности настройки мониторов. Пожалуйста, внимательно читайте описание и просматривайте все фото к выбираемой вами позиции.\r\n	</p>\r\n	<li>Сортность кожи </li>\r\n	<p>Обратите внимание, кожа - \" живой материал\", на коже даже первого сорта возможно наличие небольших шрамов полученных животным при жизни, укусов насекомых, потертости, небольших дефектов полученных при выделки кожи – для кожи хромового дубления данный показатель может доходить  до 7-10% от общей площади,  для кожи растительного дубления – до 10-15%  (обязательно учитывайте это при покупке).\r\n	</p>\r\n	<li>Особенности ременной кожи растительного дубления </li>\r\n	<p>Очень важно понимать, что у ременных артикулов  с разбивкой и пулл ап эффектом - из-за особенности технологического процесса производства  и с учетом физиологии шкур - плотность кожи может меняться в разрезе одной шкуры. Ближе к верху шкуры и к краям плотность кожи может снижаться - учитывайте это при совершении покупки. Кожа не линолеум (особенно это касается кожи растительного дубления) - это живой натуральный материал, требующий деликатного отношения и ухода.\r\n		Как и любое дорогое изделие - кожа растительного дубления требует к себе бережного отношения и ухода. Используйте только финиши на натуральной восковой основе, предварительно протестировав их на небольшом участке кожи.\r\n	</p>\r\n</ol>\r\n<h2>Как сделать заказ?</h2>\r\n<p><label>Войти или зарегистрироваться</label>\r\n</p>\r\n<p>Оформить заказ могут только зарегистрированные пользователи. Войдите в Личный кабинет под своим логином или <a href=\"#\">зарегистрируйтесь</a>, если Вы у нас впервые. Регистрация займет не более 2 минут.\r\n</p>\r\n<p><label>Оформление заказа</label>\r\n</p>\r\n<ul>\r\n	<li>Перед началом оформления заказа обязательно ознакомьтесь с правилами и условиями магазина. Далее выберите понравившийся вам  товар и нажмите кнопку <label>«Добавить в корзину»</label>.</li>\r\n	<li>На экране появится всплывающее окно, в котором будет возможность  либо вернуться к покупкам, нажав кнопку <label>«Продолжить покупки»</label>, либо перейти к оформлению заказа, нажав кнопку <label>«Оформить заказ»</label> внизу. Выберите необходимое действие;</li>\r\n	<li>Чтобы убрать не нужные позиции из корзины вам нужно нажать кнопку удалить.</li>\r\n	<li>На странице оформления вам нужно ввести все контактные данные, а также выбрать удобный способ доставки и оплаты.</li>\r\n	<li>После оформления заказа – нажмите кнопку <label>«Подтвердить заказ»</label>.</li>\r\n	<li>После нажатия кнопки «Подтвердить  заказ» появится сообщение об успешно  завершенной процедуре оформления заказа.</li>\r\n	<li>Подтверждая покупку, покупатель тем самым заявляет, что ознакомлен и согласен с правилами и условиями магазина.</li>\r\n	<li>После того как менеджер обработал ваш заказ и согласовал с вами способ доставки  – вам будут высланы реквизиты для оплаты заказа. </li>\r\n</ul>', '', '', 1, 0, 6, '', '', ''),
(6, NULL, 'ru', NULL, '2019-10-01 12:17:02', '2019-12-10 15:55:09', NULL, 4, '', 'О нас', 'o-nas', '<div class=\"row\">\r\n	<div class=\"col-sm-6\">\r\n		<div class=\"content-page-image-img\">\r\n			<img src=\"/uploads/image/0d5acc3bfe5bc561e6e014d86173eeba.jpg\" style=\"background-color: initial;\">\r\n		</div>\r\n	</div>\r\n	<div class=\"col-sm-6\">\r\n		<p>Мы занимаемся продажей натуральной итальянской кожи крс, овчины, принты, замша, велюр. Большой выбор цветовой гаммы, эксклюзивные шкуры - с выделкой в стиле винтаж - крейзи хорс, кожи растительного дубления.</p>\r\n		<p>Толщина кожи различная, от 0,6 мм до 4 мм. Мы продаем шкуры, полшкуры, лоскуты.\r\n		</p>\r\n		<p>Ежемесячное поступление новинок. Цены очень демократичные.\r\n		</p>\r\n	</div>\r\n</div>', '', '', 1, 0, 4, '', '', ''),
(7, NULL, 'ru', NULL, '2019-10-01 13:47:37', '2019-12-10 15:55:09', NULL, 4, '', 'Prima Pelle', 'prima-pelle', '<p>Мы занимаемся продажей натуральной итальянской кожи крс, овчины, принты, замша, велюр. Большой выбор цветовой гаммы, эксклюзивные шкуры - с выделкой в стиле винтаж - крейзи хорс, кожи растительного дубления. Толщина кожи различная, от 0,6 мм до 4 мм. Мы продаем шкуры, полшкуры, лоскуты. Ежемесячное поступление новинок. Цены очень демократичные.</p>', '', 'Мы занимаемся продажей натуральной итальянской кожи крс, овчины, принты, замша, велюр. Большой выбор цветовой гаммы, эксклюзивные шкуры - с выделкой в стиле винтаж - крейзи хорс, кожи растительного дубления.', 1, 0, 7, 'home', '', ''),
(9, NULL, 'ru', 6, '2019-10-01 14:02:21', '2020-03-03 09:59:08', NULL, 4, '', 'История', 'istoriya', '<blockquote>\r\n	«Создавать нечто особенное  из качественных и красивых материалов – одно удовольствие»\r\n</blockquote><p>Уверены, Вас посещали эти мысли, когда Вы брали в руки натуральную итальянскую кожу и начинали творить.\r\n</p><p><label>Prima pelle</label> – это семейный бизнес, владельцами которого являются Марина и Станислав Мразовские.\r\n</p><p>Началось все в 2011 году, тогда Марина увлеклась изготовлением кожаных браслетов, но купить качественную кожу было проблематично. Тогда Марина и Стас рискнули и сделали свой первый заказ на 70 кг кожи у итальянского поставщика. Эта партия оправдала ожидания  и моментально разошлась среди знакомых мастеров.\r\nИменно с этого момента начинается наша история – история компании \r\n	<label>Prima Pelle </label> - поставщика натуральной кожи из Италии.\r\n</p><p><label>Наша цель</label> – это создание ответственной компании, которая решит Ваши проблемы с выбором качественной кожи, фурнитуры и химии\r\n</p><p>Prima Pelle сегодня – это:\r\n</p><ul>\r\n	<li>Более 7 лет стабильной работы и более 4000 положительных отзывов.</li>\r\n	<li>Наличие постоянного ассортимента кожи растительного дубления премиум качества.</li>\r\n	<li>Максимальное точное описание позиции, реальные фото и видео.</li>\r\n	<li>Удобный сервис, построенный на уважение и поддержке каждого клиента.</li>\r\n	<li>Проведение масштабного ежегодного  конкурса <strong>HE GREATE LEATHER MASTER.</strong></li>\r\n	<li>Постоянная работа по совершенствованию и расширению ассортимента.</li>\r\n	<li>Ежегодное участие в выставках, конвентах, тренингах.</li>\r\n</ul><p>Мы  хотим, чтобы покупки в нашем магазине, приносили вам только положительные эмоции и вдохновляли вас на новые творческие шаги и победы!\r\n</p>', '', '', 1, 0, 8, '', '', ''),
(10, NULL, 'ru', 6, '2019-10-01 14:03:24', '2019-12-10 15:55:09', NULL, 4, '', 'Преимущества', 'preimushchestva', '<div class=\"advantages\">\r\n	<div class=\"row\">\r\n		<div class=\"col-sm-4\">\r\n			<div class=\"advantages-item\">\r\n				<div class=\"text-block\">\r\n					<label class=\"one-advantages\">Стабильность</label>\r\n					<p>7 лет честной  и ответственной работы\r\n					</p>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class=\"col-sm-4\">\r\n			<div class=\"advantages-item\">\r\n				<div class=\"text-block\">\r\n					<label class=\"two-advantages\">Оптимальный выбор</label>\r\n					<p>Ваши запросы растут – мы тоже)! За 7 лет мы увеличили наши поставки более чем в 30 раз. Мы стремимся сделать наш ассортимент разнообразным,  чтобы в одном магазине вы могли приобрести качественную итальянскую кожу, фурнитуру и химию\r\n					</p>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class=\"col-sm-4\">\r\n			<div class=\"advantages-item\">\r\n				<div class=\"text-block\">\r\n					<label class=\"three-advantages\">Качество</label>\r\n					<p>Работаем только с  проверенными  фабриками и поставщиками  кожи\r\n					</p>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class=\"col-sm-4\">\r\n			<div class=\"advantages-item\">\r\n				<div class=\"text-block\">\r\n					<label class=\"four-advantages\">Доверие</label>\r\n					<p>Нам доверяют более 2000 клиентов по России, Казахстану, Белоруссии\r\n					</p>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class=\"col-sm-4\">\r\n			<div class=\"advantages-item\">\r\n				<div class=\"text-block\">\r\n					<label class=\"five-advantages\">Честность и информативность</label>\r\n					<p>Мы перед вами честны и открыты, только у нас вы найдете подробное описание позиций, реальные фото и видео\r\n					</p>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class=\"col-sm-4 \">\r\n			<div class=\"advantages-item\">\r\n				<div class=\"text-block\">\r\n					<label class=\"six-advantages\">Уважение</label>\r\n					<p>Мы уважаем наших клиентов и стремимся к тому, чтобы кожевенное дело в России вышло на престижный и почитаемый уровень\r\n					</p>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>', '', '', 1, 0, 9, '', '', ''),
(11, NULL, 'ru', 6, '2019-10-01 14:04:49', '2019-12-10 15:55:09', NULL, 4, '', 'Контакты', 'kontakty', '<div class=\"row\">\r\n	<div class=\"col-sm-12 col-xs-12\">\r\n		<div class=\"flex-block contact\">\r\n			<p><label>Адрес:</label>\r\n			</p>\r\n			<p>460045, г. Оренбург, ул. Беляевская, д 55\r\n			</p>\r\n		</div>\r\n		<div class=\"flex-block contact\">\r\n			<p><label>Телефоны:</label>\r\n			</p>\r\n			<p>+7 (3532) 43–10–18 <br> +7–912–355–10–18 <br> +7–961–938–76–66\r\n			</p>\r\n		</div>\r\n		<div class=\"flex-block contact\">\r\n			<p><label>E-mail:</label>\r\n			</p>\r\n			<p>primapelleoren@gmail.com\r\n			</p>\r\n		</div>\r\n		<div class=\"flex-block contact\">\r\n			<label>Режим работы:</label>\r\n		</div>\r\n		<div class=\"flex-block contact\">\r\n			<p><label>Пн-Пт:</label>\r\n			</p>\r\n			<p>10.00 - 19.00\r\n			</p>\r\n		</div>\r\n		<div class=\"flex-block contact\">\r\n			<p><label>Сб:</label>\r\n			</p>\r\n			<p>10.00 - 16.00 <br> Без перерывов\r\n			</p>\r\n		</div>\r\n		<div class=\"flex-block contact\">\r\n			<p><label>Обед:</label>\r\n			</p>\r\n			<p>13.00 - 14.00\r\n			</p>\r\n		</div>\r\n		<div class=\"flex-block contact\">\r\n			<p><label>Вс:</label>\r\n			</p>\r\n			<p>Выходной\r\n			</p>\r\n		</div>\r\n		<div class=\"flex-block contact\">\r\n			<p><label>Мы в социальных сетях:</label>\r\n			</p>\r\n			<div class=\"social-flex\">\r\n				<a href=\"https://vk.com/chaizara\" target=\"_blank\"><i class=\"fa fa-vk\" aria-hidden=\"true\"></i></a>\r\n				<a href=\"https://www.instagram.com/prima_pelle/\" target=\"_blank\"><i class=\"fa fa-instagram\" aria-hidden=\"true\"></i></a>\r\n				<a href=\"https://www.facebook.com/PrimaPelleorenburg\" target=\"_blank\"><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>', '', '', 1, 0, 10, '', 'contacts', ''),
(12, NULL, 'ru', 4, '2019-10-03 12:17:33', '2019-12-10 15:55:09', NULL, 4, '', 'Оплата', 'payment', '<p><label>В нашем магазине возможны следующие способы  оплаты товара:</label>\r\n</p><ul>\r\n	<li>оплата на карту Сбербанка/Тинькофф банка;</li>\r\n	<li>оплата на Paypal;</li>\r\n	<li>оплата на расчетный счет;</li>\r\n	<li>денежные переводы Золотая корона.</li>\r\n</ul><p>Вариант оплаты вы выбираете на сайте, либо согласовываете с менеджером.\r\n</p>', '', '', 1, 0, 13, '', '', ''),
(15, NULL, 'ru', 4, '2019-10-03 12:26:13', '2019-12-10 15:55:27', NULL, 4, '', 'Доставка', 'delivery', '<p>\r\n	<label>Отправка заказов  осуществляется  3 раза в неделю: </label>\r\n</p><ol>\r\n	<li>понедельник;</li>\r\n	<li>среда;</li>\r\n<li>пятница;</li><li>воскресенье</li>\r\n</ol><p>\r\n	<label>Доступны следующие варианты доставки заказа:</label>\r\n</p><ul>\r\n	<li>Курьерская служба СДЕК.</li>\r\n	<li>Почта России.</li>\r\n	<li>Транспортная компания КИТ,  ПЭК.</li>\r\n</ul><p>Доставка до терминалов транспортной компании – <label>бесплатно</label>.\r\n</p><p>При оформлении заказа обязательно указывайте,  какой способ доставки вы выбираете.\r\n</p>', '', '', 1, 0, 15, '', '', ''),
(16, NULL, 'ru', 4, '2019-10-03 12:29:51', '2019-12-10 15:55:09', NULL, 4, '', 'Легкий возврат', 'return', '<div class=\"row\">\r\n	<div class=\"col-sm-10 col-xs-12\">\r\n		<p>У вас всегда есть право вернуть ваш заказ в течение 14 дней. Период, в течение которого можно вернуть заказ, отсчитывается от дня доставки последнего(-их) товара(-ов) вашего заказа.\r\n		</p>\r\n		<p>Мы принимаем к возврату товар без следов его использования, в первоначальном  виде, качественно упакованный. Самый простой способ вернуть заказ — связаться с менеджером, он объяснит вам как просто и легко сделать  возврат.\r\n		</p>\r\n		<p>После того как товар поступил к нам на склад, мы проводим его проверку.  Денежные средства будут перечислены вам  в течение 5  дней с  даты  поступления товара на склад. Мы возвращаем  полную стоимость товара без учета расходов на доставку.\r\n		</p>\r\n		<p>Также вы можете обменять  товар на любой другой из нашего магазина.\r\n		</p>\r\n	</div>\r\n</div>', '', '', 1, 0, 14, '', '', ''),
(17, NULL, 'ru', 4, '2019-10-03 12:30:22', '2019-12-10 15:55:09', NULL, 4, '', 'Служба поддержки', 'support', '<div class=\"row\">\r\n	<div class=\"col-sm-6 col-xs-12\">\r\n		<div class=\"flex-block\">\r\n			<p><label>Адрес магазина:</label></p>\r\n			<p>460045, г. Оренбург, ул. Беляевская, д 55.</p>\r\n		</div>\r\n		<div class=\"flex-block\">\r\n			<p><label>Телефон:</label></p>\r\n			<p>+7 (3532) 431-018</p>\r\n		</div>\r\n		<div class=\"flex-block\">\r\n			<p><label>E-mail:</label></p>\r\n			<p>primapelleoren@gmail.com</p>\r\n		</div>\r\n	</div>\r\n</div><p>Если у вас возникли вопросы, вы всегда их можете задать  менеджерам непосредственно в переписке на  сайте.</p>', '', '', 1, 0, 16, '', '', ''),
(18, NULL, 'ru', NULL, '2019-11-06 11:54:39', '2019-12-10 15:55:09', 4, 4, '', 'Ваши работы', 'vashi-raboty', '<p>Страница находится в стадии наполнения</p>', '', '', 1, 0, 17, '', '', ''),
(19, NULL, 'ru', NULL, '2019-11-06 11:55:43', '2019-12-10 15:55:09', 4, 4, '', 'История успеха', 'istoriya-uspeha', '<p>Страница находится в стадии наполнения</p>', '', '', 1, 0, 18, '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_review`
--

CREATE TABLE `mvc_review` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `text` text NOT NULL,
  `moderation` int(11) DEFAULT NULL,
  `username` varchar(256) DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL,
  `useremail` varchar(256) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL COMMENT 'Категория',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  `product_id` int(11) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_review`
--

INSERT INTO `mvc_review` (`id`, `user_id`, `date_created`, `text`, `moderation`, `username`, `image`, `useremail`, `category_id`, `position`, `product_id`, `rating`) VALUES
(7, NULL, '2020-03-06 12:17:35', '123123121', 1, '3213123', NULL, NULL, NULL, 1, 9, NULL),
(8, NULL, '2020-03-06 12:17:55', '13131231', 1, '3213123', NULL, NULL, NULL, 2, 9, NULL),
(9, NULL, '2020-03-06 12:18:05', 'fsdkhjdfadj hd fbgsfkhba kyurgwe;o ijflzckjhaevruakblvndngldfsanvt .e.kbnaea', 1, '323223', NULL, NULL, NULL, 3, 9, NULL),
(10, NULL, '2020-03-06 12:21:10', '3123123', 1, '321312', NULL, NULL, NULL, 4, 9, NULL),
(11, NULL, '2020-03-06 12:27:41', 'dawdawdawd', 1, 'dwadaw', NULL, NULL, NULL, 5, 9, NULL),
(12, NULL, '2020-03-06 14:23:43', '321312312', 2, '321312', NULL, NULL, NULL, 6, 9, NULL),
(13, NULL, '2020-03-06 14:37:46', '123123123123', NULL, '3123123', NULL, NULL, NULL, 7, 9, NULL),
(14, NULL, '2020-03-06 14:59:16', '23123123', NULL, '32132131', NULL, NULL, NULL, 8, 9, NULL),
(15, NULL, '2020-03-06 15:02:04', '12312312312', NULL, '321323', NULL, NULL, NULL, 9, 9, NULL),
(16, NULL, '2020-03-06 15:03:57', '312312312', NULL, '321312', NULL, NULL, NULL, 10, 9, NULL),
(17, NULL, '2020-03-06 15:06:48', '3123123', NULL, '312312', NULL, NULL, NULL, 11, 9, NULL),
(18, NULL, '2020-03-06 15:08:09', '23123123', NULL, '32131', NULL, NULL, NULL, 12, 9, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_attribute`
--

CREATE TABLE `mvc_store_attribute` (
  `id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `is_filter` smallint(6) NOT NULL DEFAULT '1',
  `description` text
) ENGINE=InnoDB AVG_ROW_LENGTH=3276 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_attribute`
--

INSERT INTO `mvc_store_attribute` (`id`, `group_id`, `name`, `title`, `type`, `unit`, `required`, `sort`, `is_filter`, `description`) VALUES
(1, NULL, 'tip-dubleniya', 'Тип дубления', 4, '', 0, 1, 1, ''),
(2, NULL, 'tolshchina', 'Толщина', 4, '', 0, 2, 1, 'Толщина для кожи'),
(5, NULL, 'zhestkost', 'Жесткость', 4, '', 0, 5, 1, ''),
(6, NULL, 'naznachenie', 'Назначение', 4, '', 0, 6, 1, ''),
(7, NULL, 'vid-poverhnosti', 'Вид поверхности', 4, '', 0, 7, 1, ''),
(8, NULL, 'tolshchina-nit', 'Толщина', 4, 'мм', 0, 8, 1, 'толщина для нити'),
(9, NULL, 'namotka', 'Намотка', 6, 'м', 0, 9, 1, 'длина'),
(10, NULL, 'razmer-dm', 'Размер (дм)', 2, '', 1, 11, 1, 'размер для кожи'),
(11, NULL, 'cvet', 'Цвет', 8, '', 0, 10, 1, ''),
(12, NULL, 'furnitura', 'Фурнитура', 2, '', 0, 12, 1, ''),
(13, NULL, 'himiya', 'Химия', 2, '', 0, 13, 1, ''),
(15, NULL, 'obem', 'Объем', 2, 'мл', 0, 14, 1, ''),
(16, NULL, 'razmer-m', 'Размер (м)', 2, '', 0, 15, 1, '');

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_attribute_group`
--

CREATE TABLE `mvc_store_attribute_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_attribute_option`
--

CREATE TABLE `mvc_store_attribute_option` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  `value` varchar(255) DEFAULT '',
  `color` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=712 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_attribute_option`
--

INSERT INTO `mvc_store_attribute_option` (`id`, `attribute_id`, `position`, `value`, `color`) VALUES
(2, 1, 1, 'Хромовое', NULL),
(3, 1, 2, 'Растительное', NULL),
(4, 1, 3, 'Комбинированное', NULL),
(5, 2, 4, '1,4-1,6 мм', NULL),
(6, 2, 5, '1,5-1,7 мм', NULL),
(7, 2, 6, '1,8-2,0 мм', NULL),
(8, 2, 7, '1,3-1,5 мм', NULL),
(9, 2, 8, '0,9-1,0 мм', NULL),
(10, 2, 9, '1,2-1,4 мм', NULL),
(11, 5, 10, 'Мягкая', NULL),
(12, 5, 11, 'Средняя', NULL),
(13, 5, 12, 'Держит форму', NULL),
(14, 6, 13, 'Галантерейная', NULL),
(15, 6, 14, 'Одежная', NULL),
(16, 6, 15, 'Ременная', NULL),
(17, 6, 16, 'Обувная', NULL),
(18, 6, 17, 'Автомобильная', NULL),
(19, 6, 18, 'Мебельная', NULL),
(20, 7, 19, 'Гладкая', NULL),
(21, 7, 20, 'Зерно', NULL),
(22, 7, 21, 'Саффиано', NULL),
(23, 7, 22, 'Рептилия', NULL),
(24, 7, 23, 'Иное', NULL),
(25, 8, 24, '0.5', NULL),
(26, 8, 25, '0.55', NULL),
(27, 8, 26, '0.6', NULL),
(28, 8, 27, '0.65', NULL),
(29, 8, 28, '0.7', NULL),
(30, 8, 29, '0.75', NULL),
(31, 8, 30, '0.8', NULL),
(32, 10, 31, '104', NULL),
(33, 10, 32, '108', NULL),
(34, 10, 33, '116', NULL),
(35, 10, 34, '122', NULL),
(36, 10, 35, '126', NULL),
(37, 11, 36, 'Желтый', 'fff2df'),
(38, 11, 37, 'Черный', '000000'),
(39, 11, 38, 'Синий', '4861cb'),
(40, 11, 39, 'Коричневый', '6e4a32'),
(41, 11, 40, 'Зеленый', '6e4a32'),
(42, 11, 41, 'Серый', '9b9b9b'),
(43, 11, 42, 'Оранжевый', 'ed871b'),
(44, 11, 43, 'Фиолетовый', '8a25e8'),
(45, 11, 44, 'Красный', 'ef2222'),
(47, 12, 46, 'Для ремня', NULL),
(48, 12, 47, 'Для сумки', NULL),
(49, 12, 48, 'Для кошелька', NULL),
(56, 13, 55, 'Краски', NULL),
(57, 13, 56, 'Финиш', NULL),
(58, 13, 57, 'Крема по уходу', NULL),
(59, 13, 58, 'Масла', NULL),
(60, 13, 59, 'Обработка уреза', NULL),
(61, 13, 60, 'Клей', NULL),
(62, 16, 63, '125', NULL),
(63, 16, 61, '35', NULL),
(64, 16, 62, '106', NULL),
(65, 16, 64, '260', NULL),
(66, 15, 65, '100', NULL),
(67, 10, 66, '1', ''),
(68, 10, 67, '60', ''),
(69, 10, 68, '61', ''),
(70, 10, 69, '62', ''),
(71, 10, 70, '63', ''),
(72, 10, 71, '64', ''),
(73, 10, 72, '65', ''),
(74, 10, 73, '66', ''),
(75, 10, 74, '67', ''),
(76, 10, 75, '68', ''),
(77, 10, 76, '69', ''),
(78, 10, 77, '70', ''),
(79, 11, 78, 'Никель', '#c0c0c0'),
(80, 11, 79, 'Латунь', '#ffff00'),
(81, 11, 80, 'Старая Латунь', '#808000'),
(82, 11, 81, 'Золото ', '#ffff00'),
(83, 11, 82, 'Старое серебро', '#808080'),
(84, 2, 83, '0,55 мм', ''),
(85, 2, 84, '0,5 мм', ''),
(86, 2, 85, '0,45 мм', ''),
(87, 2, 86, '0,6 мм', ''),
(88, 2, 87, '0,65 мм', ''),
(89, 2, 88, '0,7 мм', ''),
(90, 2, 89, '0,75 мм', ''),
(91, 2, 90, '0,8 мм', ''),
(92, 2, 91, '0,85 мм', ''),
(93, 2, 92, '0,9 мм', '');

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_category`
--

CREATE TABLE `mvc_store_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  `external_id` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_canonical` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `image_title` varchar(255) DEFAULT NULL,
  `view` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1820 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_category`
--

INSERT INTO `mvc_store_category` (`id`, `parent_id`, `slug`, `name`, `image`, `short_description`, `description`, `meta_title`, `meta_description`, `meta_keywords`, `status`, `sort`, `external_id`, `title`, `meta_canonical`, `image_alt`, `image_title`, `view`) VALUES
(1, NULL, 'katalog-kozhi', 'Каталог кожи', '581ffca3cf3b937f68cd5b94f3da19b5.jpg', '<p>Разнообразный и богатый опыт начало повседневной работы по \r\nформированию позиции обеспечивает широкому кругу (специалистов) участие в\r\n формировании</p>', '<p>Разнообразный и богатый опыт начало повседневной работы по \r\nформированию позиции обеспечивает широкому кругу (специалистов) участие в\r\n формировании</p>', '', '', '', 1, 1, NULL, '', '', '', '', ''),
(2, NULL, 'soputstvuyushchie-tovary', 'Сопутствующие товары', NULL, '', '', '', '', '', 1, 2, NULL, '', '', '', '', ''),
(3, NULL, 'rasprodazha', 'Распродажа', NULL, '', '', '', '', '', 1, 3, NULL, '', '', '', '', ''),
(6, 1, 'kozha', 'Кожа', '9102b3f5726ae08891472ee22105364d.jpg', '<p>Разнообразный и богатый опыт начало повседневной работы по формированию позиции обеспечивает широкому кругу (специалистов) участие в формировании</p>', '', '', '', '', 0, 4, NULL, '', '', '', '', ''),
(7, 2, 'furnitura', 'Фурнитура', '7cda0a2e55d7c0d8df335b8436914499.jpg', '<p>Разнообразный и богатый опыт начало повседневной работы по формированию позиции обеспечивает широкому кругу (специалистов) участие в формировании</p>', '', '', '', '', 1, 5, NULL, '', '', '', '', ''),
(8, 2, 'nit', 'Нить', 'a44ab598e64525831c129e2765c11535.jpg', '<p>Разнообразный и богатый опыт начало повседневной работы по формированию позиции обеспечивает широкому кругу (специалистов) участие в формировании</p>', '', '', '', '', 1, 6, NULL, '', '', '', '', ''),
(9, 2, 'himiya', 'Химия', '67946e5fcc9de50d498d561e5b213a88.jpg', '<p>Разнообразный и богатый опыт начало повседневной работы по формированию позиции обеспечивает широкому кругу (специалистов) участие в формировании</p>', '', '', '', '', 1, 7, NULL, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_coupon`
--

CREATE TABLE `mvc_store_coupon` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `code` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `value` decimal(10,2) NOT NULL DEFAULT '0.00',
  `min_order_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `registered_user` tinyint(4) NOT NULL DEFAULT '0',
  `free_shipping` tinyint(4) NOT NULL DEFAULT '0',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `quantity_per_user` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_delivery`
--

CREATE TABLE `mvc_store_delivery` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `price` float(10,2) NOT NULL DEFAULT '0.00',
  `free_from` float(10,2) DEFAULT NULL,
  `available_from` float(10,2) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `separate_payment` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_delivery`
--

INSERT INTO `mvc_store_delivery` (`id`, `name`, `description`, `price`, `free_from`, `available_from`, `position`, `status`, `separate_payment`) VALUES
(1, 'Почта России', '', 0.00, NULL, NULL, 1, 1, 0),
(2, 'Курьерская служба СДЕК', '', 0.00, NULL, NULL, 2, 1, 0),
(3, 'Транспортная компания КИТ, ПЭК', '', 0.00, NULL, NULL, 3, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_delivery_payment`
--

CREATE TABLE `mvc_store_delivery_payment` (
  `delivery_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_order`
--

CREATE TABLE `mvc_store_order` (
  `id` int(11) NOT NULL,
  `delivery_id` int(11) DEFAULT NULL,
  `delivery_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `payment_method_id` int(11) DEFAULT NULL,
  `paid` tinyint(4) DEFAULT '0',
  `payment_time` datetime DEFAULT NULL,
  `payment_details` text,
  `total_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `coupon_discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `separate_delivery` tinyint(4) DEFAULT '0',
  `status_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `street` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `comment` varchar(1024) NOT NULL DEFAULT '',
  `ip` varchar(15) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `note` varchar(1024) NOT NULL DEFAULT '',
  `modified` datetime DEFAULT NULL,
  `zipcode` varchar(30) DEFAULT NULL,
  `country` varchar(150) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `house` varchar(50) DEFAULT NULL,
  `apartment` varchar(10) DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `address_delivery` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_order`
--

INSERT INTO `mvc_store_order` (`id`, `delivery_id`, `delivery_price`, `payment_method_id`, `paid`, `payment_time`, `payment_details`, `total_price`, `discount`, `coupon_discount`, `separate_delivery`, `status_id`, `date`, `user_id`, `name`, `street`, `phone`, `email`, `comment`, `ip`, `url`, `note`, `modified`, `zipcode`, `country`, `city`, `house`, `apartment`, `manager_id`, `address_delivery`) VALUES
(1, 1, '0.00', NULL, 0, NULL, NULL, '28.00', '0.00', '0.00', 0, 1, '2019-11-04 11:25:54', NULL, 'test', '', '+7-247-293-4293', 'doctordrethe@bk.ru', '', '94.41.185.13', 'e877e7fb53efcf5b2b9158fd63bf931f', '', '2019-11-04 11:25:54', '', '', '', '', '', NULL, NULL),
(2, 1, '0.00', NULL, 0, NULL, NULL, '80.00', '0.00', '0.00', 0, 1, '2019-11-04 11:43:53', 6, 'test', '', '+7-342-342-3423', 'doctordrethe@bk.ru', '', '94.41.185.13', '364822df0585483c15c3053c942c53a2', '', '2019-11-04 11:43:53', '', '', '', '', '', NULL, NULL),
(3, 2, '0.00', NULL, 0, NULL, NULL, '3016.00', '0.00', '0.00', 0, 1, '2020-03-03 08:48:57', 4, 'Тестовый Тест Тестович', '', '', 'dekanrector@mail.ru', '', '94.41.170.128', 'ee6fec56c0f6566b38d4435544a71f7f', '', '2020-03-03 08:48:57', '', '', '', '', '', NULL, 'Оренбург, Восточная 42'),
(4, 2, '265.00', 1, 0, NULL, NULL, '6148.00', '0.00', '0.00', 0, 2, '2020-03-03 08:52:24', 18, 'Мразовский Стас Александрович', '', '', 'n_etu@mail.ru', '', '94.41.170.128', 'd2f09af15d102d71415911540d1e98b6', 'Вот на эту карту плати 2555555', '2020-03-03 09:37:44', '', '', '', '', '', 18, 'Восточная 42'),
(5, 1, '0.00', NULL, 0, NULL, NULL, '336.40', '0.00', '0.00', 0, 1, '2020-03-05 11:09:05', 4, 'Администратор Сайта Сего', '', '+7-999-999-9999', 'dekanrector@mail.ru', '', '94.41.140.93', 'bd9b69667cb6d38f64a14a48b2c25b3c', '', '2020-03-05 11:09:05', '', '', '', '', '', NULL, 'Оренбург, Восточная 42');

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_order_coupon`
--

CREATE TABLE `mvc_store_order_coupon` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_order_product`
--

CREATE TABLE `mvc_store_order_product` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `variants` text,
  `variants_text` varchar(1024) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `sku` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_order_product`
--

INSERT INTO `mvc_store_order_product` (`id`, `order_id`, `product_id`, `product_name`, `variants`, `variants_text`, `price`, `quantity`, `sku`) VALUES
(1, 1, 9, 'Vegetal 1.4-1.6 мм Лот Cognac Delux!', 'a:0:{}', NULL, '28.00', 1, ''),
(2, 2, 18, 'Карабин 15 мм арт. PPD0198-15', 'a:0:{}', NULL, '80.00', 1, ''),
(3, 3, 9, 'Vegetal 1.4-1.6 мм Лот Cognac Delux!', 'a:0:{}', NULL, '29.00', 104, ''),
(4, 4, 9, 'Vegetal 1.4-1.6 мм Лот Cognac Delux!', 'a:1:{i:0;a:14:{s:2:\"id\";s:2:\"32\";s:10:\"product_id\";s:1:\"9\";s:12:\"attribute_id\";s:2:\"10\";s:15:\"attribute_value\";s:3:\"104\";s:4:\"type\";s:1:\"3\";s:3:\"sku\";s:0:\"\";s:8:\"position\";s:2:\"20\";s:8:\"quantity\";s:1:\"1\";s:9:\"option_id\";s:2:\"32\";s:9:\"parent_id\";s:2:\"32\";s:6:\"amount\";s:3:\"104\";s:14:\"attribute_name\";s:9:\"razmer-dm\";s:15:\"attribute_title\";s:19:\"Размер (дм)\";s:11:\"optionValue\";s:3:\"104\";}}', NULL, '29.00', 104, ''),
(6, 4, 9, 'Vegetal 1.4-1.6 мм Лот Cognac Delux!', 'a:1:{i:0;a:14:{s:2:\"id\";s:2:\"33\";s:10:\"product_id\";s:1:\"9\";s:12:\"attribute_id\";s:2:\"10\";s:15:\"attribute_value\";s:3:\"108\";s:4:\"type\";s:1:\"3\";s:3:\"sku\";s:0:\"\";s:8:\"position\";s:2:\"21\";s:8:\"quantity\";s:1:\"2\";s:9:\"option_id\";s:2:\"33\";s:9:\"parent_id\";s:2:\"33\";s:6:\"amount\";s:3:\"108\";s:14:\"attribute_name\";s:9:\"razmer-dm\";s:15:\"attribute_title\";s:19:\"Размер (дм)\";s:11:\"optionValue\";s:3:\"108\";}}', NULL, '29.00', 108, ''),
(7, 5, 9, 'Vegetal 1.4-1.6 мм Лот Cognac Delux!', 'a:2:{i:0;a:14:{s:2:\"id\";s:2:\"34\";s:10:\"product_id\";s:1:\"9\";s:12:\"attribute_id\";s:2:\"10\";s:15:\"attribute_value\";s:3:\"116\";s:4:\"type\";s:1:\"3\";s:3:\"sku\";s:0:\"\";s:8:\"position\";s:2:\"22\";s:8:\"quantity\";s:1:\"2\";s:9:\"option_id\";s:2:\"34\";s:9:\"parent_id\";N;s:6:\"amount\";s:4:\"11.6\";s:14:\"attribute_name\";s:9:\"razmer-dm\";s:15:\"attribute_title\";s:19:\"Размер (дм)\";s:11:\"optionValue\";s:3:\"116\";}i:1;a:14:{s:2:\"id\";s:2:\"59\";s:10:\"product_id\";s:1:\"9\";s:12:\"attribute_id\";s:2:\"11\";s:15:\"attribute_value\";s:2:\"37\";s:4:\"type\";s:1:\"2\";s:3:\"sku\";s:0:\"\";s:8:\"position\";s:2:\"46\";s:8:\"quantity\";N;s:9:\"option_id\";N;s:9:\"parent_id\";s:2:\"32\";s:6:\"amount\";s:2:\"29\";s:14:\"attribute_name\";s:4:\"cvet\";s:15:\"attribute_title\";s:8:\"Цвет\";s:11:\"optionValue\";s:12:\"Желтый\";}}', NULL, '336.40', 1, '');

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_order_status`
--

CREATE TABLE `mvc_store_order_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  `color` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_order_status`
--

INSERT INTO `mvc_store_order_status` (`id`, `name`, `is_system`, `color`) VALUES
(1, 'Новый', 1, 'default'),
(2, 'Принят', 1, 'info'),
(3, 'Выполнен', 1, 'success'),
(4, 'Удален', 1, 'danger');

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_payment`
--

CREATE TABLE `mvc_store_payment` (
  `id` int(11) NOT NULL,
  `module` varchar(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `settings` text,
  `currency_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_payment`
--

INSERT INTO `mvc_store_payment` (`id`, `module`, `name`, `description`, `settings`, `currency_id`, `position`, `status`) VALUES
(1, 'manual', 'Оплата на карту Сбербанка/Тинькофф банка', '', 'a:0:{}', NULL, 1, 1),
(2, 'manual', 'Оплата на Paypal', '', 'a:0:{}', NULL, 2, 1),
(3, 'manual', 'Оплата на расчетный счет', '', 'a:0:{}', NULL, 3, 1),
(4, 'manual', 'Денежные переводы Золотая корона', '', 'a:0:{}', NULL, 4, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_producer`
--

CREATE TABLE `mvc_store_producer` (
  `id` int(11) NOT NULL,
  `name_short` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '0',
  `view` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_producer`
--

INSERT INTO `mvc_store_producer` (`id`, `name_short`, `name`, `slug`, `image`, `short_description`, `description`, `meta_title`, `meta_keywords`, `meta_description`, `status`, `sort`, `view`) VALUES
(1, 'Nuova Grenoble', 'Nuova Grenoble', 'nuova-grenoble', NULL, '', '', '', '', '', 1, 1, ''),
(2, 'Ecopell', 'Ecopell', 'ecopell', NULL, '', '', '', '', '', 1, 2, ''),
(3, 'Anna Rita', 'Anna Rita', 'anna-rita', NULL, '', '', '', '', '', 1, 3, ''),
(4, 'Fiebing\'s', 'Fiebing\'s', 'fiebings', NULL, '', '', '', '', '', 1, 4, '');

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_product`
--

CREATE TABLE `mvc_store_product` (
  `id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `producer_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sku` varchar(100) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `price` decimal(19,3) NOT NULL DEFAULT '0.000',
  `discount_price` decimal(19,3) DEFAULT NULL,
  `discount` decimal(19,3) DEFAULT NULL,
  `description` text,
  `short_description` text,
  `data` text,
  `is_special` tinyint(1) NOT NULL DEFAULT '0',
  `length` decimal(19,3) DEFAULT NULL,
  `width` decimal(19,3) DEFAULT NULL,
  `height` decimal(19,3) DEFAULT NULL,
  `weight` decimal(19,3) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `in_stock` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `average_price` decimal(19,3) DEFAULT NULL,
  `purchase_price` decimal(19,3) DEFAULT NULL,
  `recommended_price` decimal(19,3) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `external_id` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_canonical` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `image_title` varchar(255) DEFAULT NULL,
  `view` varchar(100) DEFAULT NULL,
  `link_youtube` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=2849 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_product`
--

INSERT INTO `mvc_store_product` (`id`, `type_id`, `producer_id`, `category_id`, `sku`, `name`, `slug`, `price`, `discount_price`, `discount`, `description`, `short_description`, `data`, `is_special`, `length`, `width`, `height`, `weight`, `quantity`, `in_stock`, `status`, `create_time`, `update_time`, `meta_title`, `meta_keywords`, `meta_description`, `image`, `average_price`, `purchase_price`, `recommended_price`, `position`, `external_id`, `title`, `meta_canonical`, `image_alt`, `image_title`, `view`, `link_youtube`) VALUES
(9, 5, 1, 1, '', 'Vegetal 1.4-1.6 мм Лот Cognac Delux!', 'vegetal-14-16-mm-lot-cognac-delux', '29.000', NULL, NULL, '<p>Кожа растительного дубления, часть шкуры 1\\2 culatta - фабрикf Nuova Grenoble (Италия). Это лицевая кожа с легкой винтажной разбивкой, шикарным окрасом, с приятным классическим глянцем, с финишем.\nДля чего она создана - для таких изделий как сумки, рюкзаки, дорожные сумки, для чехлов, папок, для браслетов, для обуви и т.д.\nОбратите внимание, кожа - \" живой материал\", на коже даже первого сорта возможно наличие небольших шрамов полученных животным при жизни, укусов насекомых, потертости, небольших дефектов полученных при выделки кожи до 10 % от общей площади (обязательно учитывайте это при покупке).\nИз-за сложности цветопередачи и разнице в настройках мониторов - возможно небольшое расхождение в цвете.\nЭто не постоянный ассортимент - цена шикарная!\nОбратите внимание это отрезы - потому цена очень выгодная!\nЛот продается целиком!\n</p>', '', '', 0, NULL, NULL, NULL, NULL, 10, 1, 1, '2019-10-04 13:16:39', '2020-03-05 10:38:05', '', '', '', '39698a1bfa7e9ccfdb31a0996aa3f1b2.jpg', NULL, NULL, NULL, 8, NULL, '', '', '', '', '', 'https://www.youtube.com/watch?v=j3noRfsJwEk'),
(10, 5, 1, 1, '', 'Vegetal 1.5-1.7 мм Avancorpo-Pull up Tiger', 'vegetal-15-17-mm-avancorpo-pull-up-tiger', '32.000', NULL, NULL, '<p>Кожа растительного дубления, Аванкорпо от фабрики Nuova Grenoble (Италия). Это лицевая кожа лошади, без разбивки, гладкая, с красивым эффектом пулл ап, матовая.\r\n</p><p>Кожа в меру пластичная, при этом способна держать форму.\r\n</p><p>Для чего она создана - для таких изделий шикарных изделий как сумки, рюкзаки, дорожные сумки, для чехлов, папок, для браслетов, для обуви и т.д.\r\n</p><p>Обратите внимание, кожа - \" живой материал\", на коже даже первого сорта возможно наличие небольших шрамов полученных животным при жизни, укусов насекомых, потертости, небольших дефектов полученных при выделки кожи до 10 % от общей площади (обязательно учитывайте это при покупке).\r\n</p><p>Из-за сложности цветопередачи и разнице в настройках мониторов - возможно небольшое расхождение в цвете.\r\n</p><p>Обратите внимание кожа немного подмята при транспортировке - см фото, потому цена не 35 р, а 32 р.\r\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 13:20:02', '2020-02-07 16:14:52', '', '', '', '70d1e47c4233c73963af23ef30dae527.jpg', NULL, NULL, NULL, 9, NULL, '', '', '', '', '', ''),
(11, NULL, 1, 1, '', 'Vegetal -1,4-1,6 мм! Vacchetta -Nero!', 'vegetal-14-16-mm-vacchetta-nero', '28.000', NULL, NULL, '<p>Кожа растительного дубления, часть шкуры пола от фабрики Nuova Grenoble (Италия). Это лицевая кожа с легкой винтажной разбивкой, сквозного окраса, матовая, с финишем.\r\n</p><p>Кожа в достаточно пластичная, при этом способна немного держать форму. Очень красивый лицевой рисунок.\r\n</p><p>Для чего она создана - для таких изделий как сумки, рюкзаки, дорожные сумки, для чехлов, папок, для браслетов, для обуви и т.д.\r\n</p><p>Обратите внимание, кожа - \" живой материал\", на коже даже первого сорта возможно наличие небольших шрамов полученных животным при жизни, укусов насекомых, потертости, небольших дефектов полученных при выделки кожи до 10 % от общей площади (обязательно учитывайте это при покупке).\r\n</p><p>Из-за сложности цветопередачи и разнице в настройках мониторов - возможно небольшое расхождение в цвете.\r\n</p><p>Это не постоянный ассортимент - цена шикарная!\r\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 13:20:59', '2019-10-07 15:07:57', '', '', '', '668386c69d51150251ae721406bee368.jpg', NULL, NULL, NULL, 10, NULL, '', '', '', '', '', NULL),
(12, NULL, 1, 1, '', 'Vegetal 1.8-2.0 мм Grana-Testa di Moro', 'vegetal-18-20-mm-grana-testa-di-moro', '28.000', NULL, NULL, '<p>Новая партия кожи растительного дубления!!!\r\n</p><p>Качественная кожа belly (часть шкуры - пола)! Фабрика Nuova Grenoble Италия. Это лицевая кожа для создания которой использовались анилиновые красители высокого уровня экологичности и качества, кожа равномерного классического цвета, чуть сглаженное лицо, устойчивая к царапинкам, с матовым анилиновым финишем, оборотная сторона кожи выделана и не требует дополнительной обработки.\r\n</p><p>Обратите внимание на размеры и форму шкур - очень удобны в процессе грамотного раскроя и позволяют максимально задействовать всю полезную площадь belly.\r\n</p><p>Кожа плотная, способна хорошо держать форму, хорошо ложится в изделие. Толщина кожи 1,8-2.0 мм.\r\n</p><p>Натуральная итальянская кожа подойдет для изготовления и дизайна сумок, рюкзаков, клатчей, портмоне, обуви!\r\n</p><p>Не забываем читать правила магазина.\r\n</p><p>Шкуры продаются целиком! Если в разделе размеры стоит слово \"резерв\" - это значит кожу уже забрали.\r\n</p><p>Обратите внимание- кожа своего рода \" живой материал\" на коже возможно наличие небольших шрамов полученных животным при жизни, дырочек (не более 10% для 1 сорта) воротистость, кожа имеет свой индивидуальный рисунок присущий каждой шкуре - данные свойства дефектом не считаются.\r\n</p><p>Шикарное качество по отличной цене!\r\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 13:22:58', '2019-10-07 15:08:39', '', '', '', '21b6a16cb0b6281f4ca9af0d2581e74d.jpg', NULL, NULL, NULL, 11, NULL, '', '', '', '', '', NULL),
(13, NULL, 1, 1, '', 'Vegetal -1,3-1,5 мм! Varsavia -Camouflage!', 'vegetal-13-15-mm-varsavia-camouflage', '28.000', NULL, NULL, '<p>Шикарный качественный анилиновый принт! Кожа растительного дубления, часть шкуры пола от фабрики Nuova Grenoble (Италия). Это лицевая кожа с легкой винтажной разбивкой и качественным принтом в виде камуфляжа, лицевая поверхность, матовая, с финишем.\r\n</p><p>Кожа в достаточно пластичная, при этом способна в меру держать форму. Очень красивый лицевой рисунок.\r\n</p><p>Для чего она создана - для таких изделий как сумки, рюкзаки, дорожные сумки, для чехлов, папок, для браслетов, для обуви и т.д.\r\n</p><p>Обратите внимание, кожа - \" живой материал\", на коже даже первого сорта возможно наличие небольших шрамов полученных животным при жизни, укусов насекомых, потертости, небольших дефектов полученных при выделки кожи до 10 % от общей площади (обязательно учитывайте это при покупке).\r\n</p><p>Из-за сложности цветопередачи и разнице в настройках мониторов - возможно небольшое расхождение в цвете.\r\n</p><p>Это не постоянный ассортимент - цена шикарная!\r\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 13:23:59', '2019-10-07 15:09:10', '', '', '', '74ea033950f73322411b79c3b92441f6.jpg', NULL, NULL, NULL, 12, NULL, '', '', '', '', '', NULL),
(14, NULL, NULL, 1, '', 'Натуральная кожа 1.4-1.6 мм Dotic Grey', 'naturalnaya-kozha-14-16-mm-dotic-grey', '18.000', NULL, NULL, '<p>Отличная цена!!\r\n</p><p>Натуральная итальянская кожа крс - шикарный матовый саффиано с красивым оригинальным тиснением (см фото), способна хорошо держать форму, с отличными защитными свойствами (антицарапина). Шикарная оборотная сторона, сглажена в тон лицу, сквозной окрас.\r\n</p><p>Толщина кожи 1,4-1,6 мм, подойдет для изготовления сумок, рюкзаков, портмоне, папок и т.д.\r\n</p><p>Не забываем читать правила магазина.\r\n</p><p>Шкуры режем с учетом их природных особенностей. Цена указана за 1 кв дм.\r\n</p><p>Если в разделе размеры стоит слово \"резерв\" - это значит кожу уже забрали.\r\n</p><p>Обратите внимание!! - кожа своего рода \" живой материал\" на коже возможно наличие небольших шрамов полученных животным при жизни, воротистость, кожа имеет свой индивидуальный рисунок присущий каждой шкуре - данные свойства дефектом не считаются.\r\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 13:25:26', '2019-10-07 15:09:39', '', '', '', '2531807566851e6030578eca43224365.jpg', NULL, NULL, NULL, 13, NULL, '', '', '', '', '', NULL),
(15, NULL, NULL, 1, '', 'Натуральная кожа-New 0,9-1,0 мм Mistery Azure', 'naturalnaya-kozha-new-09-10-mm-mistery-azure', '18.000', NULL, NULL, '<p>Натуральная итальянская кожа крс (полушкура) - с красивым натуральным лицом с легким зерном, с винтажным напылением спокойно-мятного цвета. Отличное качество, устойчивый цвет, отлично драпируется, с отличной обороткой.\r\n</p><p>Шикарная выделка и отличные тактильные свойства.\r\n</p><p>Толщина кожи 0,9-1,0 мм, подойдет для изготовления сумок, рюкзаков, портмоне, кошельков и т.д..\r\n</p><p>Не забываем читать правила магазина.\r\n</p><p>Шкуры продаются целиком! Если в разделе размеры стоит слово \"резерв\" - это значит кожу уже забрали.\r\n</p><p>Обратите внимание!! - кожа своего рода \" живой материал\" на коже возможно наличие небольших шрамов полученных животным при жизни, воротистость, кожа имеет свой индивидуальный рисунок присущий каждой шкуре - данные свойства дефектом не считаются.\r\n</p><p>Кожа идет по отличной акции - некоторые шкуры немного подмяты при транспортировке.\r\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 13:26:21', '2019-10-07 15:10:10', '', '', '', '197590dcd2f63b28a9aea969d368e904.jpg', NULL, NULL, NULL, 14, NULL, '', '', '', '', '', NULL),
(16, NULL, 2, 1, '', 'Натуральная кожа -Лот Marcipano - Cremano &amp; Caramel!', 'naturalnaya-kozha-lot-marcipano-cremano-caramel', '22.000', NULL, NULL, '<p>Отлично смотрится в изделиях и прекрасно сочетается между собой!\r\n</p><p>Кожа хромового дубления, фабрика Ecopell (Италия). Это лицевая кожа крс, полушкура, сквозного окраса, с качественным устойчивым финишем, с легким эффектом жатки, очень оригинально смотрится в изделиях.\r\n</p><p>Кожа в меру пластичная, при этом способна в меру держать форму.\r\n</p><p>Для чего она создана - для таких изделий как сумки, рюкзаки, дорожные сумки, для чехлов, папок, для браслетов, для обуви и т.д.\r\n</p><p>Обратите внимание, кожа - \" живой материал\", на коже даже первого сорта возможно наличие небольших шрамов полученных животным при жизни, укусов насекомых, потертости, небольших дефектов полученных при выделки кожи до 10 % от общей площади (обязательно учитывайте это при покупке).\r\n</p><p>Из-за сложности цветопередачи и разнице в настройках мониторов - возможно небольшое расхождение в цвете.\r\n</p><p>В реале цвет чуть насыщеннее, чем на фото.\r\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 13:27:07', '2019-10-07 15:10:39', '', '', '', '5bb203920ae9ee9ec502375693d7bce7.jpg', NULL, NULL, NULL, 15, NULL, '', '', '', '', '', NULL),
(17, NULL, 3, 1, '', 'Натуральная кожа 1.2-1.4 мм Crazy Horse-Jeans', 'naturalnaya-kozha-12-14-mm-crazy-horse-jeans', '28.000', NULL, NULL, '<p>Акция!!! Вместо 34 р - сейчас 28 р за 1 кв дм!\r\n</p><p>Очень красивый и необычный цвет.\r\n</p><p>Премиальное качество - отлично держит форму.\r\n</p><p>Натуральная итальянская кожа крс в полушкурах с эффектом крейзи хорс. Шикарный глубокий цвет и контрастный рисунок при загибах и заломах свидетельствует о качестве данной кожи. Крейзи каким он и должен быть в идеале)\r\n</p><p>Изготовитель conceria pellami Anna Rita (Италия).\r\n</p><p>Кожа в меру плотная, прочная, матовая, лицевая, имеет красивый глубокий окрас.\r\n</p><p>Отличная выделка, кожа в меру пластичная, хорошо ложится в изделие, с отличными тактильными свойствами. Толщина кожи 1,2-1,4 мм\r\n</p><p>Натуральная итальянская кожа подойдет для изготовления и дизайна сумок, рюкзаков, клатчей, портмоне, картхолдеров и т.д.\r\n</p><p>Работаем также и с оптом - если интересуют условия оптовых поставок - пишите в личное сообщение. Не забываем читать правила магазина.\r\n</p><p>Шкуры режем с учетом природных особенностей. Цена указана за 1 кв дм.\r\n</p><p>Если в разделе размеры стоит слово \"резерв\" - это значит кожу уже забрали.\r\n</p><p>Обратите внимание- кожа своего рода \" живой материал\" на коже возможно наличие небольших шрамов полученных животным при жизни, воротистость, кожа имеет свой индивидуальный рисунок присущий каждой шкуре - данные свойства дефектом не считаются.\r\n</p><p>Внимание - особенность выделки такой кожи как Crazy Horse допускает возможную НЕЗНАЧИТЕЛЬНУЮ отдачу цвета при трении, пожалуйста учитывайте это при приобретении данной кожи.\r\n</p><p>Эта кожа имеет свои особенности и требует определенного мастерства и знания в области изготовления изделий из нее.\r\n</p><p>Обратите внимание на фото - в реале цвет с сероватым оттенком\r\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 13:27:50', '2019-10-07 15:11:20', '', '', '', 'c116e7b946f079f02413a4f02bf0c52a.jpg', NULL, NULL, NULL, 16, NULL, '', '', '', '', '', NULL),
(18, 2, NULL, 7, '', 'Карабин 15 мм арт. PPD0198-15', 'karabin-15-mm-art-ppd0198-15', '80.000', NULL, NULL, '<p>Качественная фурнитура производства Италии.\n</p><p>Карабин.\n</p><p>Длина - 40 мм.\n</p><p>Ширина - 15 мм.\n</p><p>Цвет:\n</p><p>Никель=80 р\n</p><p>Золото=204 р\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 13:33:25', '2020-03-05 17:37:30', '', '', '', '03e6dbc1ed9f9f0134bc146b71bbb75f.jpg', NULL, NULL, NULL, 17, NULL, '', '', '', '', '', ''),
(19, 2, NULL, 7, '', 'Замок для портфеля арт. РРFB192', 'zamok-dlya-portfelya-art-rrfb192', '74.000', NULL, NULL, '<p>Качественная фурнитура производства Италии.\n</p><p>Замок для портфеля.\n</p><p>Высота - 31 мм.\n</p><p>Ширина - 29 мм.\n</p><p>Цвет:\n</p><p>Ст.латунь=74 р\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 13:34:07', '2019-12-04 20:21:39', '', '', '', '36fda312138bf82fb404cefa289b5d97.jpg', NULL, NULL, NULL, 18, NULL, '', '', '', '', '', NULL),
(20, 2, NULL, 7, '', 'Замок для портфеля арт. РРD870', 'zamok-dlya-portfelya-art-rrd870', '585.000', NULL, NULL, '<p>Качественная фурнитура производства Италии.\n</p><p>Замок для портфеля.\n</p><p>Высота - 51 мм.\n</p><p>Ширина - 43 мм.\n</p><p>Цвет:\n</p><p>Никель=585 р\n</p><p>Ст.латунь=585 р\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 13:34:40', '2019-12-04 20:21:50', '', '', '', 'ecd1838ab636b4d60e1aec310d6ee15e.jpg', NULL, NULL, NULL, 19, NULL, '', '', '', '', '', NULL),
(21, 2, NULL, 7, '', 'Рама саквояжная 30см, арт. PP81331', 'rama-sakvoyazhnaya-30sm-art-pp81331', '5630.000', NULL, NULL, '<p>Высококачественная фурнитура производства Италии.\n</p><p>Рама саквояжная .\n</p><p>Длина- 30 см, хольнитены в комплекте\n</p><p>Цвет:\n</p><p>Ст. сатин. латунь= 5630р\n</p><p>Для качественных, профессиональных работ. Очень дорого и эффектно смотрится в изделии.\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 13:35:22', '2019-12-04 20:22:02', '', '', '', '945c2ceb468a08b0b876931687f22d0a.jpg', NULL, NULL, NULL, 20, NULL, '', '', '', '', '', NULL),
(22, 3, NULL, 8, '', '0.7 мм Нитки KINT 125м круглые вощеные для ручного шва', '07-mm-nitki-kint-125m-kruglye-voshchenye-dlya-ruchnogo-shva', '460.000', NULL, NULL, '<p>Прочная вощеная нить для ручного шва. Толщина 0,7 мм. 125 метров. Нитки не расслаиваются и не ворсятся. Сильная износостойкость.\n</p><p>Прекрасно подойдут для производства качественной кожанной продукции - портмоне, сумки, кошельки, чехлы и т.д.\n</p><p>Для заказа пишите номер цвета.\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 13:40:01', '2020-03-03 12:12:57', '', '', '', 'fe944ed7b51c20853468a939f6d83103.jpg', NULL, NULL, NULL, 21, NULL, '', '', '', '', '', ''),
(23, 4, NULL, 9, '', 'Fiebing\'s -Antique Leather stain', 'fiebings-antique-leather-stain', '400.000', NULL, NULL, '<p>Акриловый антик на водной основе. Производитель Fiebing\'s (USA).\n</p><p>Доступные для заказа цвета Tan, Medium Brown, Dark Brown, Mahogany, Black.\n</p><p>Отличное средство для тонировки кожи - прекрасно подходит для выравнивания тона на коже растительного дубления, а также для создания винтажного и облачного эффекта на коже.\n</p><p>Интенсивность окраса можно легко изменять.\n</p><p>Антик очень прост в применении, наносится на поверхность при помощи губки, овчины, специального спонжа для краски. Далее влажной тканью убираем излишки пигмента, оставляя его в углублениях и порах кожи.\n</p><p>После высыхания нанести финиш на восковой основе.\n</p><p>Отличный эффект!\n</p><p>Внимание на фото продемонстрировано как можно выравнивать тон на поле растительного дубления цвета Papaya с укусами насекомых. Для контраста использовался более темный оттенок Антика - Dark Brown.\n</p><p>Обратите внимание для кожи цвета Papaya - идеально подходит оттенок Medium Brown.\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 13:40:45', '2019-12-05 19:48:00', '', '', '', 'bb0ab770c7b83da972879da4f4be613a.jpg', NULL, NULL, NULL, 22, NULL, '', '', '', '', '', NULL),
(24, NULL, NULL, 3, '', 'Натуральная кожа 1.2-1.4 мм Lucido Black Music (лак)', 'naturalnaya-kozha-12-14-mm-lucido-black-music-lak', '12.000', NULL, NULL, '<p>Вместо 20 р сейчас 12 р!\r\n</p><p>Натуральная лаковая итальянская кожа КРС шикарной выделки и качества. С фактурным лицевым эффектом.\r\n</p><p>Прекрасно подойдет для эксклюзивных изделий из кожи - сумки, клатчи, кошельки, портмоне, ремни и т.д.\r\n</p><p>Данная кожа пластичная, отлично ложится в изделие, имеет отличную оборотную сторону.\r\n</p><p>Толщина 1,2-1,4 мм !\r\n</p><p>Шкуры режем с учетом их природных особенностей. Цена указана за 1 кв дм.\r\n</p><p>Если в разделе размеры стоит слово \"резерв\" - это значит кожу уже забрали.\r\n</p><p>Обратите внимание- кожа своего рода \" живой материал\" на коже возможно наличие небольших шрамов полученных животным при жизни, воротистость, кожа имеет свой индивидуальный рисунок присущий каждой шкуре - данные свойства дефектом не считаются.\r\n</p><p>Внимательно читаем правила магазина!!!\r\n</p><p>На вопросы, на которые есть ответы в правилах магазина - не отвечаю, пожалуйста, отнеситесь к этому с пониманием.\r\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 14:18:06', '2019-10-07 15:11:59', '', '', '', '590235aa7054b0a8a0fde25f398f0174.jpg', NULL, NULL, NULL, 23, NULL, '', '', '', '', '', NULL),
(25, NULL, NULL, 3, '', 'Натуральная кожа- 1,5-1,7 мм Volo-Avio', 'naturalnaya-kozha-15-17-mm-volo-avio', '14.000', NULL, NULL, '<p>Вместо 18 теперь 14 р!!!\r\n</p><p>Натуральная итальянская кожа крс красивого средне-синего цвета.\r\n</p><p>Кожа гладкая, с отличной обороткой, сквозного окраса.\r\n</p><p>Толщина кожи 1,5-1,7 мм, достаточно пластичная, способна в меру держать форму, подойдет для изготовления сумок, рюкзаков, портмоне, кошельков и т.д..\r\n</p><p>Не забываем читать правила магазина.\r\n</p><p>Шкуры режем с учетом их природных особенностей. Цена указана за 1 кв дм.\r\n</p><p>Если в разделе размеры стоит слово \"резерв\" - это значит кожу уже забрали.\r\n</p><p>Обратите внимание- кожа своего рода \" живой материал\" на коже возможно наличие небольших шрамов полученных животным при жизни, воротистость, кожа имеет свой индивидуальный рисунок присущий каждой шкуре - данные свойства дефектом не считаются.\r\n</p><p>256=3584 р резев Катерина\r\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 14:18:50', '2019-10-07 15:12:31', '', '', '', 'ff8f509b8c2a158f263b25080d1dc3c5.jpg', NULL, NULL, NULL, 24, NULL, '', '', '', '', '', NULL),
(26, NULL, NULL, 2, '', 'Ременные заготовки - 3,4-3,5 мм! Wildberry!', 'remennye-zagotovki-34-35-mm-wildberry', '520.000', NULL, NULL, '<p>Ременные заготовки из кожи растительного дубления хорошего качества. Италия. Длина ременных заготовок 120 см+. Ширина заготовок 40 мм, но также сделаем нарезку под вашу ширину. Эта кожа сквозного анилинового крашения, с натуральным нескорректированным лицом, оборотная сторона открыта, имеет короткую мездру, без финиша!\r\n</p><p>Для чего она создана - для ремней различного назначения, для браслетов, для конной амуниции и т.д.\r\n</p><p>Обратите внимание - кожа своего рода \" живой материал\", возможно наличие небольших шрамов полученных животным при жизни, укусов насекомых, воротистость, кожа имеет свой индивидуальный рисунок присущий каждой шкуре - данные свойства дефектом не считаются.\r\n</p><p>Обратите внимание - на коже могут быть незначительные дефекты - следы укусов насекомых.\r\n</p><p>Отличный вариант - за выгодную цену.\r\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 14:24:56', '2019-10-07 14:23:40', '', '', '', 'e235ae06a27b551ecffd14aa41897c5c.jpg', NULL, NULL, NULL, 25, NULL, '', '', '', '', '', NULL),
(27, NULL, NULL, 2, '', 'Vegetal 1.0-1.1 мм Steel Silver', 'vegetal-10-11-mm-steel-silver', '24.000', NULL, NULL, '<p>Новое поступление кожи растительного дубления!!!\r\n</p><p>Это хорошего качества belly (часть шкуры пола). Обратите внимание в отличие от российской полы - итальянская имеет расширенную площадь и в основном идет в размере более 70 кв дм, что очень удобно при раскрое. Производство Италия. Это лицевая кожа с качественной ламинацией под серебро, выполненной на лицевой коже, со спокойным блеском, оборотная сторона кожи цвета натурале, сглажена.\r\n</p><p>Кожа в меру пластичная, способна держать форму, хорошо ложится в изделие. Толщина кожи 1,0-1,1 мм.\r\n</p><p>Натуральная итальянская кожа подойдет для изготовления и дизайна сумок, рюкзаков, клатчей, портмоне, обуви!\r\n</p><p>На последнем фото представлена сумка марки Campomaggi изготовленная из аналогичной кожи.\r\n</p><p>Не забываем читать правила магазина.\r\n</p><p>Шкуры режем с учетом их природных особенностей. Цена указана за 1 кв дм.\r\n</p><p>Если в разделе размеры стоит слово \"резерв\" - это значит кожу уже забрали.\r\n</p><p>Обратите внимание -цена на кожу снижена так как на коже возможно наличие дефектов полученных при транспортировке - немного подмята.\r\n</p><p>Обратите внимание- кожа своего рода \" живой материал\" на коже возможно наличие небольших шрамов полученных животным при жизни, воротистость, кожа имеет свой индивидуальный рисунок присущий каждой шкуре - данные свойства дефектом не считаются.\r\n</p><p>Обязательно учитывайте это совершая покупку!\r\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 14:27:17', '2019-10-07 15:13:13', '', '', '', 'ffc4149096d90a9218a451619f5dbba9.jpg', NULL, NULL, NULL, 26, NULL, '', '', '', '', '', NULL),
(28, NULL, NULL, 2, '', 'Заготовка для паспорта - Russo Turisto/Russia', 'zagotovka-dlya-pasporta-russo-turistorussia', '600.000', NULL, NULL, '<p>Каждая индивидуальна и неповторима за\r\n</p><p>счет живого лицевого рисунка на каждой поле растительного дубления.\r\n</p><p>Два вида тиснения на коже растительного дубления от фабрики La Perla Azzurra.\r\n</p><p>Артикул Missouri, коричневый и коньячный цвета.\r\n</p><p>Данный артикул смотрится очень красиво в подобных изделиях, со временем будет приобретать красивую патину. Представляем вашему вниманию заготовки для обложек на паспорт, автодокументы. Данные заготовки производят на профессиональном оборудовании при помощи нанесения высококачественного тиснения с точной передачей даже самых мелких элементов тиснения.\r\n</p><p>Прекрасное решение реализации новых направлений в вашем творчестве!!\r\n</p><p>Толщина кожи 1,2-1,4 мм.\r\n</p><p>Не забываем читать правила магазина.\r\n</p><p>Обратите внимание, заготовки можно тонировать и покрывать финишами, однако для понимания результата - проведите небольшой тест на коже которая идет на внутренний кармашек.\r\n</p><p>Обратите внимание!! - кожа своего рода \" живой материал\" на коже возможно наличие небольших шрамов, укусов насекомых, полученных животным при жизни, воротистость, кожа имеет свой индивидуальный рисунок присущий каждой шкуре - данные свойства дефектом не считаются.\r\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 14:28:31', '2019-10-07 15:13:44', '', '', '', 'fc7e0622e25ec1b843adbe59d48782b8.jpg', NULL, NULL, NULL, 27, NULL, '', '', '', '', '', NULL),
(29, NULL, NULL, 2, '', 'Vegetal 1.8-2.0 мм Обрезки кожи с тиснением', 'vegetal-18-20-mm-obrezki-kozhi-s-tisneniem', '850.000', NULL, NULL, '<p>Обрезки кожи растительного дубления в толщине 1,8-2,0 мм, на коже выполнено качественное тиснение. Обрезки идут двух видов: 1) это цвет натурале - с легкой обработкой воском 2) шоколадный цвет - это покрывного крашения кожа, с финишем.\r\n</p><p>Обрезки в цвете натурале можно покрасить, подтонировать (см вариант тонировки/окраса на фото), обработать кремом на восковой основе.\r\n</p><p>Кожа в меру пластичная, способна держать форму, хорошо ложится в изделие, прочная, подойдет для изготовления и декора сумок, рюкзаков, чехлов, браслетов, обложек и т.д..\r\n</p><p>Не забываем читать правила магазина.\r\n</p><p>Обратите внимание!! - размеры обрезков могут быть от 2 кв дм до размера листа формата А4,\r\n</p><p>- обрезки по размеру не сортируем,\r\n</p><p>- минимальный заказ 1 кг,\r\n</p><p>- возможна сортировка по цвету,\r\n</p><p>- на коже может быть светло-серый налет - это воск - просто разотрите его тканью и он впитается и придаст сатиновый блеск коже,\r\n</p><p>- на коже могут быть небольшие природные дефекты, свойственные для кожи растительного дубления,\r\n</p><p>- в 1 кг примерно 60-70 кв дм кожи,\r\n</p><p>- некоторые обрезки могут иметь запачканную оборотку.\r\n</p><p>Обратите внимание - на фото цвет может искажаться из за сложности цветопередачи и из-за разности настройки мониторов - учитывайте это при совершении покупки.\r\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 14:29:20', '2019-10-07 15:14:23', '', '', '', 'cd385e718d406650e6920f1d512c92d6.jpg', NULL, NULL, NULL, 28, NULL, '', '', '', '', '', NULL),
(30, NULL, NULL, 2, '', 'Ременные заготовки - 3,4-3,6 мм! Dakota-Sand', 'remennye-zagotovki-34-36-mm-dakota-sand', '850.000', NULL, NULL, '<p>Ременные заготовки из кожи растительного дубления премиум качества - фабрика La Perla Azzurra (Италия). Длина ременных заготовок 125 см +. Ширина заготовок 40 мм, но также сделаем нарезку под вашу ширину. Артикул Dacota- эта кожа сквозного анилинового крашения, с классическим гладким матовым лицом. Натуральный лицевой рисунок, с качественным финишем. Обратите внимание на качество прокраса и на урезы - они шикарны!!! Оборотная сторона сглажена и закрыта в тон лицу. Часть шкуры - двойные плечи. Сырье - бык, Франция. Кожа имеет потрясающий запах дорогой итальянской кожи растительного дубления.\r\n</p><p>Для чего она создана - для шикарных ремней различного назначения, для браслетов, для премиальной конной амуниции и т.д.\r\n</p><p>Обратите внимание - кожа своего рода \" живой материал\" на коже, даже первого сорта возможно наличие небольших шрамов полученных животным при жизни, укусов насекомых, воротистость, кожа имеет свой индивидуальный рисунок присущий каждой шкуре - данные свойства дефектом не считаются.\r\n</p><p>Обратите внимание в нашем магазине появились отличные пряжки Италия, качество шикарное. Скоро добавим их в позиции в наш магазин.\r\n</p><p>При заказе от 10 шт заготовок цена 800 р за 1 заготовку,\r\n</p><p>при заказе от 20 шт заготовок цена 750 р за 1 заготовку!\r\n</p><p>К каждой заготовке вы можете приобрести сертификат от Консорциума 1 шт =20 р.\r\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 14:30:14', '2019-10-07 15:14:54', '', '', '', '55d36dcc223100a1bdb2bbbe27313cb2.jpg', NULL, NULL, NULL, 29, NULL, '', '', '', '', '', NULL),
(31, NULL, NULL, 2, '', 'Заготовка для паспорта - Russo Turisto', 'zagotovka-dlya-pasporta-russo-turisto', '550.000', NULL, NULL, '<p>Представляем вашему вниманию заготовки для обложек на паспорт, автодокументы. Данные заготовки производят на профессиональном оборудовании при помощи нанесения высококачественного тиснения с точной передачей даже самых мелких элементов тиснения. Основа данной заготовки - итальянская кожа растительного дубления, которая представлена у нас в магазине, потому в качестве можете не сомневаться)\r\n</p><p>Прекрасное решение реализации новых направлений в вашем творчестве!!\r\n</p><p>Толщина кожи 1,6-1,8 мм.\r\n</p><p>Не забываем читать правила магазина.\r\n</p><p>Обратите внимание, некоторые заготовки уже подтонированы и обработаны качественными финишными средствами (Fiebing\'s или Kenda Farben)\r\n</p><p>Обратите внимание!! - кожа своего рода \" живой материал\" на коже возможно наличие небольших шрамов полученных животным при жизни, воротистость, кожа имеет свой индивидуальный рисунок присущий каждой шкуре - данные свойства дефектом не считаются.\r\n</p>', '', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-10-04 14:30:56', '2019-10-07 15:15:22', '', '', '', '50ab75aba0181ef80c2d871bd06b87e8.jpg', NULL, NULL, NULL, 30, NULL, '', '', '', '', '', NULL),
(32, 3, NULL, 8, '', 'Нитка Для шития', 'nitka-dlya-shitiya', '100.000', NULL, NULL, '<p>Хорошая Нить</p>', '<p>Бери и шей </p>', '', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-03-03 12:19:30', '2020-03-03 12:54:39', '', '', '', '9022d6b3064fd6bc6790bf0582933086.JPG', NULL, NULL, NULL, 31, NULL, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_product_attribute_value`
--

CREATE TABLE `mvc_store_product_attribute_value` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `number_value` double DEFAULT NULL,
  `string_value` varchar(250) DEFAULT NULL,
  `text_value` text,
  `option_value` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=144 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_product_attribute_value`
--

INSERT INTO `mvc_store_product_attribute_value` (`id`, `product_id`, `attribute_id`, `number_value`, `string_value`, `text_value`, `option_value`, `create_time`) VALUES
(36, 14, 1, NULL, '', NULL, NULL, NULL),
(40, 14, 5, NULL, 'Держит форму', NULL, NULL, NULL),
(43, 15, 1, NULL, '', NULL, NULL, NULL),
(51, 16, 2, NULL, '', NULL, NULL, NULL),
(57, 17, 1, NULL, '', NULL, NULL, NULL),
(64, 26, 1, NULL, '', NULL, NULL, NULL),
(65, 26, 2, NULL, '3,4-3,5', NULL, NULL, NULL),
(68, 26, 5, NULL, '', NULL, NULL, NULL),
(69, 26, 6, NULL, '', NULL, NULL, NULL),
(70, 26, 7, NULL, '', NULL, NULL, NULL),
(72, 27, 2, NULL, '1.0-1.1', NULL, NULL, NULL),
(91, 29, 7, NULL, '', NULL, NULL, NULL),
(93, 30, 2, NULL, '3,4-3,6 мм', NULL, NULL, NULL),
(96, 30, 5, NULL, '', NULL, NULL, NULL),
(100, 31, 2, NULL, ' 1,6-1,8 мм', NULL, NULL, NULL),
(106, 24, 1, NULL, '', NULL, NULL, NULL),
(113, 25, 1, NULL, '', NULL, NULL, NULL),
(126, 10, 1, NULL, NULL, NULL, 3, NULL),
(127, 10, 2, NULL, NULL, NULL, 6, NULL),
(128, 10, 5, NULL, NULL, NULL, 11, NULL),
(129, 10, 5, NULL, NULL, NULL, 13, NULL),
(130, 10, 6, NULL, NULL, NULL, 14, NULL),
(131, 10, 6, NULL, NULL, NULL, 15, NULL),
(132, 10, 6, NULL, NULL, NULL, 17, NULL),
(133, 10, 7, NULL, NULL, NULL, 20, NULL),
(134, 11, 1, NULL, NULL, NULL, 3, NULL),
(135, 11, 2, NULL, NULL, NULL, 5, NULL),
(136, 11, 5, NULL, NULL, NULL, 11, NULL),
(137, 11, 5, NULL, NULL, NULL, 13, NULL),
(138, 11, 6, NULL, NULL, NULL, 14, NULL),
(139, 11, 6, NULL, NULL, NULL, 15, NULL),
(140, 11, 6, NULL, NULL, NULL, 16, NULL),
(141, 11, 6, NULL, NULL, NULL, 17, NULL),
(142, 11, 7, NULL, NULL, NULL, 20, NULL),
(143, 12, 1, NULL, NULL, NULL, 3, NULL),
(144, 12, 2, NULL, NULL, NULL, 7, NULL),
(145, 12, 5, NULL, NULL, NULL, 13, NULL),
(146, 12, 6, NULL, NULL, NULL, 14, NULL),
(147, 12, 6, NULL, NULL, NULL, 15, NULL),
(148, 12, 6, NULL, NULL, NULL, 16, NULL),
(149, 12, 6, NULL, NULL, NULL, 17, NULL),
(150, 12, 7, NULL, NULL, NULL, 20, NULL),
(151, 13, 1, NULL, NULL, NULL, 3, NULL),
(152, 13, 2, NULL, NULL, NULL, 8, NULL),
(153, 13, 5, NULL, NULL, NULL, 13, NULL),
(154, 13, 6, NULL, NULL, NULL, 14, NULL),
(155, 13, 6, NULL, NULL, NULL, 15, NULL),
(156, 13, 6, NULL, NULL, NULL, 16, NULL),
(157, 13, 6, NULL, NULL, NULL, 17, NULL),
(158, 13, 7, NULL, NULL, NULL, 20, NULL),
(159, 14, 2, NULL, NULL, NULL, 5, NULL),
(160, 14, 6, NULL, NULL, NULL, 14, NULL),
(161, 14, 6, NULL, NULL, NULL, 16, NULL),
(162, 14, 7, NULL, NULL, NULL, 22, NULL),
(163, 15, 2, NULL, NULL, NULL, 9, NULL),
(164, 15, 5, NULL, NULL, NULL, 11, NULL),
(165, 15, 6, NULL, NULL, NULL, 14, NULL),
(166, 15, 6, NULL, NULL, NULL, 16, NULL),
(167, 15, 7, NULL, NULL, NULL, 20, NULL),
(168, 16, 1, NULL, NULL, NULL, 2, NULL),
(169, 16, 5, NULL, NULL, NULL, 11, NULL),
(170, 16, 5, NULL, NULL, NULL, 13, NULL),
(171, 16, 6, NULL, NULL, NULL, 14, NULL),
(172, 16, 6, NULL, NULL, NULL, 15, NULL),
(173, 16, 6, NULL, NULL, NULL, 16, NULL),
(174, 16, 6, NULL, NULL, NULL, 17, NULL),
(175, 16, 7, NULL, NULL, NULL, 20, NULL),
(176, 17, 2, NULL, NULL, NULL, 10, NULL),
(177, 17, 5, NULL, NULL, NULL, 13, NULL),
(178, 17, 6, NULL, NULL, NULL, 14, NULL),
(179, 17, 7, NULL, NULL, NULL, 20, NULL),
(180, 24, 2, NULL, NULL, NULL, 10, NULL),
(181, 24, 5, NULL, NULL, NULL, 11, NULL),
(182, 24, 6, NULL, NULL, NULL, 14, NULL),
(183, 24, 6, NULL, NULL, NULL, 15, NULL),
(184, 24, 6, NULL, NULL, NULL, 16, NULL),
(185, 24, 7, NULL, NULL, NULL, 20, NULL),
(186, 25, 2, NULL, NULL, NULL, 6, NULL),
(187, 25, 5, NULL, NULL, NULL, 11, NULL),
(188, 25, 5, NULL, NULL, NULL, 13, NULL),
(189, 25, 6, NULL, NULL, NULL, 14, NULL),
(190, 25, 7, NULL, NULL, NULL, 20, NULL),
(191, 27, 1, NULL, NULL, NULL, 3, NULL),
(192, 27, 5, NULL, NULL, NULL, 11, NULL),
(193, 27, 5, NULL, NULL, NULL, 13, NULL),
(194, 27, 6, NULL, NULL, NULL, 14, NULL),
(195, 27, 6, NULL, NULL, NULL, 16, NULL),
(196, 27, 7, NULL, NULL, NULL, 20, NULL),
(197, 28, 1, NULL, NULL, NULL, 3, NULL),
(198, 28, 2, NULL, NULL, NULL, 10, NULL),
(199, 28, 5, NULL, NULL, NULL, 11, NULL),
(200, 28, 6, NULL, NULL, NULL, 14, NULL),
(201, 28, 7, NULL, NULL, NULL, 20, NULL),
(202, 29, 1, NULL, NULL, NULL, 3, NULL),
(203, 29, 2, NULL, NULL, NULL, 7, NULL),
(204, 29, 5, NULL, NULL, NULL, 11, NULL),
(205, 29, 5, NULL, NULL, NULL, 13, NULL),
(206, 29, 6, NULL, NULL, NULL, 14, NULL),
(207, 29, 6, NULL, NULL, NULL, 16, NULL),
(208, 30, 1, NULL, NULL, NULL, 3, NULL),
(209, 30, 6, NULL, NULL, NULL, 14, NULL),
(210, 30, 6, NULL, NULL, NULL, 16, NULL),
(211, 30, 7, NULL, NULL, NULL, 20, NULL),
(212, 31, 1, NULL, NULL, NULL, 3, NULL),
(213, 31, 5, NULL, NULL, NULL, 11, NULL),
(214, 31, 6, NULL, NULL, NULL, 14, NULL),
(215, 31, 7, NULL, NULL, NULL, 20, NULL),
(218, 22, 9, 125, NULL, NULL, NULL, NULL),
(225, 9, 10, 65, NULL, NULL, 32, NULL),
(226, 9, 10, 33, NULL, NULL, NULL, NULL),
(227, 9, 10, 34, NULL, NULL, NULL, NULL),
(228, 9, 10, 35, NULL, NULL, NULL, NULL),
(229, 9, 10, 36, NULL, NULL, NULL, NULL),
(260, 18, 11, NULL, NULL, NULL, NULL, NULL),
(261, 18, 12, NULL, NULL, NULL, 48, NULL),
(262, 19, 11, NULL, NULL, NULL, NULL, NULL),
(263, 19, 12, NULL, NULL, NULL, 48, NULL),
(264, 20, 11, NULL, NULL, NULL, NULL, NULL),
(265, 20, 12, NULL, NULL, NULL, 48, NULL),
(266, 21, 11, NULL, NULL, NULL, NULL, NULL),
(267, 21, 12, NULL, NULL, NULL, 48, NULL),
(268, 23, 11, NULL, NULL, NULL, NULL, NULL),
(269, 23, 13, NULL, NULL, NULL, 56, NULL),
(276, 9, 11, NULL, NULL, NULL, NULL, NULL),
(326, 22, 11, NULL, NULL, NULL, NULL, NULL),
(327, 22, 16, NULL, NULL, NULL, NULL, NULL),
(329, 23, 15, NULL, NULL, NULL, 66, NULL),
(336, 9, 1, NULL, NULL, NULL, 3, NULL),
(337, 9, 2, NULL, NULL, NULL, 5, NULL),
(338, 9, 5, NULL, NULL, NULL, 11, NULL),
(339, 9, 6, NULL, NULL, NULL, 14, NULL),
(340, 9, 6, NULL, NULL, NULL, 17, NULL),
(341, 9, 7, NULL, NULL, NULL, 20, NULL),
(343, 22, 8, NULL, NULL, NULL, 29, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_product_category`
--

CREATE TABLE `mvc_store_product_category` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_product_image`
--

CREATE TABLE `mvc_store_product_image` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `option_color_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=180 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_product_image`
--

INSERT INTO `mvc_store_product_image` (`id`, `product_id`, `name`, `title`, `alt`, `group_id`, `option_color_id`) VALUES
(10, 9, '5e1ebb71b85b03741ffcf5ec66d5b81b.jpg', '', '', NULL, 37),
(11, 9, 'cf09c938db6d0d1b58923367f2c4ae58.jpg', '', '', NULL, 37),
(12, 9, 'b3763d3bcd508be452ca333e7bd78c8b.jpg', '', '', NULL, 37),
(13, 9, 'dda8fe491f0f5c19363253f10780a110.jpg', '', '', NULL, 37),
(14, 10, 'd7d4709d65012a150bf0c453af5f6ffd.jpg', '', '', NULL, NULL),
(15, 10, 'f97b551d7da9710896da29ba9767fe17.jpg', '', '', NULL, NULL),
(16, 10, 'd03d1fae945f84bff8af13b5cdd5479a.jpg', '', '', NULL, NULL),
(17, 10, '591a55f49adb4c4edfe0dee869e0c39c.jpg', '', '', NULL, NULL),
(18, 10, 'a141282f07c2ceb71d03842611d8056d.jpg', '', '', NULL, NULL),
(19, 11, '88d3ba0804ea8cdf1062002e40d1e756.jpg', '', '', NULL, NULL),
(20, 11, '46d18f9b3b0c610bb370184b416f94be.jpg', '', '', NULL, NULL),
(21, 11, 'b7a0c7b4a27cf1586b205c855870942b.jpg', '', '', NULL, NULL),
(22, 11, '2cede95f3d1131ab1a1b3ae4b8320a26.jpg', '', '', NULL, NULL),
(23, 11, '1a3985d9787d516305dbdc43a4f89dce.jpg', '', '', NULL, NULL),
(24, 12, 'ce2fe5cd0551e22df27282696af8b442.jpg', '', '', NULL, NULL),
(25, 12, '6c76f49b4cc6f301d8ddd08dcfc307a6.jpg', '', '', NULL, NULL),
(26, 12, '6f7b02bd1b6124aeb325421ee6fe7632.jpg', '', '', NULL, NULL),
(27, 12, '23ddb97632f72635bee6e0462af70e9c.jpg', '', '', NULL, NULL),
(28, 12, 'b55f963acebc0b999b0140581ce07df1.jpg', '', '', NULL, NULL),
(29, 13, '6fb37fef59ea3a0ce56dc4c31c9130c7.jpg', '', '', NULL, NULL),
(30, 13, '12db023aaf875071493b4cd6f86338f5.jpg', '', '', NULL, NULL),
(31, 13, '79658f20bdf0e1500e06f2ceb64728a6.jpg', '', '', NULL, NULL),
(32, 13, '9827a9da5fbad9f90314e2fc92afbdf2.jpg', '', '', NULL, NULL),
(33, 13, '590b4bc38065371c9e30bf7b865e2c21.jpg', '', '', NULL, NULL),
(34, 14, 'eaa771f072098ee9a58b599e9a4bbe12.jpg', '', '', NULL, NULL),
(35, 14, '17012efe800051ae4b08558ece6bd436.jpg', '', '', NULL, NULL),
(36, 14, 'f086ca4b53e4f1d4dec58ab631bac59e.jpg', '', '', NULL, NULL),
(37, 14, '7052e125918ad74f97d02f54337b85a0.jpg', '', '', NULL, NULL),
(38, 15, '841de177bf98755a4798b18691e5d625.jpg', '', '', NULL, NULL),
(39, 15, 'a60fc92bc0ed98cee42cd4d380b41909.jpg', '', '', NULL, NULL),
(40, 15, '51afdbcd2621733b73366db6fe2ceef3.jpg', '', '', NULL, NULL),
(41, 15, '6bc37291e50f6e195e3da4d90b647ff6.jpg', '', '', NULL, NULL),
(42, 15, 'cc5729c77f12c3c300531400983cde82.jpg', '', '', NULL, NULL),
(43, 16, '0507fe599204bbc117ad25a51ff38806.jpg', '', '', NULL, NULL),
(44, 16, 'f44705e9a84e899300ead4fa138321c2.jpg', '', '', NULL, NULL),
(45, 16, '1bf36a814bd7d7245bdac37068c1a426.jpg', '', '', NULL, NULL),
(46, 16, '59863cd6e9e1334a8b9fa23bef6b4b62.jpg', '', '', NULL, NULL),
(47, 17, 'f88ebfac4e115564fea63ac21ea9f1ec.jpg', '', '', NULL, NULL),
(48, 17, '8a417a4318926c94998c0c8f86613ffc.jpg', '', '', NULL, NULL),
(49, 17, 'ce18098ac7f57a60d12b5b6d373af158.jpg', '', '', NULL, NULL),
(50, 17, '16005c5c492b68b4a1a2f8b6b20377aa.jpg', '', '', NULL, NULL),
(51, 17, '8771c5e82160bf5ee216d7c5429b5139.jpg', '', '', NULL, NULL),
(52, 18, '2513587a558a3fee2eb8dd081703f768.jpg', '', '', NULL, NULL),
(53, 18, '53d81a682904d133a35a6333ade65302.jpg', '', '', NULL, NULL),
(54, 18, 'd7e9203c97bf758050ced0160c91e707.jpg', '', '', NULL, NULL),
(55, 18, '67df789be57708b60f80dbb8a87439f4.jpg', '', '', NULL, NULL),
(56, 19, '8f01fdbd9feecb65ae0a4864972ca18a.jpg', '', '', NULL, NULL),
(57, 19, '36649ef922d08aebeccd5d21101d33c5.jpg', '', '', NULL, NULL),
(58, 19, 'c2e09127963a0b5f9e074c73d9b178c1.jpg', '', '', NULL, NULL),
(59, 20, 'f4f79af17e2840d0f225031b63dca899.jpg', '', '', NULL, NULL),
(60, 20, 'ca80a3f99e033f8f656652f9488d5745.jpg', '', '', NULL, NULL),
(61, 21, 'c6e5cd0d1cbe8292a68817ab397eb5bc.jpg', '', '', NULL, NULL),
(62, 21, '997bf5d0ca8371e948cc3136dd22a67a.jpg', '', '', NULL, NULL),
(63, 21, '3155c07a623af71fe0da31b1f6d09b50.jpg', '', '', NULL, NULL),
(64, 21, 'a042aa651ec3f3a4e31a9c582aa6df2c.jpg', '', '', NULL, NULL),
(65, 21, 'fecf7a6c0548ca80b069a4f2b5bb34ed.jpg', '', '', NULL, NULL),
(66, 22, '6f4a6cd24152ea4ecb05e7d370769604.jpg', '', '', NULL, 37),
(67, 22, 'ddbf2113fcd69c5c9817d400d339681b.jpg', '', '', NULL, 38),
(68, 22, '87b38e0bb338d6585dc3663444b9780c.jpg', '', '', NULL, 40),
(69, 23, 'f6e66023af6d33016e859b6b94f502d1.jpg', '', '', NULL, NULL),
(70, 23, 'c64d8df58188a77e18d46634ab4de071.jpg', '', '', NULL, NULL),
(71, 23, '6fafccc71bd7bfddfa312588f1e9f0f6.jpg', '', '', NULL, NULL),
(72, 24, 'ff55036d8426beb2e62cdb1d572dd1f2.jpg', '', '', NULL, NULL),
(73, 24, '1b5c55f69ccc0a8471749f7177da1eec.jpg', '', '', NULL, NULL),
(74, 24, '0264945bd72c9da2d48156be7da73d67.jpg', '', '', NULL, NULL),
(75, 24, 'bacfebb5d3b67a06a8f34ea21bcbe292.jpg', '', '', NULL, NULL),
(76, 24, '77e60d10788ddc62e83e5716a60d146c.jpg', '', '', NULL, NULL),
(77, 25, '7d1c5a40522c95ec7b955417dfa14987.jpg', '', '', NULL, NULL),
(78, 25, 'e4c21e1977c9e292597c66414af5b6bd.jpg', '', '', NULL, NULL),
(79, 25, 'f9ce153b47f6813d65df540db3cf70d6.jpg', '', '', NULL, NULL),
(80, 26, 'bbefe18e4048c23245c0e82ae00beb47.jpg', '', '', NULL, NULL),
(81, 26, 'dafc2d5820f759d5164d6b2bbb9823b4.jpg', '', '', NULL, NULL),
(82, 26, '3df4f5d96cd24e2fa211be26b9247de8.jpg', '', '', NULL, NULL),
(83, 26, '7e33f4eed79fcd1116244172bd3c86c1.jpg', '', '', NULL, NULL),
(84, 26, '566afd3b52e40da5ae5a7b80f0770e60.jpg', '', '', NULL, NULL),
(85, 27, '9cd745b9ebe8eab79ef993ab5dda3753.jpg', '', '', NULL, NULL),
(86, 27, '3a63bb7e8248de32a68d334ef7394c21.jpg', '', '', NULL, NULL),
(87, 27, '75dd10e7d9c8fc4c271b9dfd39f124dc.jpg', '', '', NULL, NULL),
(88, 27, 'd04c98e036c5dcf17d1c884c02e47307.jpg', '', '', NULL, NULL),
(89, 27, '13ee82386e1ed1a928467488c46ddbe5.jpg', '', '', NULL, NULL),
(90, 28, '5115f72e29ffc6a7424a3faadbe7dcbe.jpg', '', '', NULL, NULL),
(91, 28, '11ca31a0bd9daf4811d9258bc23f44e4.jpg', '', '', NULL, NULL),
(92, 28, '36efb09d037fe1c7725963bbe81e6a39.jpg', '', '', NULL, NULL),
(93, 28, '755a17bdd13e1c317285169ff31433cd.jpg', '', '', NULL, NULL),
(94, 29, '3f8befde0c36df80fce78b438426b2bb.jpg', '', '', NULL, NULL),
(95, 29, '59100b9ceb3e4d77441794accb095061.jpg', '', '', NULL, NULL),
(96, 29, '0bb2075383c030f43ff85badfdcc42f3.jpg', '', '', NULL, NULL),
(97, 29, '74bb2b3db6a9c1df725156c9be37913d.jpg', '', '', NULL, NULL),
(98, 29, '1065e6f328498dfd587ac984fd3fe461.jpg', '', '', NULL, NULL),
(99, 30, 'd87414e34aced442eefa03e9d0168fe1.jpg', '', '', NULL, NULL),
(100, 30, '3513000ecc3d7b6f8ff73f55f2101b5c.jpg', '', '', NULL, NULL),
(101, 30, '58f94f32cd18b744e61b02d9f1592133.jpg', '', '', NULL, NULL),
(102, 31, 'e0dd845e3320edfb24fc218fad7949bc.jpg', '', '', NULL, NULL),
(103, 31, 'c6713bd9052849788e7504c2ffefa82d.jpg', '', '', NULL, NULL),
(104, 31, 'be3302f594a9f8f6336140db890f49d2.jpg', '', '', NULL, NULL),
(105, 31, '2a8688b64ef67ad1feaf7c045b02810b.jpg', '', '', NULL, NULL),
(106, 31, '598f4f3fafe9230d4d0ce4e91e0b2455.jpg', '', '', NULL, NULL),
(107, 22, NULL, '', '', NULL, NULL),
(108, 32, '870b7117a100971cfdd3a702644c0f5c.JPG', '', '', NULL, 37),
(109, 32, '65eea5b46a66b5cc3a004f9ce11eefcc.JPG', '', '', NULL, 38),
(110, 32, '3a857143d9e6913c8f13c5afe4666587.JPG', '', '', NULL, 39),
(111, 32, 'cbd4a791652b7e268b70521850917a7e.JPG', '', '', NULL, 40),
(112, 32, 'c09029ab6612c202e6d51bcbe1ca4d7b.JPG', '', '', NULL, 37),
(113, 32, 'cfb9d6680423e1618f2cc107a6e80ad3.JPG', '', '', NULL, 38);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_product_image_group`
--

CREATE TABLE `mvc_store_product_image_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_product_link`
--

CREATE TABLE `mvc_store_product_link` (
  `id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `linked_product_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_product_link`
--

INSERT INTO `mvc_store_product_link` (`id`, `type_id`, `product_id`, `linked_product_id`, `position`) VALUES
(1, 2, 9, 18, 1),
(2, 2, 9, 21, 2),
(3, 2, 9, 22, 3),
(4, 2, 10, 22, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_product_link_type`
--

CREATE TABLE `mvc_store_product_link_type` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_product_link_type`
--

INSERT INTO `mvc_store_product_link_type` (`id`, `code`, `title`) VALUES
(1, 'similar', 'Похожие'),
(2, 'related', 'Сопутствующие');

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_product_variant`
--

CREATE TABLE `mvc_store_product_variant` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `quantity` int(11) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_product_variant`
--

INSERT INTO `mvc_store_product_variant` (`id`, `product_id`, `attribute_id`, `attribute_value`, `amount`, `type`, `sku`, `position`, `quantity`, `option_id`, `parent_id`) VALUES
(6, 22, 16, '125', 100, 2, '', 6, NULL, 62, NULL),
(8, 23, 15, '100', 0, 0, '', 8, NULL, NULL, NULL),
(9, 18, 11, '82', 204, 2, '', 9, 2, NULL, NULL),
(10, 18, 11, '79', 80, 2, '', 10, 3, NULL, NULL),
(32, 9, 10, '104', 10.4, 3, '', 20, 1, 32, NULL),
(33, 9, 10, '108', 10.8, 3, '', 21, 2, 33, NULL),
(34, 9, 10, '116', 11.6, 3, '', 22, 2, 34, NULL),
(35, 9, 10, '122', 12.2, 3, '', 23, 2, 35, NULL),
(36, 9, 10, '126', 12.6, 3, '', 24, 2, 36, NULL),
(46, 10, 10, '104', 212, 3, '', 34, NULL, 32, NULL),
(47, 10, 11, '38', 323, 2, '', 35, NULL, NULL, NULL),
(49, 9, 10, '65', 1, 3, '', 36, 2, 73, NULL),
(50, 22, 11, '37', 460, 2, '', 37, NULL, NULL, NULL),
(51, 22, 11, '38', 460, 2, '', 38, NULL, NULL, NULL),
(52, 22, 11, '40', 460, 2, '', 39, NULL, NULL, NULL),
(53, 32, 11, '37', 100, 2, '', 40, NULL, NULL, NULL),
(54, 32, 11, '38', 500, 2, '', 41, NULL, NULL, NULL),
(55, 32, 11, '39', 200, 2, '', 42, NULL, NULL, NULL),
(56, 32, 11, '40', 460, 2, '', 43, NULL, NULL, NULL),
(57, 32, 9, '500', 0, 2, '', 44, NULL, NULL, NULL),
(58, 32, 16, '125', 1, 2, '', 45, NULL, 62, NULL),
(59, 9, 11, '37', 29, 2, '', 46, NULL, NULL, 32),
(60, 9, 11, '37', 29, 2, '', 47, NULL, NULL, 33),
(61, 9, 11, '37', 29, 2, '', 48, NULL, NULL, 34),
(62, 9, 11, '37', 29, 2, '', 49, NULL, NULL, 35),
(63, 9, 11, '37', 29, 2, '', 50, NULL, NULL, 36),
(64, 9, 11, '37', 29, 2, '', 51, NULL, NULL, 49);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_type`
--

CREATE TABLE `mvc_store_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_type`
--

INSERT INTO `mvc_store_type` (`id`, `name`) VALUES
(5, 'Кожа'),
(3, 'Нить'),
(2, 'Фурнитура'),
(4, 'Химия');

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_store_type_attribute`
--

CREATE TABLE `mvc_store_type_attribute` (
  `type_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=3276 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_store_type_attribute`
--

INSERT INTO `mvc_store_type_attribute` (`type_id`, `attribute_id`) VALUES
(5, 1),
(3, 2),
(5, 2),
(5, 5),
(5, 6),
(5, 7),
(3, 8),
(3, 9),
(5, 10),
(2, 11),
(3, 11),
(4, 11),
(5, 11),
(2, 12),
(4, 13),
(4, 15),
(3, 16);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_user_tokens`
--

CREATE TABLE `mvc_user_tokens` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `expire_time` datetime NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=16384 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_user_tokens`
--

INSERT INTO `mvc_user_tokens` (`id`, `user_id`, `token`, `type`, `status`, `create_time`, `update_time`, `ip`, `expire_time`) VALUES
(22, 4, '_6mu~hsaUzuH5Bya_e0Oa5zMDtM9EDQz', 1, 0, '2019-10-14 15:22:34', '2019-10-14 15:22:34', '145.255.22.58', '2019-10-15 15:22:34'),
(29, 5, '4Htwdpipo5jzTPV0CoOg~Koqd~1j7BXV', 1, 0, '2019-10-29 12:46:01', '2019-10-29 12:46:01', '145.255.22.98', '2019-10-30 12:46:01'),
(30, 5, 'NO6~HNeH_MF_WNiNFhSiZuIuQrCcHnf6', 4, 0, '2019-10-29 12:46:02', '2019-10-29 12:46:02', '145.255.22.98', '2019-11-05 12:46:02'),
(37, 6, 'j~EVUfUc5N4KO7DxxHX9k4kbnlLMo~ko', 1, 0, '2019-11-04 11:26:31', '2019-11-04 11:26:31', '94.41.185.13', '2019-11-05 11:26:31'),
(52, 7, 'Zz3Y8kXNv~6ENj6WjM6iJ5xou6Qy3btD', 1, 0, '2019-11-05 09:47:32', '2019-11-05 09:47:32', '94.41.185.13', '2019-11-06 09:47:32'),
(53, 7, '_B1sQNi8scFHRN7DmGGr~H8BAqDAgzSm', 4, 0, '2019-11-05 09:47:32', '2019-11-05 09:47:32', '94.41.185.13', '2019-11-12 09:47:32'),
(116, 6, '_L7gXupsX2qrnwdfXUpNdGLjkb00cWA5', 4, 0, '2020-02-17 09:35:40', '2020-02-17 09:35:40', '94.41.181.205', '2020-02-24 09:35:40'),
(118, 9, 'GYRHJgG4tk83wy1bIo55UcKRhZBBrtCg', 1, 0, '2020-02-17 11:52:56', '2020-02-17 11:52:56', '94.41.181.205', '2020-02-18 11:52:56'),
(119, 9, 'OeSp9Fhj2oDy~V0pzHJzctHkUMY6TXVB', 4, 0, '2020-02-17 11:52:56', '2020-02-17 11:52:56', '94.41.181.205', '2020-02-24 11:52:56'),
(120, 10, 'MjN6t1yK9g5X9nQPDvSxJXbpCSRUpRZr', 1, 0, '2020-02-17 11:53:42', '2020-02-17 11:53:42', '94.41.181.205', '2020-02-18 11:53:42'),
(121, 10, 'MH~RUhsDt6yVYnOs7eKCIUK3i9Zuh9r0', 4, 0, '2020-02-17 11:53:42', '2020-02-17 11:53:42', '94.41.181.205', '2020-02-24 11:53:42'),
(122, 11, 'RMfziRXoHsnzxWrTuYwVS7bZKc0UqYTf', 1, 0, '2020-02-17 12:11:41', '2020-02-17 12:11:41', '94.41.181.205', '2020-02-18 12:11:41'),
(123, 11, 'f4PdFmRFqBld2yrouWfPiT2IEAnlmk3c', 4, 0, '2020-02-17 12:11:41', '2020-02-17 12:11:41', '94.41.181.205', '2020-02-24 12:11:41'),
(124, 12, '1m4p64w~cVBG~vSmI~psEdRXjJ7Pn8Ba', 1, 0, '2020-02-17 12:14:48', '2020-02-17 12:14:48', '94.41.181.205', '2020-02-18 12:14:48'),
(125, 12, '_WyP54fOfA24ITMtRq_G35pwli84BCaO', 4, 0, '2020-02-17 12:14:48', '2020-02-17 12:14:48', '94.41.181.205', '2020-02-24 12:14:48'),
(126, 13, 'h~5de30Y~eRz5xgXPrmQ6iMgJqvKrSmn', 1, 0, '2020-02-17 14:25:22', '2020-02-17 14:25:22', '94.41.181.205', '2020-02-18 14:25:22'),
(127, 13, 'AMvU2s9O0YpnB_T7KynyCG8Nu8NQD9vK', 4, 0, '2020-02-17 14:25:23', '2020-02-17 14:25:23', '94.41.181.205', '2020-02-24 14:25:23'),
(128, 14, 'AUFiscT3z_2b9RcX~muQ9Ts6Txwm1K6n', 1, 0, '2020-02-17 14:36:31', '2020-02-17 14:36:31', '94.41.181.205', '2020-02-18 14:36:31'),
(129, 14, 'jC0UuK8ukhanEnHFAbMn3dbHDZEj8yDH', 4, 0, '2020-02-17 14:36:31', '2020-02-17 14:36:31', '94.41.181.205', '2020-02-24 14:36:31'),
(131, 15, 'REFOBjI0U3TonxvS9xAl4zL6FhjPyuS5', 1, 0, '2020-02-17 14:37:34', '2020-02-17 14:37:34', '94.41.181.205', '2020-02-18 14:37:34'),
(132, 15, 'IHEbVkuV7mKw0456Dgb5tvpD_peDhefx', 4, 0, '2020-02-17 14:37:35', '2020-02-17 14:37:35', '94.41.181.205', '2020-02-24 14:37:35'),
(134, 16, 'snGgeHkI5c69b7fj15kWwa~GUkdTIF0N', 1, 0, '2020-02-17 14:39:18', '2020-02-17 14:39:18', '94.41.181.205', '2020-02-18 14:39:18'),
(135, 16, '3~dmFQ17v~KIV_OZyJLRaYH_4vB3hkzY', 4, 0, '2020-02-17 14:39:19', '2020-02-17 14:39:19', '94.41.181.205', '2020-02-24 14:39:19'),
(137, 17, 'Iu84B4YB9XYWESEWqjxUoRryC7BteI~d', 1, 0, '2020-02-17 14:41:35', '2020-02-17 14:41:35', '94.41.181.205', '2020-02-18 14:41:35'),
(138, 17, '7dPaJbWFnl0SQZ_DjjswnMn2tSv3fZFJ', 4, 0, '2020-02-17 14:41:35', '2020-02-17 14:41:35', '94.41.181.205', '2020-02-24 14:41:35'),
(141, 18, 'RjZCGhrOzs0IgmoLlmRFPeuZC6GPFd4Q', 1, 0, '2020-03-03 08:50:27', '2020-03-03 08:50:27', '94.41.170.128', '2020-03-04 08:50:27'),
(145, 18, 'NUgQL~8WwsrFI86tadlHsz4rpIIImt9c', 4, 0, '2020-03-03 09:19:46', '2020-03-03 09:19:46', '94.41.170.128', '2020-03-10 09:19:46'),
(153, 19, 'H4ONMIvD3H9MuMVpcGXUU_XrhKgwUuoz', 1, 0, '2020-03-05 11:32:44', '2020-03-05 11:32:44', '94.41.140.93', '2020-03-06 11:32:44'),
(154, 19, 'zdBe_HAW4n8SPCeJi1SIWwBMt4Cp2dYz', 4, 0, '2020-03-05 11:32:45', '2020-03-05 11:32:45', '94.41.140.93', '2020-03-12 11:32:45'),
(155, 20, 'zi7OwA~o_a99lWpasZiA9fi7ohLgm5qi', 1, 0, '2020-03-05 11:39:04', '2020-03-05 11:39:04', '94.41.140.93', '2020-03-06 11:39:04'),
(156, 20, '8XwswRZGi3Oe0l5bohy6np8ouscYdqPb', 4, 0, '2020-03-05 11:39:05', '2020-03-05 11:39:05', '94.41.140.93', '2020-03-12 11:39:05'),
(159, 21, 'hWlppzXa5RrCR50VGxArX9U8KcX6aClT', 1, 0, '2020-03-05 12:27:50', '2020-03-05 12:27:50', '94.41.140.93', '2020-03-06 12:27:50'),
(160, 21, '7Wvk8HXDpl_754La9jhF8R6r~F0oVips', 4, 0, '2020-03-05 12:27:51', '2020-03-05 12:27:51', '94.41.140.93', '2020-03-12 12:27:51'),
(161, 22, 'UKSUeRBuspx44PuaMqwC_EU7KcDv5h~v', 1, 0, '2020-03-05 15:00:40', '2020-03-05 15:00:40', '94.41.140.93', '2020-03-06 15:00:40'),
(162, 22, 'RyUjiaeo~g2yTzJvWjQxJeNpMHEsNFwA', 4, 0, '2020-03-05 15:00:40', '2020-03-05 15:00:40', '94.41.140.93', '2020-03-12 15:00:40'),
(164, 23, 'sGCdhyVftAO5q440447OYyxooZS7qi7Z', 1, 0, '2020-03-05 16:50:05', '2020-03-05 16:50:05', '94.41.140.93', '2020-03-06 16:50:05'),
(165, 23, 'sGo28tvNzx2_9i8pbmK8pf5U58716aXr', 4, 0, '2020-03-05 16:50:06', '2020-03-05 16:50:06', '94.41.140.93', '2020-03-12 16:50:06'),
(171, 4, '_yT_RjovtS5QlC9ZojVp3GE3fooI9yMz', 4, 0, '2020-03-14 23:40:06', '2020-03-14 23:40:06', '94.41.140.93', '2020-03-21 23:40:06');

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_user_user`
--

CREATE TABLE `mvc_user_user` (
  `id` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `nick_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `birth_date` date DEFAULT NULL,
  `site` varchar(250) NOT NULL DEFAULT '',
  `about` varchar(250) NOT NULL DEFAULT '',
  `location` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '2',
  `access_level` int(11) NOT NULL DEFAULT '0',
  `visit_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `avatar` varchar(150) DEFAULT NULL,
  `hash` varchar(255) NOT NULL DEFAULT '180303fe4fbde10e947fb7782185f23a0.02464100 1569909505',
  `email_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `phone` char(30) DEFAULT NULL,
  `is_admin` int(1) DEFAULT '0',
  `method_delivery_id` int(1) DEFAULT NULL,
  `address_delivery` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_user_user`
--

INSERT INTO `mvc_user_user` (`id`, `update_time`, `first_name`, `middle_name`, `last_name`, `nick_name`, `email`, `gender`, `birth_date`, `site`, `about`, `location`, `status`, `access_level`, `visit_time`, `create_time`, `avatar`, `hash`, `email_confirm`, `phone`, `is_admin`, `method_delivery_id`, `address_delivery`) VALUES
(4, '2020-03-03 09:29:54', 'Сайта', 'Сего', 'Администратор', 'admin', 'dekanrector@mail.ru', 0, NULL, '', '', 'Тестовый адрес доставки', 1, 1, '2020-03-14 23:40:06', '2019-10-14 15:22:34', '4_1583209794.jpg', '$2y$13$iNoz0/ZbBeFgl.TMeYHB1uHtB3Ki0vTtlWMuKHmsikyOe6e9wkeUe', 1, '+7-234-234-2342', 1, 2, 'Оренбург, Восточная 42'),
(5, '2019-10-29 12:46:01', '', '', '', 'Mops56', 'kesha56rus@yandex.ru', 0, NULL, '', '', '', 1, 1, '2019-10-29 12:46:02', '2019-10-29 12:46:01', NULL, '$2y$13$so03t3SdY70Hhmsh5YpNW.4GKobUXX/kQrTFDjCqpC0fBCaanmm86', 1, NULL, 0, NULL, NULL),
(6, '2019-11-05 16:59:18', 'тест', 'тестович', 'тестовый', 'test', 'doctordrethe@bk.ru', 0, NULL, '', '', '', 1, 1, '2020-02-17 09:35:40', '2019-11-04 11:26:31', '6_1572943963.jpg', '$2y$13$oZfbPx7VtnLaHTF4LTL/4..cmNFwhwUvA3Ura4tWBb7xEFII/j2NC', 1, '', 0, NULL, NULL),
(7, '2019-11-05 09:47:32', '', '', '', 'maksim', 'maks100491@yandex.ru', 0, NULL, '', '', '', 1, 1, '2019-11-05 09:47:32', '2019-11-05 09:47:32', NULL, '$2y$13$gosdxZCgLQi3xl4XZlT7muKzNJAFtImXtIssn.oasLuGfKklknu76', 1, NULL, 0, NULL, NULL),
(9, '2020-02-17 11:52:56', '', '', '', 'user1', 'user1@site.ru', 0, NULL, '', '', '', 1, 1, '2020-02-17 11:52:56', '2020-02-17 11:52:56', NULL, '$2y$13$U0ln35dsKX3FWm0/EcjuROCIFYqc04gWNghMHax1vuowY68zfCh4i', 1, NULL, 0, NULL, NULL),
(10, '2020-02-17 11:53:42', '', '', '', 'user1581922422', 'user2@site.ru', 0, NULL, '', '', '', 1, 1, '2020-02-17 11:53:42', '2020-02-17 11:53:42', NULL, '$2y$13$2t/.vujCxVLF3MJppMKuqO.vzsQ/BScro5sD.I55LD6Qu3EDuzQwK', 1, NULL, 0, NULL, NULL),
(11, '2020-02-17 12:11:41', '', '', '', 'user1581923501', 'ewkrrf@gmail.com', 0, NULL, '', '', '', 1, 1, '2020-02-17 12:11:41', '2020-02-17 12:11:41', NULL, '$2y$13$94la1FAQk7GweMdfoA6wUOkm8rO2rdyPlzjaSDsOmc1gN0cpVAOpO', 1, NULL, 0, NULL, NULL),
(12, '2020-02-17 12:14:48', '', '', '', 'user1581923688', 'dadada@gmail.com', 0, NULL, '', '', '', 1, 1, '2020-02-17 12:14:48', '2020-02-17 12:14:48', NULL, '$2y$13$ZlYUAL9keQO2Zf5oSqVDgu9JRDjK0yYa3v5px/G3QIKJEf6CLUNxK', 1, NULL, 0, NULL, NULL),
(13, '2020-02-17 14:25:22', '', '', '', 'user1581931522', 'test1@test.ru', 0, NULL, '', '', '', 1, 1, '2020-02-17 14:25:23', '2020-02-17 14:25:22', NULL, '$2y$13$XM4QE/id9cwzHOcgAxBije0vCD9Qk0npT8GZA3ulezGTG9G2PVXnO', 1, NULL, 0, NULL, NULL),
(14, '2020-02-17 14:36:31', '', '', '', 'user1581932190', 'test2@test.test', 0, NULL, '', '', '', 1, 1, '2020-02-17 14:36:31', '2020-02-17 14:36:31', NULL, '$2y$13$c91YtFf4KW/vEI.b6gJW4eYweu.kiUMhSif4dhcGHFq13tmpVtFAu', 1, NULL, 0, NULL, NULL),
(15, '2020-02-17 14:37:34', '', '', '', 'user1581932254', 'test3@test.test', 0, NULL, '', '', '', 1, 1, '2020-02-17 14:37:35', '2020-02-17 14:37:34', NULL, '$2y$13$b4NRdBER7J6F0E2FPa74OeUf4MeYxuL9DzyfUKC1TIWp1ruTKXEb.', 1, NULL, 0, NULL, NULL),
(16, '2020-02-17 14:39:18', '', '', '', 'user1581932358', 'test4@test.test', 0, NULL, '', '', '', 1, 1, '2020-02-17 14:39:19', '2020-02-17 14:39:18', NULL, '$2y$13$C.ul7M4JAXMKp9V2JAzIMu6S1Z5ga8gJBb8947K3zvnndFQ3symQe', 1, NULL, 0, NULL, NULL),
(17, '2020-02-17 14:41:35', '', '', '', 'user1581932494', 'test5@test.test', 0, NULL, '', '', '', 1, 1, '2020-02-17 14:41:35', '2020-02-17 14:41:35', NULL, '$2y$13$9xTWrl1DuWgqpHftsdYHgu5RnwM3KjFgGtwRA1/cGAMA8wVYj7IBS', 1, NULL, 0, NULL, NULL),
(18, '2020-03-03 09:25:18', 'Стас', 'Александрович', 'Мразовский', 'user1583207427', 'n_etu@mail.ru', 0, NULL, '', '', '', 1, 1, '2020-03-03 09:19:46', '2020-03-03 08:50:27', '18_1583209518.jpg', '$2y$13$dHSLGOdVZsm11BgFxUAE4OtNtteLhLWecCxihJ4.Qd54s.QxpX2re', 1, '+7-961-938-7666', 0, 2, 'Восточная 42'),
(19, '2020-03-05 11:32:44', '', '', '', 'user1583389964', 'test@tets.test', 0, NULL, '', '', '', 1, 1, '2020-03-05 11:32:45', '2020-03-05 11:32:44', NULL, '$2y$13$veAzzNIMwgf.seNgrbXFeerAWhwyLd4909nlOcUkHvkK62/aJ85wW', 1, NULL, 0, NULL, NULL),
(20, '2020-03-05 11:39:04', '', '', '', 'user1583390344', 'test@tets.test2', 0, NULL, '', '', '', 1, 1, '2020-03-05 11:39:05', '2020-03-05 11:39:04', NULL, '$2y$13$FOTV/tQaIAnRPDzCJ98EV.1.4cJQZbhZaxxhqyuNJwV4yxK3h32ce', 1, NULL, 0, NULL, NULL),
(21, '2020-03-05 12:27:50', '', '', '', 'user1583393270', '32131@dasdasd.dsd', 0, NULL, '', '', '', 1, 1, '2020-03-05 12:27:51', '2020-03-05 12:27:50', NULL, '$2y$13$YXb1nHCplWlf.IQtmUz/7eAUf83KPjkQw4bnZ5aEuvdqcYRPU0B3K', 1, NULL, 0, NULL, NULL),
(22, '2020-03-05 15:00:40', '', '', '', 'user1583402439', '3213123123@dsadv.ew', 0, NULL, '', '', '', 1, 1, '2020-03-05 15:00:40', '2020-03-05 15:00:40', NULL, '$2y$13$xdMICmbFRRVipYgc.zBlE.yBkdWRRjBQ5nR9QCvA/NWPJ9mgjy38m', 1, NULL, 0, NULL, NULL),
(23, '2020-03-05 16:50:05', 'eqweqw', 'ewqeqw', 'eqwewq', 'user1583409005', 'eewqew@dasdsa.ewqe', 0, NULL, '', '', '', 1, 1, '2020-03-05 16:50:06', '2020-03-05 16:50:05', NULL, '$2y$13$3Ns/6u2JCBy45hPfNxoLkeUOiK9aQEFvqo3c7nP2HkGw7NwX3idb.', 1, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_user_user_auth_assignment`
--

CREATE TABLE `mvc_user_user_auth_assignment` (
  `itemname` char(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_user_user_auth_assignment`
--

INSERT INTO `mvc_user_user_auth_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', 4, NULL, NULL),
('Gallery.GalleryBackend.Addimages', 6, NULL, NULL),
('Gallery.GalleryBackend.Create', 6, NULL, NULL),
('Gallery.GalleryBackend.Delete', 6, NULL, NULL),
('Gallery.GalleryBackend.DeleteImage', 6, NULL, NULL),
('Gallery.GalleryBackend.Images', 6, NULL, NULL),
('Gallery.GalleryBackend.Index', 6, NULL, NULL),
('Gallery.GalleryBackend.Update', 6, NULL, NULL),
('Gallery.GalleryBackend.View', 6, NULL, NULL),
('Gallery.GalleryManager', 6, NULL, NULL),
('Image.ImageBackend.Create', 6, NULL, NULL),
('Image.ImageBackend.Delete', 6, NULL, NULL),
('Image.ImageBackend.Index', 6, NULL, NULL),
('Image.ImageBackend.Update', 6, NULL, NULL),
('Image.ImageBackend.View', 6, NULL, NULL),
('Image.ImageManager', 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_user_user_auth_item`
--

CREATE TABLE `mvc_user_user_auth_item` (
  `name` char(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_user_user_auth_item`
--

INSERT INTO `mvc_user_user_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'Admin', NULL, NULL),
('Gallery.GalleryBackend.Addimages', 0, 'Добавить изображение', NULL, NULL),
('Gallery.GalleryBackend.Create', 0, 'Создание галерей', NULL, NULL),
('Gallery.GalleryBackend.Delete', 0, 'Удаление галерей', NULL, NULL),
('Gallery.GalleryBackend.DeleteImage', 0, 'Удаление изображения галереи', NULL, NULL),
('Gallery.GalleryBackend.Images', 0, 'Просмотр списка изображений галерей', NULL, NULL),
('Gallery.GalleryBackend.Index', 0, 'Просмотр списка галерей', NULL, NULL),
('Gallery.GalleryBackend.Update', 0, 'Редактирование галерей', NULL, NULL),
('Gallery.GalleryBackend.View', 0, 'Просмотр галерей', NULL, NULL),
('Gallery.GalleryManager', 1, 'Управление галереями', NULL, NULL),
('Image.ImageBackend.Create', 0, 'Создание изображений', NULL, NULL),
('Image.ImageBackend.Delete', 0, 'Удаление изображений', NULL, NULL),
('Image.ImageBackend.Index', 0, 'Просмотр списка изображений', NULL, NULL),
('Image.ImageBackend.Update', 0, 'Редактирование изображений', NULL, NULL),
('Image.ImageBackend.View', 0, 'Просмотр изображений', NULL, NULL),
('Image.ImageManager', 1, 'Управление изображениями', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_user_user_auth_item_child`
--

CREATE TABLE `mvc_user_user_auth_item_child` (
  `parent` char(64) NOT NULL,
  `child` char(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_user_user_auth_item_child`
--

INSERT INTO `mvc_user_user_auth_item_child` (`parent`, `child`) VALUES
('Gallery.GalleryManager', 'Gallery.GalleryBackend.Addimages'),
('Gallery.GalleryManager', 'Gallery.GalleryBackend.Create'),
('Gallery.GalleryManager', 'Gallery.GalleryBackend.Delete'),
('Gallery.GalleryManager', 'Gallery.GalleryBackend.DeleteImage'),
('Gallery.GalleryManager', 'Gallery.GalleryBackend.Images'),
('Gallery.GalleryManager', 'Gallery.GalleryBackend.Index'),
('Gallery.GalleryManager', 'Gallery.GalleryBackend.Update'),
('Gallery.GalleryManager', 'Gallery.GalleryBackend.View'),
('Image.ImageManager', 'Image.ImageBackend.Create'),
('Image.ImageManager', 'Image.ImageBackend.Delete'),
('Image.ImageManager', 'Image.ImageBackend.Index'),
('Image.ImageManager', 'Image.ImageBackend.Update'),
('Image.ImageManager', 'Image.ImageBackend.View');

-- --------------------------------------------------------

--
-- Структура таблицы `mvc_yupe_settings`
--

CREATE TABLE `mvc_yupe_settings` (
  `id` int(11) NOT NULL,
  `module_id` varchar(100) NOT NULL,
  `param_name` varchar(100) NOT NULL,
  `param_value` varchar(500) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AVG_ROW_LENGTH=862 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mvc_yupe_settings`
--

INSERT INTO `mvc_yupe_settings` (`id`, `module_id`, `param_name`, `param_value`, `create_time`, `update_time`, `user_id`, `type`) VALUES
(1, 'yupe', 'siteDescription', 'Мы занимаемся продажей натуральной итальянской кожи крс, овчины, принты, замша, велюр. Большой выбор цветовой гаммы, эксклюзивные шкуры - с выделкой в стиле винтаж - крейзи хорс, кожи растительного дубления. Толщина кожи различная, от 0,6 мм до 4 мм. Мы продаем шкуры, полшкуры, лоскуты. Ежемесячное поступление новинок. Цены очень демократичные.', '2019-10-01 11:05:23', '2019-10-01 13:50:17', NULL, 1),
(2, 'yupe', 'siteName', 'Prima Pelle', '2019-10-01 11:05:23', '2019-10-01 13:50:17', NULL, 1),
(3, 'yupe', 'siteKeyWords', 'кожа, замша, фурнитура для кожи', '2019-10-01 11:05:23', '2019-10-01 13:50:17', NULL, 1),
(4, 'yupe', 'email', 'admin@admin.ru', '2019-10-01 11:05:23', '2019-10-01 11:05:23', NULL, 1),
(5, 'yupe', 'theme', 'skin', '2019-10-01 11:05:23', '2019-10-01 11:05:23', NULL, 1),
(6, 'yupe', 'backendTheme', '', '2019-10-01 11:05:23', '2019-10-01 11:05:23', NULL, 1),
(7, 'yupe', 'defaultLanguage', 'ru', '2019-10-01 11:05:23', '2019-10-01 11:05:23', NULL, 1),
(8, 'yupe', 'defaultBackendLanguage', 'ru', '2019-10-01 11:05:23', '2019-10-01 11:05:23', NULL, 1),
(9, 'yupe', 'coreCacheTime', '3600', '2019-10-01 13:50:17', '2019-10-01 13:50:17', NULL, 1),
(10, 'yupe', 'uploadPath', 'uploads', '2019-10-01 13:50:17', '2019-10-01 13:50:17', NULL, 1),
(11, 'yupe', 'editor', 'redactor', '2019-10-01 13:50:17', '2019-10-01 13:50:17', NULL, 1),
(12, 'yupe', 'availableLanguages', 'ru,uk,en,zh', '2019-10-01 13:50:17', '2019-10-01 13:50:17', NULL, 1),
(13, 'yupe', 'allowedIp', '', '2019-10-01 13:50:17', '2019-10-01 13:50:17', NULL, 1),
(14, 'yupe', 'hidePanelUrls', '0', '2019-10-01 13:50:17', '2019-10-01 13:50:17', NULL, 1),
(15, 'yupe', 'logo', 'images/logo.png', '2019-10-01 13:50:17', '2019-10-01 13:50:17', NULL, 1),
(16, 'yupe', 'allowedExtensions', 'gif, jpeg, png, jpg, zip, rar, doc, docx, xls, xlsx, pdf', '2019-10-01 13:50:17', '2019-10-01 13:50:17', NULL, 1),
(17, 'yupe', 'mimeTypes', 'image/gif,image/jpeg,image/png,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/zip,application/x-rar,application/x-rar-compressed, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '2019-10-01 13:50:17', '2019-10-01 13:50:17', NULL, 1),
(18, 'yupe', 'maxSize', '5242880', '2019-10-01 13:50:17', '2019-10-01 13:50:17', NULL, 1),
(19, 'yupe', 'defaultImage', '/images/nophoto.jpg', '2019-10-01 13:50:17', '2019-10-01 13:50:17', NULL, 1),
(20, 'homepage', 'mode', '2', '2019-10-01 14:05:20', '2019-10-01 14:05:20', NULL, 1),
(21, 'homepage', 'target', '7', '2019-10-01 14:05:20', '2019-10-01 14:05:28', NULL, 1),
(22, 'homepage', 'limit', '', '2019-10-01 14:05:20', '2019-10-01 14:05:20', NULL, 1),
(23, 'product', 'pageSize', '100', '2019-10-04 17:02:05', '2019-10-04 17:02:05', NULL, 2),
(24, 'user', 'avatarMaxSize', '5242880', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(25, 'user', 'avatarExtensions', 'jpg,png,gif,jpeg', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(26, 'user', 'defaultAvatarPath', 'images/avatar.png', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(27, 'user', 'avatarsDir', 'avatars', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(28, 'user', 'showCaptcha', '0', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(29, 'user', 'minCaptchaLength', '3', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(30, 'user', 'maxCaptchaLength', '6', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(31, 'user', 'minPasswordLength', '8', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(32, 'user', 'autoRecoveryPassword', '0', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(33, 'user', 'recoveryDisabled', '0', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(34, 'user', 'registrationDisabled', '0', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(35, 'user', 'notifyEmailFrom', 'test@test.ru', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(36, 'user', 'logoutSuccess', '/', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(37, 'user', 'loginSuccess', 'user/profile/profile', '2019-10-14 15:19:11', '2019-11-05 12:08:33', 4, 1),
(38, 'user', 'accountActivationSuccess', 'user/profile/profile', '2019-10-14 15:19:11', '2019-11-05 12:08:33', 4, 1),
(39, 'user', 'accountActivationFailure', '/user/account/registration', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(40, 'user', 'loginAdminSuccess', '/yupe/backend/index', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(41, 'user', 'registrationSuccess', 'user/profile/profile', '2019-10-14 15:19:11', '2019-11-05 12:08:33', 4, 1),
(42, 'user', 'sessionLifeTime', '7', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(43, 'user', 'usersPerPage', '20', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(44, 'user', 'emailAccountVerification', '0', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(45, 'user', 'badLoginCount', '3', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(46, 'user', 'phoneMask', '+7-999-999-9999', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(47, 'user', 'phonePattern', '/^((\\+?7)(-?\\d{3})-?)?(\\d{3})(-?\\d{4})$/', '2019-10-14 15:19:11', '2019-10-14 15:19:11', NULL, 1),
(48, 'user', 'generateNickName', '1', '2019-10-14 15:19:11', '2020-02-17 11:53:19', 4, 1),
(49, 'chat', 'host', 'ws://skin.dcmr.ru', '2019-11-05 00:29:58', '2019-11-05 00:29:58', 4, 1),
(50, 'chat', 'port', '8085', '2019-11-05 00:29:58', '2019-11-05 00:29:58', 4, 1),
(51, 'chat', 'adminId', '4', '2019-11-05 00:29:58', '2019-11-05 00:29:58', 4, 1),
(52, 'image', 'pageSize', '5', '2020-03-03 09:49:53', '2020-03-03 09:49:53', 4, 2),
(53, 'page', 'pageSize', '20', '2020-03-03 09:59:50', '2020-03-03 09:59:50', 4, 2),
(54, 'product', 'pageSize', '100', '2020-03-03 10:28:59', '2020-03-03 10:28:59', 4, 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `mvc_blog_blog`
--
ALTER TABLE `mvc_blog_blog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_blog_blog_slug_lang` (`slug`,`lang`),
  ADD KEY `ix_mvc_blog_blog_category_id` (`category_id`),
  ADD KEY `ix_mvc_blog_blog_create_date` (`create_time`),
  ADD KEY `ix_mvc_blog_blog_create_user` (`create_user_id`),
  ADD KEY `ix_mvc_blog_blog_lang` (`lang`),
  ADD KEY `ix_mvc_blog_blog_slug` (`slug`),
  ADD KEY `ix_mvc_blog_blog_status` (`status`),
  ADD KEY `ix_mvc_blog_blog_type` (`type`),
  ADD KEY `ix_mvc_blog_blog_update_date` (`update_time`),
  ADD KEY `ix_mvc_blog_blog_update_user` (`update_user_id`);

--
-- Индексы таблицы `mvc_blog_post`
--
ALTER TABLE `mvc_blog_post`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_blog_post_lang_slug` (`slug`,`lang`),
  ADD KEY `ix_mvc_blog_post_access_type` (`access_type`),
  ADD KEY `ix_mvc_blog_post_blog_id` (`blog_id`),
  ADD KEY `ix_mvc_blog_post_category_id` (`category_id`),
  ADD KEY `ix_mvc_blog_post_comment_status` (`comment_status`),
  ADD KEY `ix_mvc_blog_post_create_user_id` (`create_user_id`),
  ADD KEY `ix_mvc_blog_post_lang` (`lang`),
  ADD KEY `ix_mvc_blog_post_publish_date` (`publish_time`),
  ADD KEY `ix_mvc_blog_post_slug` (`slug`),
  ADD KEY `ix_mvc_blog_post_status` (`status`),
  ADD KEY `ix_mvc_blog_post_update_user_id` (`update_user_id`);

--
-- Индексы таблицы `mvc_blog_post_to_tag`
--
ALTER TABLE `mvc_blog_post_to_tag`
  ADD PRIMARY KEY (`post_id`,`tag_id`),
  ADD KEY `ix_mvc_blog_post_to_tag_post_id` (`post_id`),
  ADD KEY `ix_mvc_blog_post_to_tag_tag_id` (`tag_id`);

--
-- Индексы таблицы `mvc_blog_tag`
--
ALTER TABLE `mvc_blog_tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_blog_tag_tag_name` (`name`);

--
-- Индексы таблицы `mvc_blog_user_to_blog`
--
ALTER TABLE `mvc_blog_user_to_blog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_blog_user_to_blog_blog_user_to_blog_u_b` (`user_id`,`blog_id`),
  ADD KEY `ix_mvc_blog_user_to_blog_blog_user_to_blog_id` (`blog_id`),
  ADD KEY `ix_mvc_blog_user_to_blog_blog_user_to_blog_role` (`role`),
  ADD KEY `ix_mvc_blog_user_to_blog_blog_user_to_blog_status` (`status`),
  ADD KEY `ix_mvc_blog_user_to_blog_blog_user_to_blog_user_id` (`user_id`);

--
-- Индексы таблицы `mvc_category_category`
--
ALTER TABLE `mvc_category_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_category_category_alias_lang` (`slug`,`lang`),
  ADD KEY `ix_mvc_category_category_parent_id` (`parent_id`),
  ADD KEY `ix_mvc_category_category_status` (`status`);

--
-- Индексы таблицы `mvc_chat_chat`
--
ALTER TABLE `mvc_chat_chat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_mvc_chat_chat_user_from` (`user_from`),
  ADD KEY `ix_mvc_chat_chat_user_to` (`user_to`);

--
-- Индексы таблицы `mvc_comment_comment`
--
ALTER TABLE `mvc_comment_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_mvc_comment_comment_level` (`level`),
  ADD KEY `ix_mvc_comment_comment_lft` (`lft`),
  ADD KEY `ix_mvc_comment_comment_model` (`model`),
  ADD KEY `ix_mvc_comment_comment_model_id` (`model_id`),
  ADD KEY `ix_mvc_comment_comment_model_model_id` (`model`,`model_id`),
  ADD KEY `ix_mvc_comment_comment_parent_id` (`parent_id`),
  ADD KEY `ix_mvc_comment_comment_rgt` (`rgt`),
  ADD KEY `ix_mvc_comment_comment_root` (`root`),
  ADD KEY `ix_mvc_comment_comment_status` (`status`),
  ADD KEY `ix_mvc_comment_comment_user_id` (`user_id`);

--
-- Индексы таблицы `mvc_contentblock_content_block`
--
ALTER TABLE `mvc_contentblock_content_block`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_contentblock_content_block_code` (`code`),
  ADD KEY `ix_mvc_contentblock_content_block_status` (`status`),
  ADD KEY `ix_mvc_contentblock_content_block_type` (`type`);

--
-- Индексы таблицы `mvc_dictionary_dictionary_data`
--
ALTER TABLE `mvc_dictionary_dictionary_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_dictionary_dictionary_data_code_unique` (`code`),
  ADD KEY `ix_mvc_dictionary_dictionary_data_group_id` (`group_id`),
  ADD KEY `ix_mvc_dictionary_dictionary_data_create_user_id` (`create_user_id`),
  ADD KEY `ix_mvc_dictionary_dictionary_data_update_user_id` (`update_user_id`),
  ADD KEY `ix_mvc_dictionary_dictionary_data_status` (`status`);

--
-- Индексы таблицы `mvc_dictionary_dictionary_group`
--
ALTER TABLE `mvc_dictionary_dictionary_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_dictionary_dictionary_group_code` (`code`),
  ADD KEY `ix_mvc_dictionary_dictionary_group_create_user_id` (`create_user_id`),
  ADD KEY `ix_mvc_dictionary_dictionary_group_update_user_id` (`update_user_id`);

--
-- Индексы таблицы `mvc_feedback_feedback`
--
ALTER TABLE `mvc_feedback_feedback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_mvc_feedback_feedback_answer_user` (`answer_user`),
  ADD KEY `ix_mvc_feedback_feedback_category` (`category_id`),
  ADD KEY `ix_mvc_feedback_feedback_isfaq` (`is_faq`),
  ADD KEY `ix_mvc_feedback_feedback_status` (`status`),
  ADD KEY `ix_mvc_feedback_feedback_type` (`type`);

--
-- Индексы таблицы `mvc_gallery_gallery`
--
ALTER TABLE `mvc_gallery_gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_mvc_gallery_gallery_owner` (`owner`),
  ADD KEY `ix_mvc_gallery_gallery_sort` (`sort`),
  ADD KEY `ix_mvc_gallery_gallery_status` (`status`),
  ADD KEY `fk_mvc_gallery_gallery_gallery_preview_to_image` (`preview_id`),
  ADD KEY `fk_mvc_gallery_gallery_gallery_to_category` (`category_id`);

--
-- Индексы таблицы `mvc_gallery_image_to_gallery`
--
ALTER TABLE `mvc_gallery_image_to_gallery`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_gallery_image_to_gallery_gallery_to_image` (`image_id`,`gallery_id`),
  ADD KEY `ix_mvc_gallery_image_to_gallery_gallery_to_image_gallery` (`gallery_id`),
  ADD KEY `ix_mvc_gallery_image_to_gallery_gallery_to_image_image` (`image_id`);

--
-- Индексы таблицы `mvc_image_image`
--
ALTER TABLE `mvc_image_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_mvc_image_image_category_id` (`category_id`),
  ADD KEY `ix_mvc_image_image_status` (`status`),
  ADD KEY `ix_mvc_image_image_type` (`type`),
  ADD KEY `ix_mvc_image_image_user` (`user_id`),
  ADD KEY `fk_mvc_image_image_parent_id` (`parent_id`);

--
-- Индексы таблицы `mvc_mail_mail_event`
--
ALTER TABLE `mvc_mail_mail_event`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_mail_mail_event_code` (`code`);

--
-- Индексы таблицы `mvc_mail_mail_template`
--
ALTER TABLE `mvc_mail_mail_template`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_mail_mail_template_code` (`code`),
  ADD KEY `ix_mvc_mail_mail_template_event_id` (`event_id`),
  ADD KEY `ix_mvc_mail_mail_template_status` (`status`);

--
-- Индексы таблицы `mvc_menu_menu`
--
ALTER TABLE `mvc_menu_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_menu_menu_code` (`code`),
  ADD KEY `ix_mvc_menu_menu_status` (`status`);

--
-- Индексы таблицы `mvc_menu_menu_item`
--
ALTER TABLE `mvc_menu_menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_mvc_menu_menu_item_menu_id` (`menu_id`),
  ADD KEY `ix_mvc_menu_menu_item_sort` (`sort`),
  ADD KEY `ix_mvc_menu_menu_item_status` (`status`);

--
-- Индексы таблицы `mvc_migrations`
--
ALTER TABLE `mvc_migrations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_migrations_module` (`module`);

--
-- Индексы таблицы `mvc_news_news`
--
ALTER TABLE `mvc_news_news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_news_news_alias_lang` (`slug`,`lang`),
  ADD KEY `ix_mvc_news_news_category_id` (`category_id`),
  ADD KEY `ix_mvc_news_news_date` (`date`),
  ADD KEY `ix_mvc_news_news_status` (`status`),
  ADD KEY `ix_mvc_news_news_user_id` (`user_id`);

--
-- Индексы таблицы `mvc_notify_settings`
--
ALTER TABLE `mvc_notify_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_mvc_notify_settings_user_id` (`user_id`);

--
-- Индексы таблицы `mvc_page_page`
--
ALTER TABLE `mvc_page_page`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_page_page_slug_lang` (`slug`,`lang`),
  ADD KEY `ix_mvc_page_page_category_id` (`category_id`),
  ADD KEY `ix_mvc_page_page_change_user_id` (`change_user_id`),
  ADD KEY `ix_mvc_page_page_is_protected` (`is_protected`),
  ADD KEY `ix_mvc_page_page_menu_order` (`order`),
  ADD KEY `ix_mvc_page_page_status` (`status`),
  ADD KEY `ix_mvc_page_page_user_id` (`user_id`);

--
-- Индексы таблицы `mvc_review`
--
ALTER TABLE `mvc_review`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `mvc_store_attribute`
--
ALTER TABLE `mvc_store_attribute`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_store_attribute_name_group` (`name`,`group_id`),
  ADD KEY `ix_mvc_store_attribute_title` (`title`),
  ADD KEY `fk_mvc_store_attribute_group` (`group_id`);

--
-- Индексы таблицы `mvc_store_attribute_group`
--
ALTER TABLE `mvc_store_attribute_group`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `mvc_store_attribute_option`
--
ALTER TABLE `mvc_store_attribute_option`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_mvc_store_attribute_option_attribute_id` (`attribute_id`),
  ADD KEY `ix_mvc_store_attribute_option_position` (`position`);

--
-- Индексы таблицы `mvc_store_category`
--
ALTER TABLE `mvc_store_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_store_category_alias` (`slug`),
  ADD KEY `ix_mvc_store_category_parent_id` (`parent_id`),
  ADD KEY `ix_mvc_store_category_status` (`status`),
  ADD KEY `mvc_store_category_external_id_ix` (`external_id`);

--
-- Индексы таблицы `mvc_store_coupon`
--
ALTER TABLE `mvc_store_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `mvc_store_delivery`
--
ALTER TABLE `mvc_store_delivery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_mvc_store_delivery_position` (`position`);

--
-- Индексы таблицы `mvc_store_delivery_payment`
--
ALTER TABLE `mvc_store_delivery_payment`
  ADD PRIMARY KEY (`delivery_id`,`payment_id`),
  ADD KEY `fk_mvc_store_delivery_payment_payment` (`payment_id`);

--
-- Индексы таблицы `mvc_store_order`
--
ALTER TABLE `mvc_store_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `udx_mvc_store_order_url` (`url`),
  ADD KEY `idx_mvc_store_order_date` (`date`),
  ADD KEY `idx_mvc_store_order_paid` (`paid`),
  ADD KEY `idx_mvc_store_order_status` (`status_id`),
  ADD KEY `idx_mvc_store_order_user_id` (`user_id`),
  ADD KEY `fk_mvc_store_order_delivery` (`delivery_id`),
  ADD KEY `fk_mvc_store_order_manager` (`manager_id`),
  ADD KEY `fk_mvc_store_order_payment` (`payment_method_id`);

--
-- Индексы таблицы `mvc_store_order_coupon`
--
ALTER TABLE `mvc_store_order_coupon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mvc_store_order_coupon_coupon` (`coupon_id`),
  ADD KEY `fk_mvc_store_order_coupon_order` (`order_id`);

--
-- Индексы таблицы `mvc_store_order_product`
--
ALTER TABLE `mvc_store_order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_mvc_store_order_product_order_id` (`order_id`),
  ADD KEY `idx_mvc_store_order_product_product_id` (`product_id`);

--
-- Индексы таблицы `mvc_store_order_status`
--
ALTER TABLE `mvc_store_order_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `mvc_store_payment`
--
ALTER TABLE `mvc_store_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_mvc_store_payment_position` (`position`);

--
-- Индексы таблицы `mvc_store_producer`
--
ALTER TABLE `mvc_store_producer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_mvc_store_producer_slug` (`slug`),
  ADD KEY `ix_mvc_store_producer_sort` (`sort`);

--
-- Индексы таблицы `mvc_store_product`
--
ALTER TABLE `mvc_store_product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_store_product_alias` (`slug`),
  ADD KEY `ix_mvc_store_product_create_time` (`create_time`),
  ADD KEY `ix_mvc_store_product_discount_price` (`discount_price`),
  ADD KEY `ix_mvc_store_product_name` (`name`),
  ADD KEY `ix_mvc_store_product_position` (`position`),
  ADD KEY `ix_mvc_store_product_price` (`price`),
  ADD KEY `ix_mvc_store_product_producer_id` (`producer_id`),
  ADD KEY `ix_mvc_store_product_sku` (`sku`),
  ADD KEY `ix_mvc_store_product_status` (`status`),
  ADD KEY `ix_mvc_store_product_type_id` (`type_id`),
  ADD KEY `ix_mvc_store_product_update_time` (`update_time`),
  ADD KEY `mvc_store_product_external_id_ix` (`external_id`),
  ADD KEY `fk_mvc_store_product_category` (`category_id`);

--
-- Индексы таблицы `mvc_store_product_attribute_value`
--
ALTER TABLE `mvc_store_product_attribute_value`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mvc_ix_product_attribute_number_value` (`number_value`),
  ADD KEY `mvc_ix_product_attribute_string_value` (`string_value`),
  ADD KEY `mvc_fk_product_attribute_attribute` (`attribute_id`),
  ADD KEY `mvc_fk_product_attribute_option` (`option_value`),
  ADD KEY `mvc_fk_product_attribute_product` (`product_id`);

--
-- Индексы таблицы `mvc_store_product_category`
--
ALTER TABLE `mvc_store_product_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_mvc_store_product_category_category_id` (`category_id`),
  ADD KEY `ix_mvc_store_product_category_product_id` (`product_id`);

--
-- Индексы таблицы `mvc_store_product_image`
--
ALTER TABLE `mvc_store_product_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mvc_store_product_image_group` (`group_id`),
  ADD KEY `fk_mvc_store_product_image_product` (`product_id`);

--
-- Индексы таблицы `mvc_store_product_image_group`
--
ALTER TABLE `mvc_store_product_image_group`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `mvc_store_product_link`
--
ALTER TABLE `mvc_store_product_link`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_store_product_link_product` (`product_id`,`linked_product_id`),
  ADD KEY `fk_mvc_store_product_link_linked_product` (`linked_product_id`),
  ADD KEY `fk_mvc_store_product_link_type` (`type_id`);

--
-- Индексы таблицы `mvc_store_product_link_type`
--
ALTER TABLE `mvc_store_product_link_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_store_product_link_type_code` (`code`),
  ADD UNIQUE KEY `ux_mvc_store_product_link_type_title` (`title`);

--
-- Индексы таблицы `mvc_store_product_variant`
--
ALTER TABLE `mvc_store_product_variant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_mvc_store_product_variant_attribute` (`attribute_id`),
  ADD KEY `idx_mvc_store_product_variant_product` (`product_id`),
  ADD KEY `idx_mvc_store_product_variant_value` (`attribute_value`);

--
-- Индексы таблицы `mvc_store_type`
--
ALTER TABLE `mvc_store_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_store_type_name` (`name`);

--
-- Индексы таблицы `mvc_store_type_attribute`
--
ALTER TABLE `mvc_store_type_attribute`
  ADD PRIMARY KEY (`type_id`,`attribute_id`),
  ADD KEY `fk_mvc_store_type_attribute_attribute` (`attribute_id`);

--
-- Индексы таблицы `mvc_user_tokens`
--
ALTER TABLE `mvc_user_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_mvc_user_tokens_user_id` (`user_id`);

--
-- Индексы таблицы `mvc_user_user`
--
ALTER TABLE `mvc_user_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_user_user_email` (`email`),
  ADD UNIQUE KEY `ux_mvc_user_user_nick_name` (`nick_name`),
  ADD KEY `ix_mvc_user_user_status` (`status`),
  ADD KEY `fk_mvc_user_user_method_delivery_id` (`method_delivery_id`);

--
-- Индексы таблицы `mvc_user_user_auth_assignment`
--
ALTER TABLE `mvc_user_user_auth_assignment`
  ADD PRIMARY KEY (`itemname`,`userid`),
  ADD KEY `fk_mvc_user_user_auth_assignment_user` (`userid`);

--
-- Индексы таблицы `mvc_user_user_auth_item`
--
ALTER TABLE `mvc_user_user_auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `ix_mvc_user_user_auth_item_type` (`type`);

--
-- Индексы таблицы `mvc_user_user_auth_item_child`
--
ALTER TABLE `mvc_user_user_auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `fk_mvc_user_user_auth_item_child_child` (`child`);

--
-- Индексы таблицы `mvc_yupe_settings`
--
ALTER TABLE `mvc_yupe_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_mvc_yupe_settings_module_id_param_name_user_id` (`module_id`,`param_name`,`user_id`),
  ADD KEY `ix_mvc_yupe_settings_module_id` (`module_id`),
  ADD KEY `ix_mvc_yupe_settings_param_name` (`param_name`),
  ADD KEY `fk_mvc_yupe_settings_user_id` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `mvc_blog_blog`
--
ALTER TABLE `mvc_blog_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `mvc_blog_post`
--
ALTER TABLE `mvc_blog_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `mvc_blog_tag`
--
ALTER TABLE `mvc_blog_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `mvc_blog_user_to_blog`
--
ALTER TABLE `mvc_blog_user_to_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `mvc_category_category`
--
ALTER TABLE `mvc_category_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `mvc_chat_chat`
--
ALTER TABLE `mvc_chat_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `mvc_comment_comment`
--
ALTER TABLE `mvc_comment_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `mvc_contentblock_content_block`
--
ALTER TABLE `mvc_contentblock_content_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `mvc_dictionary_dictionary_data`
--
ALTER TABLE `mvc_dictionary_dictionary_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `mvc_dictionary_dictionary_group`
--
ALTER TABLE `mvc_dictionary_dictionary_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `mvc_feedback_feedback`
--
ALTER TABLE `mvc_feedback_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `mvc_gallery_gallery`
--
ALTER TABLE `mvc_gallery_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблицы `mvc_gallery_image_to_gallery`
--
ALTER TABLE `mvc_gallery_image_to_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT для таблицы `mvc_image_image`
--
ALTER TABLE `mvc_image_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;
--
-- AUTO_INCREMENT для таблицы `mvc_mail_mail_event`
--
ALTER TABLE `mvc_mail_mail_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `mvc_mail_mail_template`
--
ALTER TABLE `mvc_mail_mail_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `mvc_menu_menu`
--
ALTER TABLE `mvc_menu_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `mvc_menu_menu_item`
--
ALTER TABLE `mvc_menu_menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT для таблицы `mvc_migrations`
--
ALTER TABLE `mvc_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;
--
-- AUTO_INCREMENT для таблицы `mvc_news_news`
--
ALTER TABLE `mvc_news_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `mvc_notify_settings`
--
ALTER TABLE `mvc_notify_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `mvc_page_page`
--
ALTER TABLE `mvc_page_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `mvc_review`
--
ALTER TABLE `mvc_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблицы `mvc_store_attribute`
--
ALTER TABLE `mvc_store_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `mvc_store_attribute_group`
--
ALTER TABLE `mvc_store_attribute_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `mvc_store_attribute_option`
--
ALTER TABLE `mvc_store_attribute_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT для таблицы `mvc_store_category`
--
ALTER TABLE `mvc_store_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `mvc_store_coupon`
--
ALTER TABLE `mvc_store_coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `mvc_store_delivery`
--
ALTER TABLE `mvc_store_delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `mvc_store_order`
--
ALTER TABLE `mvc_store_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `mvc_store_order_coupon`
--
ALTER TABLE `mvc_store_order_coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `mvc_store_order_product`
--
ALTER TABLE `mvc_store_order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `mvc_store_order_status`
--
ALTER TABLE `mvc_store_order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `mvc_store_payment`
--
ALTER TABLE `mvc_store_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `mvc_store_producer`
--
ALTER TABLE `mvc_store_producer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `mvc_store_product`
--
ALTER TABLE `mvc_store_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT для таблицы `mvc_store_product_attribute_value`
--
ALTER TABLE `mvc_store_product_attribute_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=344;
--
-- AUTO_INCREMENT для таблицы `mvc_store_product_category`
--
ALTER TABLE `mvc_store_product_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `mvc_store_product_image`
--
ALTER TABLE `mvc_store_product_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT для таблицы `mvc_store_product_image_group`
--
ALTER TABLE `mvc_store_product_image_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `mvc_store_product_link`
--
ALTER TABLE `mvc_store_product_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `mvc_store_product_link_type`
--
ALTER TABLE `mvc_store_product_link_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `mvc_store_product_variant`
--
ALTER TABLE `mvc_store_product_variant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT для таблицы `mvc_store_type`
--
ALTER TABLE `mvc_store_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `mvc_user_tokens`
--
ALTER TABLE `mvc_user_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;
--
-- AUTO_INCREMENT для таблицы `mvc_user_user`
--
ALTER TABLE `mvc_user_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT для таблицы `mvc_yupe_settings`
--
ALTER TABLE `mvc_yupe_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `mvc_blog_blog`
--
ALTER TABLE `mvc_blog_blog`
  ADD CONSTRAINT `fk_mvc_blog_blog_category_id` FOREIGN KEY (`category_id`) REFERENCES `mvc_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_mvc_blog_blog_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `mvc_user_user` (`id`),
  ADD CONSTRAINT `fk_mvc_blog_blog_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `mvc_user_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `mvc_blog_post`
--
ALTER TABLE `mvc_blog_post`
  ADD CONSTRAINT `fk_mvc_blog_post_blog` FOREIGN KEY (`blog_id`) REFERENCES `mvc_blog_blog` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_mvc_blog_post_category_id` FOREIGN KEY (`category_id`) REFERENCES `mvc_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_mvc_blog_post_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `mvc_user_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_mvc_blog_post_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `mvc_user_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `mvc_blog_post_to_tag`
--
ALTER TABLE `mvc_blog_post_to_tag`
  ADD CONSTRAINT `fk_mvc_blog_post_to_tag_post_id` FOREIGN KEY (`post_id`) REFERENCES `mvc_blog_post` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_mvc_blog_post_to_tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `mvc_blog_tag` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_blog_user_to_blog`
--
ALTER TABLE `mvc_blog_user_to_blog`
  ADD CONSTRAINT `fk_mvc_blog_user_to_blog_blog_user_to_blog_blog_id` FOREIGN KEY (`blog_id`) REFERENCES `mvc_blog_blog` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_mvc_blog_user_to_blog_blog_user_to_blog_user_id` FOREIGN KEY (`user_id`) REFERENCES `mvc_user_user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_category_category`
--
ALTER TABLE `mvc_category_category`
  ADD CONSTRAINT `fk_mvc_category_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `mvc_category_category` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `mvc_chat_chat`
--
ALTER TABLE `mvc_chat_chat`
  ADD CONSTRAINT `fk_mvc_chat_chat_user_from` FOREIGN KEY (`user_from`) REFERENCES `mvc_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mvc_chat_chat_user_to` FOREIGN KEY (`user_to`) REFERENCES `mvc_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_comment_comment`
--
ALTER TABLE `mvc_comment_comment`
  ADD CONSTRAINT `fk_mvc_comment_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `mvc_comment_comment` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_mvc_comment_comment_user_id` FOREIGN KEY (`user_id`) REFERENCES `mvc_user_user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_dictionary_dictionary_data`
--
ALTER TABLE `mvc_dictionary_dictionary_data`
  ADD CONSTRAINT `fk_mvc_dictionary_dictionary_data_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `mvc_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mvc_dictionary_dictionary_data_data_group_id` FOREIGN KEY (`group_id`) REFERENCES `mvc_dictionary_dictionary_group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mvc_dictionary_dictionary_data_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `mvc_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `mvc_dictionary_dictionary_group`
--
ALTER TABLE `mvc_dictionary_dictionary_group`
  ADD CONSTRAINT `fk_mvc_dictionary_dictionary_group_create_user_id` FOREIGN KEY (`create_user_id`) REFERENCES `mvc_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mvc_dictionary_dictionary_group_update_user_id` FOREIGN KEY (`update_user_id`) REFERENCES `mvc_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `mvc_feedback_feedback`
--
ALTER TABLE `mvc_feedback_feedback`
  ADD CONSTRAINT `fk_mvc_feedback_feedback_answer_user` FOREIGN KEY (`answer_user`) REFERENCES `mvc_user_user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_mvc_feedback_feedback_category` FOREIGN KEY (`category_id`) REFERENCES `mvc_category_category` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `mvc_gallery_gallery`
--
ALTER TABLE `mvc_gallery_gallery`
  ADD CONSTRAINT `fk_mvc_gallery_gallery_gallery_preview_to_image` FOREIGN KEY (`preview_id`) REFERENCES `mvc_image_image` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_mvc_gallery_gallery_gallery_to_category` FOREIGN KEY (`category_id`) REFERENCES `mvc_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_mvc_gallery_gallery_owner` FOREIGN KEY (`owner`) REFERENCES `mvc_user_user` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `mvc_gallery_image_to_gallery`
--
ALTER TABLE `mvc_gallery_image_to_gallery`
  ADD CONSTRAINT `fk_mvc_gallery_image_to_gallery_gallery_to_image_gallery` FOREIGN KEY (`gallery_id`) REFERENCES `mvc_gallery_gallery` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_mvc_gallery_image_to_gallery_gallery_to_image_image` FOREIGN KEY (`image_id`) REFERENCES `mvc_image_image` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_image_image`
--
ALTER TABLE `mvc_image_image`
  ADD CONSTRAINT `fk_mvc_image_image_category_id` FOREIGN KEY (`category_id`) REFERENCES `mvc_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_mvc_image_image_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `mvc_image_image` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_mvc_image_image_user_id` FOREIGN KEY (`user_id`) REFERENCES `mvc_user_user` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `mvc_mail_mail_template`
--
ALTER TABLE `mvc_mail_mail_template`
  ADD CONSTRAINT `fk_mvc_mail_mail_template_event_id` FOREIGN KEY (`event_id`) REFERENCES `mvc_mail_mail_event` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_menu_menu_item`
--
ALTER TABLE `mvc_menu_menu_item`
  ADD CONSTRAINT `fk_mvc_menu_menu_item_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `mvc_menu_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_news_news`
--
ALTER TABLE `mvc_news_news`
  ADD CONSTRAINT `fk_mvc_news_news_category_id` FOREIGN KEY (`category_id`) REFERENCES `mvc_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_mvc_news_news_user_id` FOREIGN KEY (`user_id`) REFERENCES `mvc_user_user` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `mvc_notify_settings`
--
ALTER TABLE `mvc_notify_settings`
  ADD CONSTRAINT `fk_mvc_notify_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `mvc_user_user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_page_page`
--
ALTER TABLE `mvc_page_page`
  ADD CONSTRAINT `fk_mvc_page_page_category_id` FOREIGN KEY (`category_id`) REFERENCES `mvc_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_mvc_page_page_change_user_id` FOREIGN KEY (`change_user_id`) REFERENCES `mvc_user_user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_mvc_page_page_user_id` FOREIGN KEY (`user_id`) REFERENCES `mvc_user_user` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `mvc_store_attribute`
--
ALTER TABLE `mvc_store_attribute`
  ADD CONSTRAINT `fk_mvc_store_attribute_group` FOREIGN KEY (`group_id`) REFERENCES `mvc_store_attribute_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_store_attribute_option`
--
ALTER TABLE `mvc_store_attribute_option`
  ADD CONSTRAINT `fk_mvc_store_attribute_option_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `mvc_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_store_category`
--
ALTER TABLE `mvc_store_category`
  ADD CONSTRAINT `fk_mvc_store_category_parent` FOREIGN KEY (`parent_id`) REFERENCES `mvc_store_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_store_delivery_payment`
--
ALTER TABLE `mvc_store_delivery_payment`
  ADD CONSTRAINT `fk_mvc_store_delivery_payment_delivery` FOREIGN KEY (`delivery_id`) REFERENCES `mvc_store_delivery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mvc_store_delivery_payment_payment` FOREIGN KEY (`payment_id`) REFERENCES `mvc_store_payment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_store_order`
--
ALTER TABLE `mvc_store_order`
  ADD CONSTRAINT `fk_mvc_store_order_delivery` FOREIGN KEY (`delivery_id`) REFERENCES `mvc_store_delivery` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mvc_store_order_manager` FOREIGN KEY (`manager_id`) REFERENCES `mvc_user_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mvc_store_order_payment` FOREIGN KEY (`payment_method_id`) REFERENCES `mvc_store_payment` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mvc_store_order_status` FOREIGN KEY (`status_id`) REFERENCES `mvc_store_order_status` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mvc_store_order_user` FOREIGN KEY (`user_id`) REFERENCES `mvc_user_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_store_order_coupon`
--
ALTER TABLE `mvc_store_order_coupon`
  ADD CONSTRAINT `fk_mvc_store_order_coupon_coupon` FOREIGN KEY (`coupon_id`) REFERENCES `mvc_store_coupon` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_mvc_store_order_coupon_order` FOREIGN KEY (`order_id`) REFERENCES `mvc_store_order` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_store_order_product`
--
ALTER TABLE `mvc_store_order_product`
  ADD CONSTRAINT `fk_mvc_store_order_product_order` FOREIGN KEY (`order_id`) REFERENCES `mvc_store_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mvc_store_order_product_product` FOREIGN KEY (`product_id`) REFERENCES `mvc_store_product` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_store_product`
--
ALTER TABLE `mvc_store_product`
  ADD CONSTRAINT `fk_mvc_store_product_category` FOREIGN KEY (`category_id`) REFERENCES `mvc_store_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mvc_store_product_producer` FOREIGN KEY (`producer_id`) REFERENCES `mvc_store_producer` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mvc_store_product_type` FOREIGN KEY (`type_id`) REFERENCES `mvc_store_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_store_product_attribute_value`
--
ALTER TABLE `mvc_store_product_attribute_value`
  ADD CONSTRAINT `mvc_fk_product_attribute_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `mvc_store_attribute` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `mvc_fk_product_attribute_option` FOREIGN KEY (`option_value`) REFERENCES `mvc_store_attribute_option` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `mvc_fk_product_attribute_product` FOREIGN KEY (`product_id`) REFERENCES `mvc_store_product` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_store_product_category`
--
ALTER TABLE `mvc_store_product_category`
  ADD CONSTRAINT `fk_mvc_store_product_category_category` FOREIGN KEY (`category_id`) REFERENCES `mvc_store_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mvc_store_product_category_product` FOREIGN KEY (`product_id`) REFERENCES `mvc_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_store_product_image`
--
ALTER TABLE `mvc_store_product_image`
  ADD CONSTRAINT `fk_mvc_store_product_image_group` FOREIGN KEY (`group_id`) REFERENCES `mvc_store_product_image_group` (`id`) ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_mvc_store_product_image_product` FOREIGN KEY (`product_id`) REFERENCES `mvc_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_store_product_link`
--
ALTER TABLE `mvc_store_product_link`
  ADD CONSTRAINT `fk_mvc_store_product_link_linked_product` FOREIGN KEY (`linked_product_id`) REFERENCES `mvc_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mvc_store_product_link_product` FOREIGN KEY (`product_id`) REFERENCES `mvc_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mvc_store_product_link_type` FOREIGN KEY (`type_id`) REFERENCES `mvc_store_product_link_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_store_product_variant`
--
ALTER TABLE `mvc_store_product_variant`
  ADD CONSTRAINT `fk_mvc_store_product_variant_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `mvc_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mvc_store_product_variant_product` FOREIGN KEY (`product_id`) REFERENCES `mvc_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_store_type_attribute`
--
ALTER TABLE `mvc_store_type_attribute`
  ADD CONSTRAINT `fk_mvc_store_type_attribute_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `mvc_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mvc_store_type_attribute_type` FOREIGN KEY (`type_id`) REFERENCES `mvc_store_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_user_tokens`
--
ALTER TABLE `mvc_user_tokens`
  ADD CONSTRAINT `fk_mvc_user_tokens_user_id` FOREIGN KEY (`user_id`) REFERENCES `mvc_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_user_user`
--
ALTER TABLE `mvc_user_user`
  ADD CONSTRAINT `fk_mvc_user_user_method_delivery_id` FOREIGN KEY (`method_delivery_id`) REFERENCES `mvc_store_delivery` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `mvc_user_user_auth_assignment`
--
ALTER TABLE `mvc_user_user_auth_assignment`
  ADD CONSTRAINT `fk_mvc_user_user_auth_assignment_item` FOREIGN KEY (`itemname`) REFERENCES `mvc_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mvc_user_user_auth_assignment_user` FOREIGN KEY (`userid`) REFERENCES `mvc_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_user_user_auth_item_child`
--
ALTER TABLE `mvc_user_user_auth_item_child`
  ADD CONSTRAINT `fk_mvc_user_user_auth_item_child_child` FOREIGN KEY (`child`) REFERENCES `mvc_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mvc_user_user_auth_itemchild_parent` FOREIGN KEY (`parent`) REFERENCES `mvc_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mvc_yupe_settings`
--
ALTER TABLE `mvc_yupe_settings`
  ADD CONSTRAINT `fk_mvc_yupe_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `mvc_user_user` (`id`) ON DELETE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
